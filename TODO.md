# 01.05.

* `slwr_gng_update` could maybe delete isolated nodes, if it's not used by slwr/network.

* Move `plot_function_meshes.py` from `examples` to `slwr` (visualization directory).

# 07.05.

* Redo the 50 independent runs from Section 2 of the last report.

# 09.05.


* It would be nice if slwr-json-utils.h offered json array of doubles to `slwr_matrix` and `slwr_vector` functions(maybe with tests). There's already an array to matrix conversion private function in json-utils.c.

# 19.05.

* When the maximum amount of models is reached, the network should continue training those (unless some of them are pruned away). At the moment, an attempt to add more models is treated as an error and the training stops.

# 30.09.

* Update the code in `examples` and update the README.

* Update the visualization section in the README.

* Write the evaluation section in the README.

# 01.10.

* Here are the valid receptive field updates: "off", "pt-modify-rho", "pt-modify-chi", "pt-modify-upsilon", "dbd", "covariance". Use the rho and the dbd for comparison against LWPR. Covariance, chi, and upsilon should be used in comparing just the receptive field updates.

* In section 2 of the evaluation report, the default LWPR allocation ("default-lwpr") is used with PLS/DBD (neither yet available), OLS/PT-MODIFY ("pt-modify-rho" and "decay-error-max: off"; no parameter to switch between the regression kinds yet), and OLS/PT-MODIFY/delta error max ("pt-modify-rho" and "decay-error-max: on"). These methods are used to learn the Crossed Ridge and the Rotated Sine.

* From the point above, I need four sets of parameters: two for non-decayed error max (one per surface to be approximated), and two for the decayed error max (one per surface to be approximated).

# 18.10.

* There are some important TODOs to be cleaned up in `slwr/network`!
