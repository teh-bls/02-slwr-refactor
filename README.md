# Slwr

Slwr ("slower") is a collection of routines that can be assembled together into locally weighted regression (LWR)
networks. Its primary purpose is to simplify the comparison of two different receptive field update rules and two
different local model allocation strategies.

The routines are:

* `slwr/ols`, an implementation of ordinary least squares regression as described in [[1]](#references),
* ~~`slwr/pls`, an implemetation of partial least squares regression as presented in [[2]](#references),~~ **to be
  ported over**
* `slwr/pt-modify`, a receptive field update rule based on a variation of the PT-MODIFY ellipsoid modification method
  that is given in [[3]](#references),
* ~~`slwr/dbd`, an implemetation of the delta-bar-delta receptive field update rule that is presented
  in [[2]](#references), and~~ **to be ported over**
* `slwr/gng`, a variation of the Growing Neural Gas that is described in [[4]](#references).

Each routine is implemented trivially and as described in literature, with no attempts at optimizations.

To each LWR network that can be assembled belong a regression kind, a local model allocation method, and a receptive
field update rule:

| Network | Regression kind | Local model allocation | Receptive field update rule |
|:--|:--|:--|:--|
| `slwr/network` | `slwr/ols` or `slwr/pls`| Default Locally Weighted Projection Regression (LWPR) or several variations  of `slwr/gng` | `slwr/dbd` or `slwr/pt-modify` |
| ~~`slwr/llm`~~ To be ported over | `slwr/ols` | None (fixed number of local models) | None (fixed receptive fields) |
| ~~`slwr/lwpr`~~ To be ported over | `slwr/pls` | Default LWPR | `slwr/dbd` |

Alongside them, several helpers and tools are available:

* `slwr/examples` contains a small number of examples of basic usage of the routines. These are limited to a
  two-dimensional input space and, where applicable, a one-dimensional output space,

* `visualization` contains scripts for basic visualization of networks that map two-dimensional inputs to
  one-dimensional output, including local model populations and surface meshes, and

* `evaluation` provides helpers for training instances of `slwr/network` and plotting the results.

These are described in more detail below.

## Building

By default, a static and a dynamic library is built along with `slwr/examples` and `evaluation`.

### Dependencies

* CMake 3.0
* [GSL](https://www.gnu.org/software/gsl/)
* [GLib 2.0](https://wiki.gnome.org/Projects/GLib)
* [Jansson](https://github.com/akheron/jansson)

### Compiling all targets

From the root directory of the project:

```
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=<installation-location>
cmake --build .
```

### Installing the dynamic library and headers (optional)

```
cmake --install .
```

A pkg-config file will be installed in `<installation-location>/lib/pkgconfig`.

Note that examples are not installed.

## Examples

The binaries for the built examples, which are not installed, are located in the `examples` subdirectory of the build
directory. Once built, all examples can be run without any additional arguments.
If changes to network parameters are required, these can be made in their corresponding source files in the `examples`
directory and rebuilt.
Some examples, like `slwr-gng-example` and `slwr-network-example`, write their output to a JSON file located in the same
directory from which the example is executed.

## Visualization

### Dependencies

* Python 3
* [Matplotlib](https://matplotlib.org/)
* [NumPy](https://numpy.org/)
* [dvipng](https://tug.org/texinfohtml/dvipng.html), TeX Live's DVI to PNG translator
* type1cm, the arbitrary size font selection for TeX Live

### Installation

From the root directory of the project:

```
cd visualization
pip install .
```

### Running example visualization scripts

`visualization/examples/plot_function_meshes.py` plots meshes of available two-dimensional surface meshes:

![](doc/images/crossed-ridge-mesh.png) ![](doc/images/rotated-sine-mesh.png)

`visualization/slwr/plot_population.py` takes a population JSON as argument (produced, for example, by
the `slwr-network-example`) and plots the receptive fields of its model population:

![](doc/images/crossed-ridge-population.png) ![](doc/images/rotated-sine-population.png)

`visualization/examples/plot_slwr_gng.py` reads the GNG network defined in `visualization/examples/gng-example.json` and
plots it:

![](doc/images/slwr-gng-example.png)


## Evaluation


## References

* [1] Ritter H., Martinetz T., Schulten K., Barsky D. *Neural computation and self-organizing maps : an introduction*.
  Addison-Wesley, 1992.
* [2] Vijayakumar S., D'Souza A., Schaal S. *Incremental online learning in high dimensions*. Neural Computation, 17:
  2602-2634, 2005.
* [3] Pope, S. B. *Algorithms for Ellipsoids*. FDA-08-01, Cornell University, Ithaca, New York 14853, 2008.
* [4] Fritzke, B. *A Growing Neural Gas Network Learns Topologies*. NIPS'94: Proceedings of the 7th International
  Conference on Neural Information Processing Systems, Pages 625-632, January 1994.
