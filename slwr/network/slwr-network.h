#pragma once

#include "slwr/common/slwr-types.h"
#include "slwr/gng/slwr-gng.h"
#include "slwr/ols/slwr-ols.h"
#include "slwr/pt-modify/slwr-pt-modify.h"
#include "slwr/receptive-field/slwr-receptive-field.h"
#include "slwr/utils/slwr-allocation-helper.h"

#include <glib.h>

typedef struct {
  slwr_vector *input_space_max;
  slwr_vector *input_space_min;
  gsize n_updates;
} slwr_network_statistics;

typedef struct {
  gdouble lambda_init;  /* The initial value of the forgetting factor lambda. */
  gdouble lambda_final; /* The final value of the forgetting factor lambda. */
  slwr_vector *n_data;  /* The number of data the model encountered. */
  slwr_vector *lambda;  /* The update forgetting factor. */
} slwr_network_model_statistics;

typedef struct {
  gdouble inputs_seen; /* TODO: either drop or move to
                          slwr_network_model_statistics */
  slwr_receptive_field *rf;
  slwr_ols_context *ols_ctx;
  slwr_pt_modify_parameters *pt_modify_params;
  slwr_incremental_weighted_mean
      mean_error;             /*TODO: move to slwr_network_model_statistics */
  gdouble error_max_decay_N0; /* The initial value of error_max_decay */
  gsize n_rf_updates; /* The number of receptive field updates; TODO: how is
                         this different to inputs_seen above? move to
                         slwr_network_model_statistics */
  slwr_gng_node *gng_node; /* The corresponding GNG node (if applicable) */
  gdouble last_activation; /* The last activation w of the model by an input */
  slwr_network_model_statistics *statistics;
} slwr_network_model;

typedef struct {
  GPtrArray *available_models;
  GPtrArray *allocated_models;
  slwr_allocation_helper *allocation_helper;
  slwr_gng_context *gng_ctx;
  gboolean error_guided_insertion_single_model_present;
  slwr_allocation_helper_result *highest_activation;
  slwr_allocation_helper_result *second_highest_activation;
} slwr_model_population;

typedef enum {
  SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_OFF,
  SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_PT_MODIFY_RHO,
  SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_PT_MODIFY_CHI,
  SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_PT_MODIFY_UPSILON,
  SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_COVARIANCE,
  SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_DBD
} slwr_network_receptive_field_update_kind;

/* The global slwr_network parameters */
typedef struct {
  gsize n_in;
  gsize n_out;
  slwr_matrix *D_init;
  gsize max_models;
  slwr_network_receptive_field_update_kind rf_update_kind;
  gchar *model_allocation;
  gboolean decay_error_max; /* If TRUE, the value of error_max kept by each
                               model will decay as that model's receptive field
                               is updated. */
  gboolean
      apply_strict_decay_error_max; /* If TRUE, the initial value of each
                                       model's error_max must not fall under the
                                       value of the global error_max; used only
                                       if decay_error_max is TRUE. */
  gdouble decay_error_max_tau; /* The decay constant for error_max; used only if
                                  decay_error_max is TRUE. */
  gdouble error_max;      /* The value of the global error_max; used only if
                             decay_error_max is TRUE. */
  gboolean model_pruning; /* If TRUE, models in the populations are pruned
                             according to the default LWPR pruning rules. */
  gchar *error_guided_insertion_error_source; /* Expects either "output-space"
                                                 or "input-space". */
  gdouble w_gen;
  gdouble w_prune;
  gdouble lambda_init;  /* The initial value of the forgetting factor; TODO:
                           describe what this is used for */
  gdouble lambda_tau;   /* The forgetting factor annealing constant; TODO:
                           describe   what this is used for */
  gdouble lambda_final; /* The final value of the forgetting factor; TODO:
                           describe what this is used for*/
} slwr_network_parameters;

typedef struct {
  slwr_ols_parameters *ols_params;
  slwr_pt_modify_parameters *pt_modify_params;
  slwr_gng_parameters *gng_params;
  slwr_network_parameters *network_params;
  slwr_network_statistics *stats;
  GPtrArray *populations;
} slwr_network_context;

void slwr_network_parameters_default_values(gsize n_in,
                                            slwr_network_parameters *params);

slwr_network_context *
slwr_network_context_alloc_from_json_string(const gchar *json);
void slwr_network_context_free(slwr_network_context *ctx);

void slwr_network_parameters_free(slwr_network_parameters *params);

slwr_status slwr_network_update(slwr_workspace *ws, slwr_network_context *ctx,
                                const slwr_vector *x, const slwr_vector *y);

slwr_status slwr_network_single_model_predict(slwr_workspace *ws,
                                              const slwr_network_context *ctx,
                                              const slwr_network_model *model,
                                              const slwr_vector *x,
                                              gdouble *y_pred);
slwr_status slwr_network_predict(slwr_workspace *ws,
                                 const slwr_network_context *ctx,
                                 const slwr_vector *x, slwr_vector *y_pred);
