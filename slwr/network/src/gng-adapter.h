#pragma once

#include "slwr/common/slwr-types.h"
#include "slwr/network/slwr-network.h"

#include <gsl/gsl_vector.h>

slwr_gng_context *gng_adapter_context_alloc();

slwr_gng_node *gng_adapter_node_alloc(slwr_receptive_field *rf);

void gng_adapter_node_free(slwr_gng_node *node);

typedef struct {
  slwr_gng_edge *edge; /* A non-owning pointer to the least activated edge */
  gdouble
      midpoint_activation_a; /* The activation of edge->a's receptive field */
  gdouble
      midpoint_activation_b; /* The activation of edge->b's receptive field */
} gng_adapter_edge_with_least_activated_nodes_result;

gng_adapter_edge_with_least_activated_nodes_result *
gng_adapter_find_edge_with_least_activated_nodes(
    slwr_workspace *ws, const slwr_network_context *ctx,
    const slwr_model_population *population);

void gng_adapter_least_activated_edge_result_free(
    gng_adapter_edge_with_least_activated_nodes_result *result);

slwr_status gng_adapter_update(slwr_workspace *ws, slwr_network_context *ctx,
                               slwr_model_population *population,
                               slwr_gng_node *most_activated_node,
                               slwr_gng_node *second_most_activated_node,
                               const gsl_vector *x);

slwr_gng_node *gng_adapter_insert_node_between(
    slwr_workspace *ws, slwr_model_population *population,
    const slwr_gng_node *a, const slwr_gng_node *b, slwr_matrix *D_template);

slwr_gng_node *gng_adapter_insert_node_with_respect_to_input_error(
    slwr_workspace *ws, const slwr_network_context *ctx,
    slwr_model_population *population);

slwr_gng_node *gng_adapter_insert_node_with_respect_to_output_error(
    slwr_workspace *ws, const slwr_network_context *ctx,
    slwr_model_population *population);

slwr_status gng_adapter_remove_node(slwr_model_population *population,
                                    slwr_gng_node *node);

slwr_status gng_adapter_prune_edges(slwr_network_context *ctx,
                                    slwr_model_population *population);
