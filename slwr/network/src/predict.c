#include "predict.h"

#include "slwr/network/slwr-network.h"

#include "slwr/ols/src/ols.h"
#include "slwr/receptive-field/src/receptive-field.h"

#include <math.h>

slwr_status single_model_predict(slwr_workspace *ws,
                                 const slwr_network_context *ctx,
                                 const slwr_network_model *model,
                                 const slwr_vector *x, gdouble *y_pred) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(model != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  gdouble y_data[] = {*y_pred};
  slwr_vector y = {.size = 1, .data = y_data};
  /* TODO: take the type of regression into account; comes from ctx's network
   * parameters */
  slwr_status status =
      ols_predict_slwr(ws, model->ols_ctx, model->rf->c, x, &y);
  *y_pred =
      y.data[0]; /* TODO: can I simply use the address of y_pred as y_data? */
  return status;
}

/**
 * Predicts the output y_pred from given input x using the given model. The
 * output is not weighted by the activation of the model by x.
 *
 * @param ws The workspace.
 * @param ctx The network context.
 * @param model The model to be used for prediction.
 * @param x The input vector.
 * @param y_pred The predicted output.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating error otherwise.
 */
slwr_status slwr_network_single_model_predict(slwr_workspace *ws,
                                              const slwr_network_context *ctx,
                                              const slwr_network_model *model,
                                              const slwr_vector *x,
                                              gdouble *y_pred) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(model != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  gdouble y_data[] = {*y_pred};
  slwr_vector y = {.size = 1, .data = y_data};
  /* TODO: take the type of regression into account; comes from ctx's network
   * parameters */
  /* TODO: this is called in prediction and in training - for training, I'd like
   * not to reset the workspace. Change this! */
  slwr_status status =
      slwr_ols_predict(ws, model->ols_ctx, model->rf->c, x, &y);
  *y_pred =
      y.data[0]; /* TODO: can I simply use the address of y_pred as y_data? */

  return status;
}

static slwr_status population_predict(slwr_workspace *ws,
                                      const slwr_network_context *ctx,
                                      const slwr_model_population *population,
                                      const slwr_vector *x, gdouble *y_pred) {

  slwr_status status = SLWR_STATUS_OK;
  gdouble yp = 0.0;
  gdouble sum_w = 0.0;
  for (gsize i = 0; i < population->allocated_models->len; ++i) {
    const slwr_network_model *model =
        (const slwr_network_model *)g_ptr_array_index(
            population->allocated_models, i);
    gdouble w = 0.0;
    if ((status = determine_activation_slwr(
             ws, model->rf, x, SLWR_KERNEL_GAUSSIAN, &w)) != SLWR_STATUS_OK) {
      goto end;
    }
    if (fpclassify(w) == FP_SUBNORMAL) {
      w = 0.0;
    }

    gdouble yp_i = 0.0;
    if ((status = slwr_network_single_model_predict(ws, ctx, model, x,
                                                    &yp_i)) != SLWR_STATUS_OK) {
      goto end;
    }
    yp += w * yp_i;
    sum_w += w;
  }

  *y_pred = sum_w > 0 ? yp / sum_w : 0.0;

end:
  return status;
}

slwr_status slwr_network_predict(slwr_workspace *ws,
                                 const slwr_network_context *ctx,
                                 const slwr_vector *x, slwr_vector *y_pred) {

  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->network_params->n_in == x->size,
                       SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(ctx->network_params->n_out == y_pred->size,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  slwr_status status = SLWR_STATUS_OK;

  for (gsize i = 0; i < ctx->populations->len; ++i) {
    gdouble *y = &y_pred->data[i];
    const slwr_model_population *population =
        (const slwr_model_population *)g_ptr_array_index(ctx->populations, i);
    if ((status = population_predict(ws, ctx, population, x, y)) !=
        SLWR_STATUS_OK) {
      g_critical(
          "%s: could not predict for model population at output dimension %lu",
          __PRETTY_FUNCTION__, i);
      goto end;
    }
  }

end:
  return status;
}
