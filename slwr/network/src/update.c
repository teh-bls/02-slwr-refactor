#include "slwr/network/slwr-network.h"

#include "error-guided-insertion-update.h"
#include "lwpr-allocation-update.h"
#include "network-model-statistics.h"
#include "parameter-utils.h"
#include "population.h"
#include "predict.h"

#include "slwr/gng/src/gng.h"
#include "slwr/ols/src/ols.h"
#include "slwr/pt-modify/src/pt-modify.h"
#include "slwr/receptive-field/src/receptive-field.h"

#include <gsl/gsl_math.h>

static slwr_status update_model_regression(slwr_workspace *ws,
                                           slwr_network_context *ctx,
                                           slwr_network_model *model,
                                           const slwr_vector *x, gdouble y) {
  gdouble yv_data[] = {y};
  slwr_vector yv = {.size = 1, .data = yv_data};
  return ols_update_slwr(ws, model->ols_ctx, ctx->ols_params, model->rf->c,
                         model->last_activation, x, &yv);
}

/** Determines the given model's unweighted prediction error for the given
 * input x and actual output y. The prediction error is the absolute difference
 * between the actual and predicted outputs.
 *
 * TODO: change this to return an error slwr_status rather than GSL_NAN on
 * failure.
 *
 * @param ws The workspace.
 * @param ctx The network context.
 * @param model The model to predict from.
 * @param x The input vector.
 * @param y The actual output.
 * @return The prediction error on success, GSL_NAN on failure.
 */
static gdouble determine_model_error(slwr_workspace *ws,
                                     const slwr_network_context *ctx,
                                     const slwr_network_model *model,
                                     const slwr_vector *x, gdouble y) {
  gdouble error = GSL_NAN;
  gdouble y_pred;
  if (single_model_predict(ws, ctx, model, x, &y_pred) != SLWR_STATUS_OK) {
    return error;
  }
  if (!gsl_isnan(y_pred)) {
    error = fabs(y - y_pred);
  }
  return error;
}

/**
 * Determines whether the regression parameters of the given model were updated
 * sufficiently for that model to be considered trustworthy.
 *
 * For the time being, this is a comparison to a constant. The LWPR approach to
 * trustworthiness considers the number of updates on each PLS projection
 * direction.
 *
 * TODO: Update the description
 *
 * @param model The model to be tested for trustworthiness.
 * @param n_in The input dimensionality.
 * @return TRUE is the model is trustworthy, FALSE otherwise.
 */
static inline gboolean model_trustworthy(const slwr_network_model *model,
                                         gsize n_in) {
  gboolean trustworthy =
      model->statistics->n_data->data[0] > 2.0f * (gdouble)n_in;
  return trustworthy;
}

static inline void decay_error_max(slwr_network_model *model, gsize t,
                                   gdouble target_error_max, gdouble tau) {
  model->pt_modify_params->error_max =
      (model->error_max_decay_N0 - target_error_max) *
          exp(-1 * (gdouble)t / tau) +
      target_error_max;
}

static inline void
initialize_error_max_decay_if_required(const slwr_network_context *ctx,
                                       slwr_network_model *model) {
  if (gsl_isnan(model->pt_modify_params->error_max)) {
    if (ctx->network_params->apply_strict_decay_error_max &&
        ctx->network_params->error_max > model->mean_error.mean) {
      model->pt_modify_params->error_max = ctx->network_params->error_max;
    } else {
      model->pt_modify_params->error_max = model->mean_error.mean;
    }
  }
  if (gsl_isnan(model->error_max_decay_N0)) {
    model->error_max_decay_N0 = model->pt_modify_params->error_max;
  }
}

static slwr_status update_receptive_field(slwr_workspace *ws,
                                          slwr_network_context *ctx,
                                          slwr_network_model *model,
                                          const slwr_vector *x, gdouble error) {
  if (!model_trustworthy(model, ctx->network_params->n_in)) {
    return SLWR_STATUS_OK;
  }

  if (ctx->network_params->decay_error_max) {
    initialize_error_max_decay_if_required(ctx, model);
  }

  const gdouble weighted_error = model->last_activation * error;
  /* TODO: read the receptive field update rule from parameters */
  slwr_status status =
      pt_modify_update_slwr(ws, model->pt_modify_params, SLWR_PT_MODIFY_RHO,
                            model->rf, x, weighted_error);
  if (status == SLWR_STATUS_OK) {
    ++model->n_rf_updates;
    if (ctx->network_params->decay_error_max) {
      decay_error_max(model, model->n_rf_updates,
                      ctx->network_params->error_max,
                      ctx->network_params->decay_error_max_tau);
    }
  }

  return status;
}

static inline gboolean sufficiently_activated(gdouble w) { return w > 0.001; }

/**
 * Determines the activations by the input x of all models in the population as
 * well as the highest and second highest activations.
 *
 * The activation of each model is stored in the corresponding field. The
 * highest and second highest activations are stored in the corresponding fields
 * of the population struct.
 *
 * @param ws The slwr workspace.
 * @param population The model population.
 * @param x The input vector x.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status
determine_and_store_model_activations(slwr_workspace *ws,
                                      slwr_model_population *population,
                                      const slwr_vector *x) {
  slwr_status status =
      slwr_allocation_helper_reset(population->allocation_helper);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  /* TODO: I think I can have an error_guided_insertion_single_model_present()
   * function instead of this flag */
  if (population->error_guided_insertion_single_model_present) {
    slwr_network_model *model = (slwr_network_model *)g_ptr_array_index(
        population->allocated_models, 0);
    model->last_activation = 1.0;
    return SLWR_STATUS_OK;
  }
  for (gsize i = 0; i < population->allocated_models->len; ++i) {
    slwr_network_model *model = (slwr_network_model *)g_ptr_array_index(
        population->allocated_models, i);
    if (!model) {
      return SLWR_STATUS_ERROR;
    }
    if ((status = determine_activation_slwr(
             ws, model->rf, x, SLWR_KERNEL_GAUSSIAN,
             &model->last_activation)) != SLWR_STATUS_OK) {
      return status;
    }
    if ((status = slwr_allocation_helper_update(population->allocation_helper,
                                                model->last_activation, i)) !=
        SLWR_STATUS_OK) {
      return status;
    }
  }
  if ((status = population_store_highest_and_second_highest_activations(
           population)) != SLWR_STATUS_OK) {
    g_critical("could not determine highest and second highest activations");
    return status;
  }
  return SLWR_STATUS_OK;
}

/**
 * Updates the model population's regression and receptive fields.
 *
 * Assumes that each model has its last_activation field set to the
 * corresponding activation for input x. (If in doubt, call
 * determine_and_store_model_activations() first).
 *
 * @param ws The slwr workspace.
 * @param ctx The slwr network context.
 * @param population The model population.
 * @param x The input vector x.
 * @param y The actual output.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status update_population(slwr_workspace *ws,
                                     slwr_network_context *ctx,
                                     slwr_model_population *population,
                                     const slwr_vector *x, gdouble y) {
  for (gsize i = 0; i < population->allocated_models->len; ++i) {
    slwr_network_model *model = (slwr_network_model *)g_ptr_array_index(
        population->allocated_models, i);

    if (!model) {
      g_critical("could not access allocated model at index %lu", i);
      return SLWR_STATUS_NOT_FOUND;
    }
    if (!sufficiently_activated(model->last_activation)) {
      continue;
    }
    slwr_status status = update_model_regression(ws, ctx, model, x, y);
    if (status != SLWR_STATUS_OK) {
      return status;
    }
    gdouble model_error = determine_model_error(ws, ctx, model, x, y);
    if (gsl_isnan(model_error)) {
      return SLWR_STATUS_ERROR;
    }
    if (receptive_field_update_on(ctx) &&
        !population->error_guided_insertion_single_model_present) {
      update_receptive_field(ws, ctx, model, x, model_error);
    }
    slwr_incremental_weighted_mean_update(&model->mean_error, model_error,
                                          model->last_activation);
    /* model->inputs_seen += model->last_activation; */
    model->inputs_seen += 1.0; /* TODO: rename and resolve this */

    /* TODO: find a better place for updating GNG's node output space error
     */
    if (model->gng_node) {
      model->gng_node->output_space_error = model->mean_error.mean;
    }
    if ((status = slwr_network_model_statistics_update(model->statistics)) !=
        SLWR_STATUS_OK) {
      return status;
    }
  }
  return SLWR_STATUS_OK;
}

static inline gboolean
model_allocation_is_error_guided_insertion(const slwr_network_context *ctx) {
  return model_allocation_is_error_guided_insertion_and_vector_quantization(
             ctx) ||
         model_allocation_is_error_guided_insertion_and_penalized_vector_quantization(
             ctx);
}

static inline gboolean model_suffices(const slwr_network_context *ctx,
                                      const slwr_network_model *model) {
  return model->mean_error.mean <= ctx->network_params->error_max;
}

static slwr_status update_with_error_guided_insertion(
    slwr_workspace *ws, slwr_network_context *ctx,
    slwr_model_population *population, const slwr_vector *x, gdouble y) {
  if (population->allocated_models->len == 0) {
    slwr_network_model *model = population_add_model_centered_at(population, x);
    if (!model) {
      g_critical("failed adding the initial model");
      return SLWR_STATUS_ERROR;
    }
    population->error_guided_insertion_single_model_present = TRUE;
  }

  slwr_status status = determine_and_store_model_activations(ws, population, x);
  if (status != SLWR_STATUS_OK) {
    g_critical("failed determining activations of models in the population");
    return status;
  }
  status = update_population(ws, ctx, population, x, y);
  if (status != SLWR_STATUS_OK) {
    g_critical("failed updating the model population");
    return status;
  }
  /* TODO: move the GNG update from update_population to here */

  if (population->error_guided_insertion_single_model_present) {
    if (error_guided_insertion_should_insert_model(ctx) &&
        !model_suffices(ctx,
                        g_ptr_array_index(population->allocated_models, 0))) {
      if ((status = error_guided_insertion_setup_gng_network(
               ctx, population)) != SLWR_STATUS_OK) {
        g_critical("failed setting up the GNG network");
        return status;
      }
      population->error_guided_insertion_single_model_present = FALSE;
    }
  } else {
    if ((status = error_guided_insertion_gng_update(ws, ctx, population, x)) !=
        SLWR_STATUS_OK) {
      g_critical("could not update the underlying GNG network");
      return status;
    }

    if (error_guided_insertion_should_insert_model(ctx)) {
      if ((status = error_guided_insertion_insert_model(ws, ctx, population)) !=
          SLWR_STATUS_OK) {
        g_critical("could not insert a new model");
        return status;
      }
    }

    if ((status = error_guided_insertion_prune_models_and_edges(
             ctx, population)) != SLWR_STATUS_OK) {
      g_critical("could not prune models and GNG edges");
      return status;
    }

    if ((status = gng_decrease_input_space_errors(
             population->gng_ctx, ctx->gng_params)) != SLWR_STATUS_OK) {
      g_critical("could not decrease GNG node errors");
      return status;
    }
  }

  return SLWR_STATUS_OK;
}

static slwr_status
update_with_lwpr_allocation(slwr_workspace *ws, slwr_network_context *ctx,
                            slwr_model_population *population,
                            const slwr_vector *x, gdouble y) {
  if (population->allocated_models->len == 0) {
    if (!population_add_model_centered_at(population, x)) {
      g_critical("failed adding the initial model");
      return SLWR_STATUS_ERROR;
    }
  }
  slwr_status status = determine_and_store_model_activations(ws, population, x);
  if (status != SLWR_STATUS_OK) {
    g_critical("failed determining activations of models in the population");
    return status;
  }
  if ((status = update_population(ws, ctx, population, x, y)) !=
      SLWR_STATUS_OK) {
    g_critical("failed updating the model population");
    return status;
  }

  if ((status = lwpr_allocation_allocate_model(ctx, population, x)) !=
      SLWR_STATUS_OK) {
    g_critical("failed allocating a new model");
    return status;
  }

  if (ctx->network_params->model_pruning) {
    if ((status = lwpr_allocation_prune_model(ctx, population)) !=
        SLWR_STATUS_OK) {
      g_critical("failed pruning models");
      return status;
    }
  }

  return SLWR_STATUS_OK;
}

static slwr_status update(slwr_workspace *ws, slwr_network_context *ctx,
                          slwr_model_population *population,
                          const slwr_vector *x, gdouble y) {
  if (model_allocation_is_default_lwpr(ctx)) {
    return update_with_lwpr_allocation(ws, ctx, population, x, y);
  } else if (model_allocation_is_error_guided_insertion(ctx)) {
    return update_with_error_guided_insertion(ws, ctx, population, x, y);
  }
  g_critical("unknown model allocation method %s",
             ctx->network_params->model_allocation);
  return SLWR_STATUS_ERROR;
}

static void update_network_statistics(slwr_network_context *ctx,
                                      const slwr_vector *x) {
  for (gsize i = 0; i < x->size; ++i) {
    gdouble *max = &ctx->stats->input_space_max->data[i];
    if (x->data[i] > *max) {
      *max = x->data[i];
    }
    gdouble *min = &ctx->stats->input_space_min->data[i];
    if (x->data[i] < *min) {
      *min = x->data[i];
    }
  }
  ++ctx->stats->n_updates;
}

slwr_status slwr_network_update(slwr_workspace *ws, slwr_network_context *ctx,
                                const slwr_vector *x, const slwr_vector *y) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->network_params->n_in == x->size, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->network_params->n_out == y->size,
                       SLWR_STATUS_ERROR);

  slwr_workspace_reset(ws);
  slwr_status status = SLWR_STATUS_OK;

  for (gsize i = 0; i < ctx->populations->len; ++i) {
    slwr_model_population *population =
        (slwr_model_population *)g_ptr_array_index(ctx->populations, i);
    if ((status = update(ws, ctx, population, x, y->data[i])) !=
        SLWR_STATUS_OK) {
      g_critical("%s: could not update the population for output dimension %lu",
                 __PRETTY_FUNCTION__, i);
      goto end;
    }
  }
  update_network_statistics(ctx, x);

end:
  slwr_workspace_reset(ws);
  return status;
}
