#include "lwpr-allocation-update.h"

#include "context.h"
#include "population.h"

slwr_status lwpr_allocation_allocate_model(slwr_network_context *ctx,
                                           slwr_model_population *population,
                                           const slwr_vector *x) {
  const gdouble w_gen = ctx->network_params->w_gen;

  if (population->highest_activation->activation < w_gen) {
    slwr_network_model *highest_activation_model =
        (slwr_network_model *)g_ptr_array_index(
            population->allocated_models,
            population->highest_activation->index);
    if (!highest_activation_model) {
      return SLWR_STATUS_ERROR;
    }
    slwr_network_model *allocated_model = population_add_model_with_D(
        ctx, population, x, highest_activation_model->rf->D);
    if (!allocated_model) {
      return SLWR_STATUS_ERROR;
    }
    /* TODO: don't copy the OLS context for now
    if (ols_context_copy(allocated_model->ols_ctx,
                         highest_activation_model->ols_ctx) != SLWR_STATUS_OK) {
      return SLWR_STATUS_ERROR;
    }
    */
  }
  return SLWR_STATUS_OK;
}

slwr_status lwpr_allocation_prune_model(slwr_network_context *ctx,
                                        slwr_model_population *population) {

  if (!population_should_prune_model(ctx, population)) {
    return SLWR_STATUS_OK;
  }

  slwr_status status;
  gsize removal_index = population->second_highest_activation->index;
  if ((status = population_remove_model_at(ctx, population, removal_index)) !=
      SLWR_STATUS_OK) {
    return status;
  }
  return SLWR_STATUS_OK;
}
