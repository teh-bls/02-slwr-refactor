#include "gng-adapter.h"
#include "parameter-utils.h"

#include "slwr/common/src/workspace.h"
#include "slwr/gng/src/gng.h"
#include "slwr/receptive-field/src/receptive-field.h"

#include <glib.h>
#include <gsl/gsl_math.h>

/* TODO: this is a very slow mess; resolving the mix between slwr_vector and
 * gsl_vector might fix it.
 */
static slwr_status update_node_center_with_penalized_vector_quantization(
    slwr_workspace *ws, const slwr_gng_context *ctx, slwr_gng_node *n,
    const gsl_vector *x, gdouble epsilon) {
  gsl_vector *delta = workspace_vector(ws, WORKSPACE_N_IN);
  if (!delta) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *b = workspace_vector(ws, WORKSPACE_N_IN);
  if (!b) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *c =
      workspace_gsl_vector_from_slwr_vector(ws, n->rf->c, WORKSPACE_N_IN);
  if (!c) {
    return SLWR_STATUS_ERROR;
  }
  gsl_matrix *D =
      workspace_gsl_matrix_from_slwr_matrix(ws, n->rf->D, WORKSPACE_N_IN);
  if (!D) {
    return SLWR_STATUS_ERROR;
  }

  if (gsl_vector_memcpy(delta, x) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_sub(delta, c) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_scale(delta, epsilon)) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_memcpy(b, delta) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_add(b, c) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }

  double max_wb = DBL_MIN;
  for (gsize i = 0; i < ctx->nodes->len; ++i) {
    const slwr_gng_node *m =
        (const slwr_gng_node *)g_ptr_array_index(ctx->nodes, i);
    if (m == n) {
      continue;
    }
    gdouble wb;
    if (determine_activation_gsl(ws, D, c, b, SLWR_KERNEL_GAUSSIAN, &wb) !=
        SLWR_STATUS_OK) {
      return SLWR_STATUS_ERROR;
    }
    max_wb = wb > max_wb ? wb : max_wb;
  }

  if (gsl_vector_scale(delta, max_wb) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_add(c, delta) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }

  return slwr_vector_from_gsl_vector(n->rf->c, c) == SLWR_STATUS_OK
             ? SLWR_STATUS_OK
             : SLWR_STATUS_ERROR;
}

static slwr_status update_node_center(slwr_workspace *ws,
                                      const slwr_gng_context *ctx,
                                      slwr_gng_node *n, const gsl_vector *x,
                                      gdouble epsilon,
                                      const gchar *update_method) {
  if (node_center_update_method_is_vector_quantization(update_method)) {
    return gng_update_node_center(ws, n, x, epsilon);
  } else if (node_center_update_method_is_penalized_vector_quantization(
                 update_method)) {
    return update_node_center_with_penalized_vector_quantization(ws, ctx, n, x,
                                                                 epsilon);
  }
  g_critical("unknown node center update method %s", update_method);
  return SLWR_STATUS_ERROR;
}

static slwr_status update_neighborhood(slwr_workspace *ws,
                                       const slwr_gng_context *ctx,
                                       slwr_gng_node *n,
                                       const slwr_gng_parameters *params,
                                       const gsl_vector *x, GPtrArray *edges,
                                       const gchar *update_method) {
  if (update_node_center(ws, ctx, n, x, params->epsilon_n, update_method) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  for (gsize i = 0; i < edges->len; ++i) {
    const slwr_gng_edge *e = g_ptr_array_index(edges, i);
    if (e->a == n) {
      if (update_node_center(ws, ctx, e->b, x, params->epsilon_b,
                             update_method) != SLWR_STATUS_OK) {
        return SLWR_STATUS_ERROR;
      }
    }
    if (e->b == n) {
      if (update_node_center(ws, ctx, e->a, x, params->epsilon_b,
                             update_method) != SLWR_STATUS_OK) {
        return SLWR_STATUS_ERROR;
      }
    }
  }
  return SLWR_STATUS_OK;
}

slwr_gng_context *gng_adapter_context_alloc() {
  slwr_gng_context *ctx =
      (slwr_gng_context *)g_malloc0(sizeof(slwr_gng_context));
  if (!ctx) {
    return NULL;
  }

  ctx->nodes =
      g_ptr_array_new_with_free_func((GDestroyNotify)gng_adapter_node_free);
  ctx->edges =
      g_ptr_array_new_with_free_func((GDestroyNotify)slwr_gng_edge_free);
  ctx->neighborhood = g_ptr_array_new();

  return ctx;
}

slwr_gng_node *gng_adapter_node_alloc(slwr_receptive_field *rf) {
  slwr_gng_node *n = (slwr_gng_node *)g_malloc0(sizeof(slwr_gng_node));
  if (!n) {
    return NULL;
  }
  n->rf = rf;
  return n;
}

void gng_adapter_node_free(slwr_gng_node *node) {
  if (!node) {
    return;
  }
  g_free(node);
}

static slwr_status find_midpoint(const slwr_gng_node *a, const slwr_gng_node *b,
                                 slwr_vector *midpoint) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(midpoint != NULL, SLWR_STATUS_ERROR);

  slwr_status status;
  if ((status = slwr_vector_copy(midpoint, a->rf->c)) != SLWR_STATUS_OK) {
    return status;
  }
  if ((status = slwr_vector_add(midpoint, b->rf->c)) != SLWR_STATUS_OK) {
    return status;
  }
  if ((status = slwr_vector_scale(midpoint, 0.5)) != SLWR_STATUS_OK) {
    return status;
  }
  return SLWR_STATUS_OK;
}

gng_adapter_edge_with_least_activated_nodes_result *
gng_adapter_find_edge_with_least_activated_nodes(
    slwr_workspace *ws, const slwr_network_context *ctx,
    const slwr_model_population *population) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(ctx != NULL, NULL);

  const gsize n_in = ctx->network_params->n_in;
  gdouble midpoint_data[n_in];
  slwr_vector midpoint = {.size = n_in, .data = midpoint_data};
  gdouble minimum_activation = GSL_DBL_MAX;

  gng_adapter_edge_with_least_activated_nodes_result *result =
      g_malloc0(sizeof(gng_adapter_edge_with_least_activated_nodes_result));
  if (!result) {
    g_critical("cannot allocate memory for the return value");
    goto on_error;
  }

  const GPtrArray *edges = population->gng_ctx->edges;
  for (gsize i = 0; i < edges->len; ++i) {
    slwr_gng_edge *e = (slwr_gng_edge *)g_ptr_array_index(edges, i);
    if (find_midpoint(e->a, e->b, &midpoint) != SLWR_STATUS_OK) {
      g_critical(
          "cannot find the midpoint between two nodes connected by an edge");
      goto on_error;
    }
    if (determine_activation_slwr(ws, e->a->rf, &midpoint, SLWR_KERNEL_GAUSSIAN,
                                  &result->midpoint_activation_a) !=
        SLWR_STATUS_OK) {
      g_critical("cannot determine activation");
      goto on_error;
    }
    if (determine_activation_slwr(ws, e->b->rf, &midpoint, SLWR_KERNEL_GAUSSIAN,
                                  &result->midpoint_activation_b) !=
        SLWR_STATUS_OK) {
      g_critical("cannot determine activation");
      goto on_error;
    }
    if (result->midpoint_activation_a + result->midpoint_activation_b <
        minimum_activation) {
      result->edge = e;
      minimum_activation =
          result->midpoint_activation_a + result->midpoint_activation_b;
    }
  }
  if (!result->edge) {
    g_critical("cannot find least activated edge");
    goto on_error;
  }

  return result;

on_error:
  gng_adapter_least_activated_edge_result_free(result);
  return NULL;
}

void gng_adapter_least_activated_edge_result_free(
    gng_adapter_edge_with_least_activated_nodes_result *result) {
  if (!result) {
    return;
  }
  /* The result->edge pointer is non-owning */
  g_free(result);
}

/* TODO: rename this function to reflect what it is doing (updates the existing
 * gng network without node insertion and edge pruning) */
slwr_status gng_adapter_update(slwr_workspace *ws, slwr_network_context *ctx,
                               slwr_model_population *population,
                               slwr_gng_node *most_activated_node,
                               slwr_gng_node *second_most_activated_node,
                               const gsl_vector *x) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(population != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(most_activated_node != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(second_most_activated_node != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);

  GPtrArray *edges = population->gng_ctx->edges;
  gng_increment_edge_age(most_activated_node, edges);

  if (gng_accumulate_input_space_node_error(ws, most_activated_node, x) !=
      SLWR_STATUS_OK) {
    g_critical("could not accumulate the input space error");
    return SLWR_STATUS_ERROR;
  }

  if (update_neighborhood(
          ws, population->gng_ctx, most_activated_node, ctx->gng_params, x,
          edges, ctx->network_params->model_allocation) != SLWR_STATUS_OK) {
    g_critical("could not update the neighborhood");
    return SLWR_STATUS_ERROR;
  }

  if (gng_update_edge_between(most_activated_node, second_most_activated_node,
                              edges) != SLWR_STATUS_OK) {
    g_critical("could not update the edge between closest nodes");
    return SLWR_STATUS_ERROR;
  }

  return SLWR_STATUS_OK;
}

slwr_gng_node *gng_adapter_insert_node_between(
    slwr_workspace *ws, slwr_model_population *population,
    const slwr_gng_node *a, const slwr_gng_node *b, slwr_matrix *D_template) {
  return gng_insert_node_between(ws, population->gng_ctx, a, b, D_template);
}

slwr_gng_node *gng_adapter_insert_node_with_respect_to_input_error(
    slwr_workspace *ws, const slwr_network_context *ctx,
    slwr_model_population *population) {
  return gng_insert_node_with_respect_to_input_error(ws, population->gng_ctx,
                                                     ctx->gng_params);
}

slwr_gng_node *gng_adapter_insert_node_with_respect_to_output_error(
    slwr_workspace *ws, const slwr_network_context *ctx,
    slwr_model_population *population) {
  return gng_insert_node_with_respect_to_output_error(ws, population->gng_ctx,
                                                      ctx->gng_params);
}

slwr_status gng_adapter_remove_node(slwr_model_population *population,
                                    slwr_gng_node *node) {
  return gng_remove_node(population->gng_ctx, node);
}

slwr_status gng_adapter_prune_edges(slwr_network_context *ctx,
                                    slwr_model_population *population) {
  return gng_prune_edges(population->gng_ctx, ctx->gng_params);
}
