#include "context.h"

#include "slwr/common/slwr-workspace.h"
#include "slwr/utils/slwr-json-utils.h"

#include <gsl/gsl_math.h>

static slwr_network_model_statistics *
slwr_network_model_statistics_alloc(gsize n_in) {
  slwr_network_model_statistics *statistics =
      (slwr_network_model_statistics *)g_malloc0(
          sizeof(slwr_network_model_statistics));
  if (!statistics) {
    goto on_error;
  }
  statistics->n_data = slwr_vector_alloc(n_in);
  if (!statistics->n_data) {
    goto on_error;
  }
  statistics->lambda = slwr_vector_alloc(n_in);
  if (!statistics->lambda) {
    goto on_error;
  }
  return statistics;

on_error:
  free(statistics);
  return NULL;
}

static void
slwr_network_model_statistics_free(slwr_network_model_statistics *statistics) {
  if (!statistics) {
    return;
  }
  if (statistics->lambda) {
    slwr_vector_free(statistics->lambda);
  }
  if (statistics->n_data) {
    slwr_vector_free(statistics->n_data);
  }
  g_free(statistics);
}

static slwr_network_statistics *slwr_network_statistics_alloc(gsize n_in) {
  slwr_network_statistics *stats =
      (slwr_network_statistics *)g_malloc0(sizeof(slwr_network_statistics));
  if (!stats) {
    goto on_error;
  }
  stats->input_space_min = slwr_vector_alloc(n_in);
  if (!stats->input_space_min) {
    goto on_error;
  }
  stats->input_space_max = slwr_vector_alloc(n_in);
  if (!stats->input_space_max) {
    goto on_error;
  }
  stats->n_updates = 0;
  return stats;

on_error:
  if (stats->input_space_max) {
    slwr_vector_free(stats->input_space_max);
  }
  if (stats->input_space_min) {
    slwr_vector_free(stats->input_space_min);
  }
  g_free(stats);
  return NULL;
}

static void slwr_network_statistics_free(slwr_network_statistics *stats) {
  if (!stats) {
    return;
  }
  if (stats->input_space_max) {
    slwr_vector_free(stats->input_space_max);
  }
  if (stats->input_space_min) {
    slwr_vector_free(stats->input_space_min);
  }
  g_free(stats);
}

static void network_model_free(slwr_network_model *model) {
  if (!model) {
    return;
  }
  if (model->statistics) {
    slwr_network_model_statistics_free(model->statistics);
  }
  if (model->ols_ctx) {
    slwr_ols_context_free(model->ols_ctx);
  }
  if (model->rf) {
    slwr_receptive_field_free(model->rf);
  }
  if (model->pt_modify_params) {
    g_free(model->pt_modify_params);
  }
  g_free(model);
}

static inline void model_free(gpointer item, gpointer user_data) {
  network_model_free((slwr_network_model *)item);
}

static slwr_network_model *
network_model_alloc(gsize n_in, const slwr_ols_parameters *ols_params) {
  slwr_network_model *model = NULL;

  model = (slwr_network_model *)g_malloc0(sizeof(slwr_network_model));
  if (!model) {
    goto on_error;
  }
  model->rf = slwr_receptive_field_alloc(n_in);
  if (!model->rf) {
    goto on_error;
  }
  model->ols_ctx = slwr_ols_context_alloc(ols_params);
  if (!model->ols_ctx) {
    goto on_error;
  }
  return model;

on_error:
  if (model) {
    network_model_free(model);
  }
  return NULL;
}

static void model_population_free(slwr_model_population *population) {
  if (!population) {
    return;
  }
  if (population->second_highest_activation) {
    g_free(population->second_highest_activation);
  }
  if (population->highest_activation) {
    g_free(population->highest_activation);
  }
  if (population->available_models) {
    g_ptr_array_foreach(population->available_models, model_free, NULL);
    g_ptr_array_free(population->available_models, TRUE);
  }
  if (population->allocated_models) {
    g_ptr_array_foreach(population->allocated_models, model_free, NULL);
    g_ptr_array_free(population->allocated_models, TRUE);
  }
  if (population->allocation_helper) {
    slwr_allocation_helper_free(population->allocation_helper);
  }
  if (population->gng_ctx) {
    slwr_gng_context_free(population->gng_ctx);
  }
  g_free(population);
}

static slwr_model_population *
model_population_alloc(const slwr_network_context *ctx) {
  slwr_model_population *population =
      (slwr_model_population *)g_malloc0(sizeof(slwr_model_population));
  if (!population) {
    goto on_error;
  }

  population->available_models =
      g_ptr_array_sized_new(ctx->network_params->max_models);
  population->allocated_models =
      g_ptr_array_sized_new(ctx->network_params->max_models);
  for (gsize i = 0; i < ctx->network_params->max_models; ++i) {
    slwr_network_model *model =
        network_model_alloc(ctx->network_params->n_in, ctx->ols_params);
    if (!model) {
      goto on_error;
    }
    slwr_matrix_copy(model->rf->D, ctx->network_params->D_init);

    model->pt_modify_params = (slwr_pt_modify_parameters *)g_malloc0(
        sizeof(slwr_pt_modify_parameters));
    if (!model->pt_modify_params) {
      goto on_error;
    }
    model->pt_modify_params->n_in = ctx->pt_modify_params->n_in;
    model->pt_modify_params->epsilon = ctx->pt_modify_params->epsilon;
    model->pt_modify_params->s = ctx->pt_modify_params->s;
    model->pt_modify_params->error_max = ctx->network_params->decay_error_max
                                             ? GSL_NAN
                                             : ctx->pt_modify_params->error_max;
    model->pt_modify_params->dist_max = ctx->pt_modify_params->dist_max;
    model->error_max_decay_N0 = GSL_NAN;
    model->n_rf_updates = 0;
    model->gng_node = NULL;
    model->last_activation = 0;
    /* TODO: is this really the place for allocating the model statistics? Why
     * are they, as well as gng_node and other fields, not allocated in
     * network_model_alloc()? */
    model->statistics =
        slwr_network_model_statistics_alloc(ctx->network_params->n_in);
    if (!model->statistics) {
      goto on_error;
    }
    /* TODO: whose responsibility is to initialize the model statistics? */
    model->statistics->lambda_init = ctx->network_params->lambda_init;
    model->statistics->lambda_final = ctx->network_params->lambda_final;
    slwr_vector_set_all(model->statistics->lambda,
                        ctx->network_params->lambda_init);
    slwr_vector_set_all(model->statistics->n_data, 1e-10);

    slwr_incremental_weighted_mean_reset(&model->mean_error);

    g_ptr_array_add(population->available_models, model);
  }

  population->allocation_helper = slwr_allocation_helper_alloc();
  population->gng_ctx = NULL;
  population->error_guided_insertion_single_model_present = FALSE;
  population->highest_activation = (slwr_allocation_helper_result *)g_malloc0(
      sizeof(slwr_allocation_helper_result));
  if (!population->highest_activation) {
    goto on_error;
  }
  population->second_highest_activation =
      (slwr_allocation_helper_result *)g_malloc0(
          sizeof(slwr_allocation_helper_result));
  if (!population->second_highest_activation) {
    goto on_error;
  }

  return population;

on_error:
  if (population) {
    model_population_free(population);
  }
  return NULL;
}

static inline void population_free(gpointer item, gpointer user_data) {
  model_population_free((slwr_model_population *)item);
}

static void network_context_free(slwr_network_context *ctx) {
  if (!ctx) {
    return;
  }
  if (ctx->stats) {
    slwr_network_statistics_free(ctx->stats);
  }
  if (ctx->ols_params) {
    g_free(ctx->ols_params);
  }
  if (ctx->pt_modify_params) {
    g_free(ctx->pt_modify_params);
  }
  if (ctx->gng_params) {
    g_free(ctx->gng_params);
  }
  if (ctx->populations) {
    g_ptr_array_foreach(ctx->populations, population_free, NULL);
    g_ptr_array_free(ctx->populations, TRUE);
  }
  if (ctx->network_params) {
    slwr_network_parameters_free(ctx->network_params);
  }
  g_free(ctx);
}

slwr_status ols_context_copy(slwr_ols_context *to,
                             const slwr_ols_context *from) {
  g_return_val_if_fail(to != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(from != NULL, SLWR_STATUS_ERROR);

  slwr_status status;
  if ((status = slwr_matrix_copy(to->A, from->A) != SLWR_STATUS_OK)) {
    return status;
  }
  if ((status = slwr_vector_copy(to->w_out, from->w_out) != SLWR_STATUS_OK)) {
    return status;
  }
  return SLWR_STATUS_OK;
}

void slwr_network_parameters_default_values(gsize n_in,
                                            slwr_network_parameters *params) {
  g_return_if_fail(params != NULL);
  g_return_if_fail(params->D_init != NULL);
  g_return_if_fail(params->D_init->size1 == params->D_init->size2);
  g_return_if_fail(params->D_init->size1 == n_in);

  params->n_in = n_in;
  slwr_matrix_set_identity(params->D_init);
  params->max_models = 500;
  params->rf_update_kind = SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_OFF;
  if (params->model_allocation) {
    g_free(params->model_allocation);
  }
  params->model_allocation = g_strdup("default-lwpr");
  params->decay_error_max = TRUE;
  params->apply_strict_decay_error_max = FALSE;
  params->decay_error_max_tau = 1000.0;
  params->error_max = 0.100;
  params->model_pruning = TRUE;
  if (params->error_guided_insertion_error_source) {
    g_free(params->error_guided_insertion_error_source);
  }
  params->error_guided_insertion_error_source =
      g_strdup("minimally-activated-models");
  params->w_gen = 0.200;
  params->w_prune = 0.700;
  params->lambda_init = 0.999;
  params->lambda_tau = 0.9999;
  params->lambda_final = 0.99999;
}

slwr_network_context *
slwr_network_context_alloc_from_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  slwr_network_context *ctx = NULL;
  slwr_ols_parameters *ols_params = NULL;
  slwr_pt_modify_parameters *pt_modify_params = NULL;
  slwr_gng_parameters *gng_params = NULL;
  slwr_network_parameters *network_params = NULL;

  ols_params = slwr_ols_parameters_from_json_string(json);
  if (!ols_params) {
    goto on_error;
  }
  pt_modify_params = slwr_pt_modify_parameters_from_json_string(json);
  if (!pt_modify_params) {
    goto on_error;
  }
  gng_params = slwr_gng_parameters_from_json_string(json);
  if (!gng_params) {
    goto on_error;
  }
  ctx = (slwr_network_context *)g_malloc0(sizeof(slwr_network_context));
  if (!ctx) {
    goto on_error;
  }
  ctx->ols_params = ols_params;
  ctx->pt_modify_params = pt_modify_params;
  ctx->gng_params = gng_params;

  network_params = slwr_network_parameters_from_json_string(json);
  if (!network_params) {
    goto on_error;
  }
  ctx->network_params = network_params;

  ctx->populations = g_ptr_array_sized_new(network_params->n_out);

  ctx->stats = slwr_network_statistics_alloc(network_params->n_in);
  if (!ctx->stats) {
    goto on_error;
  }

  for (gsize i = 0; i < network_params->n_out; ++i) {
    slwr_model_population *population = model_population_alloc(ctx);
    if (!population) {
      goto on_error;
    }
    g_ptr_array_add(ctx->populations, population);
  }

  return ctx;

on_error:
  if (network_params) {
    slwr_network_parameters_free(network_params);
    network_params = NULL;
  }
  if (gng_params) {
    g_free(gng_params);
    gng_params = NULL;
  }
  if (pt_modify_params) {
    g_free(pt_modify_params);
    pt_modify_params = NULL;
  }
  if (ols_params) {
    g_free(ols_params);
    ols_params = NULL;
  }
  if (!ctx) {
    network_context_free(ctx);
  }
  return NULL;
}

void slwr_network_context_free(slwr_network_context *ctx) {
  if (!ctx) {
    return;
  }
  network_context_free(ctx);
}

void slwr_network_parameters_free(slwr_network_parameters *params) {
  if (!params) {
    return;
  }
  if (params->D_init) {
    slwr_matrix_free(params->D_init);
  }
  if (params->model_allocation) {
    g_free(params->model_allocation);
  }
  if (params->error_guided_insertion_error_source) {
    g_free(params->error_guided_insertion_error_source);
  }
  g_free(params);
}
