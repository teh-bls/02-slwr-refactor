#pragma once

#include "slwr/network/slwr-network.h"

slwr_status
slwr_network_model_statistics_update(slwr_network_model_statistics *statistics);
