#pragma once

#include "slwr/common/slwr-workspace.h"
#include "slwr/network/slwr-network.h"

slwr_status single_model_predict(slwr_workspace *ws,
                                 const slwr_network_context *ctx,
                                 const slwr_network_model *model,
                                 const slwr_vector *x, gdouble *y_pred);

