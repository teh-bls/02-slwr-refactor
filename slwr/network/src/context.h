#pragma once

#include "slwr/network/slwr-network.h"

slwr_status ols_context_copy(slwr_ols_context *to,
                             const slwr_ols_context *from);
