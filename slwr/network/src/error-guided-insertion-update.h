#pragma once

#include "slwr/network/slwr-network.h"

#include <glib.h>

gboolean
error_guided_insertion_should_insert_model(const slwr_network_context *ctx);

slwr_status
error_guided_insertion_create_gng_context(slwr_model_population *population,
                                          slwr_network_model *m1,
                                          slwr_network_model *m2);

slwr_status
error_guided_insertion_setup_gng_network(const slwr_network_context *ctx,
                                         slwr_model_population *population);

slwr_status
error_guided_insertion_insert_model(slwr_workspace *ws,
                                    slwr_network_context *ctx,
                                    slwr_model_population *population);

slwr_status error_guided_insertion_prune_models_and_edges(
    slwr_network_context *ctx, slwr_model_population *population);

slwr_status error_guided_insertion_gng_update(slwr_workspace *ws,
                                              slwr_network_context *ctx,
                                              slwr_model_population *population,
                                              const slwr_vector *x);
