#pragma once

#include "slwr/network/slwr-network.h"

#include <glib.h>

gboolean receptive_field_update_on(const slwr_network_context *ctx);

gboolean error_guided_insertion_error_source_is_input_space(
    const slwr_network_context *ctx);

gboolean error_guided_insertion_error_source_is_output_space(
    const slwr_network_context *ctx);

gboolean error_guided_insertion_error_source_is_minimally_activated_models(
    const slwr_network_context *ctx);

gboolean model_allocation_is_default_lwpr(const slwr_network_context *ctx);

gboolean model_allocation_is_error_guided_insertion_and_vector_quantization(
    const slwr_network_context *ctx);

gboolean
model_allocation_is_error_guided_insertion_and_penalized_vector_quantization(
    const slwr_network_context *ctx);

/* TODO: replace gchar *update_method with const slwr_network_context *ctx */
gboolean
node_center_update_method_is_vector_quantization(const gchar *update_method);

/* TODO: replace gchar *update_method with const slwr_network_context *ctx */
gboolean node_center_update_method_is_penalized_vector_quantization(
    const gchar *update_method);
