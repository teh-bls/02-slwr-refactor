#include "parameter-utils.h"

#include "slwr/utils/slwr-str-utils.h"

gboolean receptive_field_update_on(const slwr_network_context *ctx) {
  return ctx->network_params->rf_update_kind !=
         SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_OFF;
}

gboolean error_guided_insertion_error_source_is_input_space(
    const slwr_network_context *ctx) {
  return slwr_str_equal(
      ctx->network_params->error_guided_insertion_error_source, "input-space");
}

gboolean error_guided_insertion_error_source_is_output_space(
    const slwr_network_context *ctx) {
  return slwr_str_equal(
      ctx->network_params->error_guided_insertion_error_source, "output-space");
}

gboolean error_guided_insertion_error_source_is_minimally_activated_models(
    const slwr_network_context *ctx) {
  return slwr_str_equal(
      ctx->network_params->error_guided_insertion_error_source,
      "minimally-activated-models");
}

gboolean model_allocation_is_default_lwpr(const slwr_network_context *ctx) {
  return slwr_str_equal(ctx->network_params->model_allocation, "default-lwpr");
}

gboolean model_allocation_is_error_guided_insertion_and_vector_quantization(
    const slwr_network_context *ctx) {
  return slwr_str_equal(ctx->network_params->model_allocation,
                        "error-guided-insertion-and-vector-quantization");
}

gboolean
model_allocation_is_error_guided_insertion_and_penalized_vector_quantization(
    const slwr_network_context *ctx) {
  return slwr_str_equal(
      ctx->network_params->model_allocation,
      "error-guided-insertion-and-penalized-vector-quantization");
}

gboolean
node_center_update_method_is_vector_quantization(const gchar *update_method) {
  return slwr_str_equal(update_method,
                        "error-guided-insertion-and-vector-quantization");
}

gboolean node_center_update_method_is_penalized_vector_quantization(
    const gchar *update_method) {
  return slwr_str_equal(
      update_method,
      "error-guided-insertion-and-penalized-vector-quantization");
}
