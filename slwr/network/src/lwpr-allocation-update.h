#pragma once

#include "slwr/common/slwr-workspace.h"
#include "slwr/network/slwr-network.h"

slwr_status lwpr_allocation_allocate_model(slwr_network_context *ctx,
                                           slwr_model_population *population,
                                           const slwr_vector *x);

slwr_status lwpr_allocation_prune_model(slwr_network_context *ctx,
                                        slwr_model_population *population);
