#pragma once

#include "slwr/network/slwr-network.h"

#include <glib.h>

slwr_status population_store_highest_and_second_highest_activations(
    slwr_model_population *population);

slwr_status population_remove_model_at(slwr_network_context *ctx,
                                       slwr_model_population *population,
                                       gsize i);

slwr_status
population_remove_model_from_population(slwr_network_context *ctx,
                                        slwr_model_population *population,
                                        const slwr_network_model *model);

slwr_network_model *
population_add_model_to_population(const slwr_model_population *population);

slwr_network_model *
population_add_model_centered_at(slwr_model_population *population,
                                 const slwr_vector *c);

slwr_network_model *population_add_model_with_D(
    slwr_network_context *ctx, slwr_model_population *population,
    const slwr_vector *c, const slwr_matrix *D_template);

gboolean population_should_prune_model(const slwr_network_context *ctx,
                                       const slwr_model_population *population);
