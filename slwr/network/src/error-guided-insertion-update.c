#include "error-guided-insertion-update.h"

#include "context.h"
#include "gng-adapter.h"
#include "parameter-utils.h"
#include "population.h"

#include "slwr/common/src/workspace.h"
#include "slwr/gng/src/gng.h"

gboolean
error_guided_insertion_should_insert_model(const slwr_network_context *ctx) {
  return ctx->stats->n_updates > 0 &&
         ctx->stats->n_updates % ctx->gng_params->insertion_interval == 0;
}

slwr_status
error_guided_insertion_create_gng_context(slwr_model_population *population,
                                          slwr_network_model *m1,
                                          slwr_network_model *m2) {
  population->gng_ctx = gng_adapter_context_alloc();
  if (!population->gng_ctx) {
    g_critical("could not allocate GNG context");
    return SLWR_STATUS_ERROR;
  }

  slwr_gng_node *n1 = gng_adapter_node_alloc(m1->rf);
  if (!n1) {
    g_critical("could not allocate an initial GNG node");
    return SLWR_STATUS_ERROR;
  }
  slwr_gng_node *n2 = gng_adapter_node_alloc(m2->rf);
  if (!n2) {
    g_critical("could not allocate an initial GNG node");
    return SLWR_STATUS_ERROR;
  }
  g_ptr_array_add(population->gng_ctx->nodes, n1);
  g_ptr_array_add(population->gng_ctx->nodes, n2);
  m1->gng_node = n1;
  m2->gng_node = n2;

  slwr_gng_edge *e = slwr_gng_edge_alloc();
  if (!e) {
    g_critical("could not allocate an initial GNG edge");
    return SLWR_STATUS_ERROR;
  }
  e->a = n1;
  e->b = n2;
  g_ptr_array_add(population->gng_ctx->edges, e);

  return SLWR_STATUS_OK;
}

slwr_status
error_guided_insertion_setup_gng_network(const slwr_network_context *ctx,
                                         slwr_model_population *population) {
  slwr_network_model *m1 = NULL;
  slwr_network_model *m2 = NULL;
  const slwr_vector *min = ctx->stats->input_space_min;
  const slwr_vector *max = ctx->stats->input_space_max;
  const gsize n_in = ctx->network_params->n_in;

  const gsize num_models = population->allocated_models->len;
  if (num_models != 1) {
    g_critical(
        "there should be exactly 1 model (there are currently %lu models)",
        num_models);
    return SLWR_STATUS_ERROR;
  }

  m1 = (slwr_network_model *)g_ptr_array_index(population->allocated_models, 0);
  m2 = population_add_model_to_population(population);
  if (!m2) {
    g_critical(
        "could not add the second model while setting up the GNG network");
    return SLWR_STATUS_ERROR;
  }
  for (gsize i = 0; i < n_in; ++i) {
    m1->rf->c->data[i] = g_random_double_range(min->data[i], max->data[i]);
    m2->rf->c->data[i] = g_random_double_range(min->data[i], max->data[i]);
  }
  if (slwr_matrix_copy(m2->rf->D, m1->rf->D) != SLWR_STATUS_OK) {
    g_critical("could not initialize the second model while setting up the GNG "
               "network");
    return SLWR_STATUS_ERROR;
  }

  return error_guided_insertion_create_gng_context(population, m1, m2);
}

static slwr_status insert_node_between_least_activated_nodes(
    slwr_workspace *ws, const slwr_network_context *ctx,
    slwr_model_population *population, slwr_gng_node **new_node) {
  slwr_status status = SLWR_STATUS_OK;
  gng_adapter_edge_with_least_activated_nodes_result *r = NULL;
  *new_node = NULL;

  r = gng_adapter_find_edge_with_least_activated_nodes(ws, ctx, population);
  if (!r) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  const gdouble w_gen = ctx->network_params->w_gen;
  if (r->midpoint_activation_a + r->midpoint_activation_b > w_gen) {
    /* No need to insert a node */
    status = SLWR_STATUS_OK;
    goto end;
  }

  *new_node = gng_adapter_insert_node_between(
      ws, population, r->edge->a, r->edge->b,
      r->midpoint_activation_a > r->midpoint_activation_b ? r->edge->a->rf->D
                                                          : r->edge->b->rf->D);
  if (!*new_node) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }

end:
  if (r) {
    gng_adapter_least_activated_edge_result_free(r);
  }
  return status;
}

slwr_status
error_guided_insertion_insert_model(slwr_workspace *ws,
                                    slwr_network_context *ctx,
                                    slwr_model_population *population) {
  slwr_gng_node *node = NULL;
  if (error_guided_insertion_error_source_is_input_space(ctx)) {
    node = gng_adapter_insert_node_with_respect_to_input_error(ws, ctx,
                                                               population);
  } else if (error_guided_insertion_error_source_is_output_space(ctx)) {
    node = gng_adapter_insert_node_with_respect_to_output_error(ws, ctx,
                                                                population);
  } else if (error_guided_insertion_error_source_is_minimally_activated_models(
                 ctx)) {
    slwr_status status =
        insert_node_between_least_activated_nodes(ws, ctx, population, &node);
    if (status == SLWR_STATUS_OK && !node) {
      /* w_gen exceeded, no model needs to be inserted yet */
      return SLWR_STATUS_OK;
    }
  }
  if (!node) {
    g_critical("could not insert a new GNG node");
    return SLWR_STATUS_ERROR;
  }
  const slwr_network_model *highest_activation_model =
      (const slwr_network_model *)g_ptr_array_index(
          population->allocated_models, population->highest_activation->index);
  slwr_network_model *model = population_add_model_with_D(
      ctx, population, node->rf->c, highest_activation_model->rf->D);
  if (!model) {
    g_critical("could not insert a new model");
    return SLWR_STATUS_ERROR;
  }
  if (ols_context_copy(model->ols_ctx, highest_activation_model->ols_ctx) !=
      SLWR_STATUS_OK) {
    /* TODO: I think that I would need to free the node in this case before
     * returning SLWR_STATUS_ERROR. */
    g_critical("could not initialize the OLS context of a new model");
    return SLWR_STATUS_ERROR;
  }

  /* TODO: resolve the non-owning receptive field situation differently */
  slwr_receptive_field_free(node->rf);
  node->rf = model->rf;

  model->gng_node = node;
  return SLWR_STATUS_OK;
}

static slwr_status
error_guided_insertion_prune_model(slwr_network_context *ctx,
                                   slwr_model_population *population) {
  if (!population_should_prune_model(ctx, population)) {
    return SLWR_STATUS_OK;
  }

  slwr_status status;
  gsize removal_index = population->second_highest_activation->index;
  slwr_network_model *m =
      g_ptr_array_index(population->allocated_models, removal_index);
  if (!m) {
    return SLWR_STATUS_NOT_FOUND;
  }
  if ((status = gng_adapter_remove_node(population, m->gng_node)) !=
      SLWR_STATUS_OK) {
    g_critical("cannot remove the GNG node associated with pruned model");
    return status;
  }
  if ((status = population_remove_model_at(ctx, population, removal_index)) !=
      SLWR_STATUS_OK) {
    return status;
  }
  return SLWR_STATUS_OK;
}

/**
 *
 * Prunes the GNG edges unconditionally (those whose ages exceed a maximum
 * value) and, if requested, uses PRUNE-MODELS-AND-EDGES from the report.
 *
 * @param ctx
 * @param population
 * @return
 */
slwr_status error_guided_insertion_prune_models_and_edges(
    slwr_network_context *ctx, slwr_model_population *population) {
  slwr_status status;
  /* Prune old GNG edges unconditionally */
  if ((status = gng_adapter_prune_edges(ctx, population)) != SLWR_STATUS_OK) {
    g_critical("could not prune GNG edges");
    return status;
  }
  if (ctx->network_params->model_pruning) {
    return error_guided_insertion_prune_model(ctx, population);
  }
  return SLWR_STATUS_OK;
}

slwr_status error_guided_insertion_gng_update(slwr_workspace *ws,
                                              slwr_network_context *ctx,
                                              slwr_model_population *population,
                                              const slwr_vector *x) {
  slwr_network_model *most_activated_model =
      (slwr_network_model *)g_ptr_array_index(
          population->allocated_models, population->highest_activation->index);
  if (!most_activated_model) {
    return SLWR_STATUS_ERROR;
  }
  slwr_network_model *second_most_activated_model =
      (slwr_network_model *)g_ptr_array_index(
          population->allocated_models,
          population->second_highest_activation->index);
  if (!second_most_activated_model) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    return SLWR_STATUS_ERROR;
  }
  return gng_adapter_update(ws, ctx, population, most_activated_model->gng_node,
                            second_most_activated_model->gng_node, gx);
}
