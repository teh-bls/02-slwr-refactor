#include "population.h"

#include "slwr/common/src/workspace.h"

#include <gsl/gsl_math.h>

/**
 * A helper that stores the highest and second highest activations of receptive
 * field in the given population to the corresponding fields of that population
 * structure.
 *
 * @param population The population structure to query and store the highest and
 * second highest activations to.
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR on failure.
 */
slwr_status population_store_highest_and_second_highest_activations(
    slwr_model_population *population) {
  slwr_status status;
  if ((status = slwr_allocation_helper_get_highest_activation(
           population->allocation_helper, population->highest_activation)) !=
      SLWR_STATUS_OK) {
    return status;
  }
  if ((status = slwr_allocation_helper_get_second_highest_activation(
           population->allocation_helper,
           population->second_highest_activation)) != SLWR_STATUS_OK) {
    return status;
  }
  return SLWR_STATUS_OK;
}

/**
 * Removes the model at index i from the model population.
 *
 * @param ctx The network context.
 * @param population The model population.
 * @param i Index of the model to be removed.
 * @return SLWR_STATUS_OK if the model was removed, SLWR_STATUS_NOT_FOUND
 * otherwise.
 */
slwr_status population_remove_model_at(slwr_network_context *ctx,
                                       slwr_model_population *population,
                                       gsize i) {
  slwr_network_model *model = (slwr_network_model *)g_ptr_array_remove_index(
      population->allocated_models, i);
  if (!model) {
    g_critical(
        "requested model at index %lu not found in the population of size %d",
        i, population->allocated_models->len);
    return SLWR_STATUS_NOT_FOUND;
  }
  /* Return the model to the population initialized */
  slwr_matrix_copy(model->rf->D, ctx->network_params->D_init);
  slwr_vector_set_zero(model->rf->c);
  model->pt_modify_params->error_max = ctx->network_params->decay_error_max
                                           ? GSL_NAN
                                           : ctx->pt_modify_params->error_max;
  model->error_max_decay_N0 = GSL_NAN;
  g_ptr_array_add(population->available_models, model);
  return SLWR_STATUS_OK;
}

/**
 * Removes a model from the given model population.
 *
 * @param ctx The network context.
 * @param population The model population.
 * @param model Model to remove.
 * @return SLWR_STATUS_OK if the model was removed, SLWR_STATUS_NOT_FOUND
 * otherwise.
 */
slwr_status
population_remove_model_from_population(slwr_network_context *ctx,
                                        slwr_model_population *population,
                                        const slwr_network_model *model) {
  guint i = 0;
  if (!g_ptr_array_find(population->allocated_models, model, &i)) {
    return SLWR_STATUS_NOT_FOUND;
  }
  return population_remove_model_at(ctx, population, i);
}

/**
 * Adds a model to the given model population.
 *
 * Assumes that models in population->available_models have their D_init fields
 * initialized to a meaningful value.
 *
 * @param population The model population.
 * @return A non-owning pointer to the newly added model or NULL if no model was
 * added.
 */
slwr_network_model *
population_add_model_to_population(const slwr_model_population *population) {
  slwr_network_model *model =
      (slwr_network_model *)g_ptr_array_remove_index_fast(
          population->available_models, population->available_models->len - 1);
  if (model) {
    g_ptr_array_add(population->allocated_models, model);
  } else {
    g_critical("maximum number of models reached");
  }
  return model;
}

/**
 * Adds a model to the given model population and centers it at c.
 *
 * Assumes that models in population->available_models had their D_init fields
 * initialized to a meaningful value.
 *
 * @param population The model population.
 * @param c The center of the new model.
 * @return A non-owning pointer to the newly added model or NULL if no model has
 * been added.
 */
slwr_network_model *
population_add_model_centered_at(slwr_model_population *population,
                                 const slwr_vector *c) {
  slwr_network_model *model = population_add_model_to_population(population);
  if (model == NULL) {
    g_critical("could not add model");
    goto end;
  }
  if (slwr_vector_copy(model->rf->c, c) != SLWR_STATUS_OK) {
    g_critical(
        "failed to center model at the given point, centering it at zero");
    slwr_vector_set_zero(model->rf->c);
  }
end:
  return model;
}

/**
 * Adds a model to the model population, centers it at c, and sets its D matrix
 * to the given D matrix template.
 *
 * @param ctx The network context.
 * @param population The model population.
 * @param c The center of the new model.
 * @param D_template Template to be used for new model's D matrix.
 * @return A non-owning pointer to the newly added model or NULL if no model has
 * been added.
 */
slwr_network_model *population_add_model_with_D(
    slwr_network_context *ctx, slwr_model_population *population,
    const slwr_vector *c, const slwr_matrix *D_template) {
  slwr_network_model *model = population_add_model_centered_at(population, c);
  if (model) {
    if (slwr_matrix_copy(model->rf->D, D_template) != SLWR_STATUS_OK) {
      g_critical("could not initialize the receptive field from template");
      population_remove_model_from_population(ctx, population, model);
      model = NULL;
    }
  }
  return model;
}

gboolean
population_should_prune_model(const slwr_network_context *ctx,
                              const slwr_model_population *population) {
  const gdouble w_prune = ctx->network_params->w_prune;
  return population->highest_activation->activation > w_prune &&
         population->second_highest_activation->activation > w_prune;
}
