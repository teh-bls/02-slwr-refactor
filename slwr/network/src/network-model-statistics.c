#include "network-model-statistics.h"

#include <slwr/common/slwr-workspace.h>

slwr_status slwr_network_model_statistics_update(
    slwr_network_model_statistics *statistics) {
  g_return_val_if_fail(statistics != NULL, SLWR_STATUS_ERROR);
  slwr_status status;
  if ((status = slwr_vector_mul(statistics->n_data, statistics->lambda)) !=
      SLWR_STATUS_OK) {
    return status;
  }
  if ((status = slwr_vector_add_constant(statistics->n_data, 1.0)) !=
      SLWR_STATUS_OK) {
    return status;
  }
  if ((status = slwr_vector_scale(statistics->lambda,
                                  statistics->lambda_init)) != SLWR_STATUS_OK) {
    return status;
  }
  if ((status = slwr_vector_add_constant(statistics->lambda,
                                         statistics->lambda_final *
                                             (1 - statistics->lambda_init))) !=
      SLWR_STATUS_OK) {
    return status;
  }
  return SLWR_STATUS_OK;
}
