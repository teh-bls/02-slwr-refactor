#pragma once

#include "slwr/common/slwr-types.h"

typedef struct {
  slwr_matrix *D;
  slwr_vector *c;
} slwr_receptive_field;

slwr_receptive_field *slwr_receptive_field_alloc(gsize n_in);

void slwr_receptive_field_free(slwr_receptive_field *rf);

slwr_status slwr_mahalanobis_distance(slwr_workspace *ws,
                                      const slwr_receptive_field *rf,
                                      const slwr_vector *x, double *distance);

slwr_status slwr_determine_activation(slwr_workspace *ws,
                                      const slwr_receptive_field *rf,
                                      const slwr_vector *x,
                                      slwr_kernel_type kernel,
                                      double *activation);
