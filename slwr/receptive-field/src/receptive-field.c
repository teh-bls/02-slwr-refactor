#include "receptive-field.h"

#include "slwr/common/src/workspace.h"
#include "slwr/receptive-field/slwr-receptive-field.h"
#include "slwr/utils/src/math-utils.h"

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

/**
 * Allocates a new receptive field and initializes its c to 0 and its D to the
 * identity matrix.
 *
 * @param n_in Input space dimensionality.
 * @return The allocated receptive field on success, NULL on failure.
 */
slwr_receptive_field *slwr_receptive_field_alloc(gsize n_in) {
  slwr_receptive_field *rf =
      (slwr_receptive_field *)g_malloc0(sizeof(slwr_receptive_field));
  if (!rf) {
    goto on_error;
  }
  rf->D = slwr_matrix_alloc(n_in, n_in);
  if (!rf->D) {
    goto on_error;
  }
  for (gsize i = 0; i < n_in; ++i) {
    rf->D->data[n_in * i + i] = 1.0;
  }
  rf->c = slwr_vector_alloc(n_in);
  if (!rf->c) {
    goto on_error;
  }
  return rf;

on_error:
  if (rf) {
    slwr_receptive_field_free(rf);
  }
  return NULL;
}

void slwr_receptive_field_free(slwr_receptive_field *rf) {
  if (!rf) {
    return;
  }
  slwr_vector_free(rf->c);
  slwr_matrix_free(rf->D);
  g_free(rf);
}

/**
 * Calculates the Mahalanobis distance from the point x and the receptive field
 * rf.
 *
 * Resets the workspace.
 *
 * @param ws The slwr workspace.
 * @param rf The receptive field.
 * @param x The point x.
 * @param distance (in/out) The Mahalanobis distance.
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR otherwise.
 */
slwr_status slwr_mahalanobis_distance(slwr_workspace *ws,
                                      const slwr_receptive_field *rf,
                                      const slwr_vector *x, gdouble *distance) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf->c->size == x->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(rf->D->size1 == x->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(rf->D->size2 == x->size, SLWR_STATUS_DIMENSION_MISMATCH);

  slwr_status status = SLWR_STATUS_OK;
  gsl_vector *c = NULL;
  gsl_matrix *D = NULL;
  gsl_vector *gx = NULL;

  slwr_workspace_reset(ws);

  c = workspace_gsl_vector_from_slwr_vector(ws, rf->c, WORKSPACE_N_IN);
  if (!c) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  D = workspace_gsl_matrix_from_slwr_matrix(ws, rf->D, WORKSPACE_N_IN);
  if (!D) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }

  if (mahalanobis_distance_gsl(ws, D, c, gx, distance) != SLWR_STATUS_OK) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }

end:
  slwr_workspace_reset(ws);
  return status;
}

/**
 * Calculates the Mahalanobis distance from the point x and the receptive field
 * described by the matrix D and centered at c.
 *
 * Does not reset the workspace.
 *
 * @param ws The slwr workspace.
 * @param D The receptive field's D matrix.
 * @param c The center of the receptive field.
 * @param x The point x.
 * @param distance (in/out) The Mahalanobis distance.
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR otherwise.
 */
slwr_status mahalanobis_distance_gsl(slwr_workspace *ws, const gsl_matrix *D,
                                     const gsl_vector *c, const gsl_vector *x,
                                     gdouble *distance) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(D != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);

  gsl_vector *v = workspace_vector(ws, WORKSPACE_N_IN);
  if (!v) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *xc = workspace_vector(ws, WORKSPACE_N_IN);
  if (!xc) {
    return SLWR_STATUS_ERROR;
  }
  if (utils_vector_difference(x, c, xc) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_dgemv(CblasNoTrans, 1.0, D, xc, 0, v) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_ddot(xc, v, distance) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

/**
 * Determines the activation of the receptive field by input x from slwr
 * parameters.
 *
 * Does not reset the workspace.
 *
 * @param ws The slwr workspace.
 * @param rf Receptive field that is activated.
 * @param x The input that activates the receptive field.
 * @param kernel Kernel type, either SLWR_KERNEL_GAUSSIAN or
 * SLWR_KERNEL_BISQUARE.
 * @param activation (in/out) The activation.
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR otherwise.
 */
slwr_status determine_activation_slwr(slwr_workspace *ws,
                                      const slwr_receptive_field *rf,
                                      const slwr_vector *x,
                                      slwr_kernel_type kernel,
                                      double *activation) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);

  gsl_matrix *D =
      workspace_gsl_matrix_from_slwr_matrix(ws, rf->D, WORKSPACE_N_IN);
  if (!D) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *c =
      workspace_gsl_vector_from_slwr_vector(ws, rf->c, WORKSPACE_N_IN);
  if (!c) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    return SLWR_STATUS_ERROR;
  }
  return determine_activation_gsl(ws, D, c, gx, kernel, activation);
}

/**
 * Determines the activation of the receptive field described by D and centered
 * at c by input x from gsl parameters.
 *
 * Does not reset the workspace.
 *
 * @param ws The slwr workspace.
 * @param D Matrix encoding the receptive field.
 * @param c Center of the receptive field.
 * @param x The input that activates the receptive field.
 * @param kernel Kernel type, either SLWR_KERNEL_GAUSSIAN or
 * SLWR_KERNEL_BISQUARE.
 * @param activation (in/out) The activation.
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR otherwise.
 */
slwr_status determine_activation_gsl(slwr_workspace *ws, const gsl_matrix *D,
                                     const gsl_vector *c, const gsl_vector *x,
                                     slwr_kernel_type kernel,
                                     double *activation) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(D != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);

  gdouble distance;
  if (mahalanobis_distance_gsl(ws, D, c, x, &distance) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (kernel == SLWR_KERNEL_GAUSSIAN) {
    *activation = exp(-0.5 * distance);
  } else if (kernel == SLWR_KERNEL_BISQUARE) {
    gdouble e = 1 - 0.25 * distance;
    *activation = e < 0 ? 0.0 : gsl_pow_2(e);
  } else {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

/**
 * Determines the activation of the receptive field by input x.
 *
 * Resets the workspace.
 *
 * @param ws The slwr workspace.
 * @param rf Receptive field that is activated.
 * @param x The input that activates the receptive field.
 * @param kernel Kernel type, either SLWR_KERNEL_GAUSSIAN or
 * SLWR_KERNEL_BISQUARE.
 * @param activation (in/out) The activation.
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR otherwise.
 */
slwr_status slwr_determine_activation(slwr_workspace *ws,
                                      const slwr_receptive_field *rf,
                                      const slwr_vector *x,
                                      slwr_kernel_type kernel,
                                      gdouble *activation) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf->c->size == x->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(rf->D->size1 == x->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(rf->D->size2 == x->size, SLWR_STATUS_DIMENSION_MISMATCH);

  slwr_workspace_reset(ws);
  slwr_status status = determine_activation_slwr(ws, rf, x, kernel, activation);
  slwr_workspace_reset(ws);
  return status;
}
