#pragma once

#include "slwr/common/slwr-types.h"
#include "slwr/receptive-field/slwr-receptive-field.h"

#include <glib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

slwr_status mahalanobis_distance_gsl(slwr_workspace *ws, const gsl_matrix *D,
                                     const gsl_vector *c, const gsl_vector *x,
                                     gdouble *distance);

slwr_status determine_activation_slwr(slwr_workspace *ws,
                                      const slwr_receptive_field *rf,
                                      const slwr_vector *x,
                                      slwr_kernel_type kernel,
                                      double *activation);

slwr_status determine_activation_gsl(slwr_workspace *ws, const gsl_matrix *D,
                                     const gsl_vector *c, const gsl_vector *x,
                                     slwr_kernel_type kernel,
                                     double *activation);
