#pragma once

#include "slwr/common/slwr-types.h"

#include <glib.h>

gchar *slwr_get_file_contents(const gchar *filename);

gchar *slwr_time_now_as_str();

slwr_status slwr_write_str_to_file(const gchar *str, const gchar *filename);
