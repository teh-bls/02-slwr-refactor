#pragma once

#include "../common/slwr-types.h"

#include <glib.h>

extern const gchar *slwr_available_generator_functions[7];

gchar *slwr_available_generator_functions_to_str();
gboolean slwr_valid_generator_function_name(const gchar *name);

typedef slwr_status (*generator)(const slwr_vector *, slwr_vector *);

typedef struct {
  gchar *name;
  gsize n_in;
  gsize n_out;
  slwr_vector *domain_min;
  slwr_vector *domain_max;
  generator generate;
} slwr_synthetic_data_generator;

slwr_synthetic_data_generator *
slwr_synthetic_data_generator_from_name(const gchar *name);
void slwr_synthetic_data_free(slwr_synthetic_data_generator *data);

slwr_status slwr_crossed_ridge(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_crossed_ridge_data();

slwr_status slwr_sigmoid_x1(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_sigmoid_x1_data();

slwr_status slwr_rotated_sine(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_rotated_sine_data();

slwr_status slwr_beale(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_beale_data();

slwr_status slwr_styblinski_tang(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_styblinski_tang_data();

slwr_status slwr_three_hump_camel(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_three_hump_camel_data();

slwr_status slwr_himmelblau(const slwr_vector *x, slwr_vector *y);
slwr_synthetic_data_generator *slwr_himmelblau_data();

slwr_status slwr_meshgrid_2d(const slwr_vector *x1, const slwr_vector *x2,
                             slwr_matrix *xx1, slwr_matrix *xx2);
