#pragma once

#include <glib.h>

gboolean slwr_str_equal(const gchar *s1, const gchar *s2);
gboolean slwr_is_empty_string(const gchar *s);
