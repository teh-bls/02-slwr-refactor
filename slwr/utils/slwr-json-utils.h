#pragma once

#include "../common/slwr-types.h"
#include "../gng/slwr-gng.h"
#include "../network/slwr-network.h"
#include "../ols/slwr-ols.h"
#include "../pt-modify/slwr-pt-modify.h"

#include <glib.h>
#include <jansson.h>

json_t *slwr_load_json_string(const gchar *json);

slwr_gng_parameters *slwr_gng_parameters_from_json_string(const gchar *json);
slwr_ols_parameters *slwr_ols_parameters_from_json_string(const gchar *json);
slwr_pt_modify_parameters *
slwr_pt_modify_parameters_from_json_string(const gchar *json);
slwr_network_parameters *
slwr_network_parameters_from_json_string(const gchar *json);

json_t *slwr_gng_parameters_to_json(const slwr_gng_parameters *params);
json_t *slwr_ols_parameters_to_json(const slwr_ols_parameters *params);
json_t *
slwr_pt_modify_parameters_to_json(const slwr_pt_modify_parameters *params);
json_t *slwr_network_parameters_to_json(const slwr_network_parameters *params);

json_t *slwr_vector_to_json(const slwr_vector *v);
slwr_vector *slwr_vector_from_json(const json_t *json);
json_t *slwr_matrix_to_json(const slwr_matrix *m);
json_t *slwr_receptive_field_to_json(const slwr_receptive_field *rf);
json_t *slwr_gng_context_to_json(const slwr_gng_context *ctx);
json_t *slwr_ols_context_to_json(const slwr_ols_context *ctx);
json_t *slwr_model_population_to_json(const slwr_model_population *population);

json_t *slwr_mesh_2d_to_json(const slwr_matrix *xx1, const slwr_matrix *xx2,
                             const slwr_matrix *z);

slwr_status slwr_write_json_to_file(const json_t *obj, const gchar *filename);
slwr_status slwr_write_compact_json_to_file(const json_t *obj,
                                            const gchar *filename);
