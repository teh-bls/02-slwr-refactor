#pragma once

#include "../common/slwr-types.h"

#include <glib.h>

typedef struct slwr_allocation_helper slwr_allocation_helper;

typedef struct {
  gdouble activation;
  gsize index;
} slwr_allocation_helper_result;

slwr_allocation_helper *slwr_allocation_helper_alloc();
void slwr_allocation_helper_free(slwr_allocation_helper *helper);

slwr_status slwr_allocation_helper_update(slwr_allocation_helper *helper,
                                          gdouble activation, gsize index);
slwr_status slwr_allocation_helper_reset(slwr_allocation_helper *helper);

slwr_status slwr_allocation_helper_get_highest_activation(
    const slwr_allocation_helper *helper,
    slwr_allocation_helper_result *result);
slwr_status slwr_allocation_helper_get_second_highest_activation(
    const slwr_allocation_helper *helper,
    slwr_allocation_helper_result *result);
