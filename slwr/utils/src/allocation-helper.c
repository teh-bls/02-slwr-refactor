#include "slwr/utils/slwr-allocation-helper.h"

#include "slwr/common/slwr-workspace.h"
#include "slwr/utils/slwr-math-utils.h"

static const gsize current = 0;
static const gsize second_highest = 1;
static const gsize highest = 2;

struct slwr_allocation_helper {
  /**
   * activations is a vector of three weights, ordered (current, second_highest,
   * highest). indices is the corresponding vector containing the model
   * indices.*/
  slwr_vector *activations;
  slwr_vector *indices;
};

static void update_activations(slwr_vector *activations, slwr_vector *indices) {
  if (activations->data[current] > activations->data[second_highest]) {
    if (activations->data[current] > activations->data[highest]) {
      /* The current activation is greater than the highest activation: the
       * second-highest activation entry gets the old highest activation, and
       * the highest activation entry gets the current activation. */
      slwr_swap_double(&activations->data[second_highest],
                       &activations->data[highest]);
      slwr_swap_double(&indices->data[second_highest], &indices->data[highest]);
      activations->data[highest] = activations->data[current];
      indices->data[highest] = indices->data[current];
    } else {
      /* The current activation is greater than the second-highest activation:
       * the second-highest activations entry gets the current activations.  */
      activations->data[second_highest] = activations->data[current];
      indices->data[second_highest] = indices->data[current];
    }
  }
}

slwr_allocation_helper *slwr_allocation_helper_alloc() {
  slwr_allocation_helper *helper =
      (slwr_allocation_helper *)g_malloc0(sizeof(slwr_allocation_helper));
  if (!helper) {
    return NULL;
  }
  helper->indices = slwr_vector_alloc(3);
  if (!helper->indices) {
    goto on_error;
  }
  helper->activations = slwr_vector_alloc(3);
  if (!helper->activations) {
    goto on_error;
  }

  return helper;

on_error:
  slwr_allocation_helper_free(helper);
  return NULL;
}

void slwr_allocation_helper_free(slwr_allocation_helper *helper) {
  if (!helper) {
    return;
  }
  if (helper->activations) {
    slwr_vector_free(helper->activations);
  }
  if (helper->indices) {
    slwr_vector_free(helper->indices);
  }
  g_free(helper);
}

slwr_status slwr_allocation_helper_update(slwr_allocation_helper *helper,
                                          gdouble activation, gsize index) {
  g_return_val_if_fail(helper != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(helper->activations != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(helper->indices != NULL, SLWR_STATUS_ERROR);

  helper->activations->data[current] = activation;
  helper->indices->data[current] = (gdouble)index;
  update_activations(helper->activations, helper->indices);

  return SLWR_STATUS_OK;
}

slwr_status slwr_allocation_helper_reset(slwr_allocation_helper *helper) {
  g_return_val_if_fail(helper != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(helper->activations != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(helper->indices != NULL, SLWR_STATUS_ERROR);

  slwr_vector_set_zero(helper->activations);
  slwr_vector_set_zero(helper->indices);

  return SLWR_STATUS_OK;
}

slwr_status slwr_allocation_helper_get_highest_activation(
    const slwr_allocation_helper *helper,
    slwr_allocation_helper_result *result) {
  g_return_val_if_fail(helper != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(result != NULL, SLWR_STATUS_ERROR);

  result->activation = helper->activations->data[highest];
  result->index = (gsize)helper->indices->data[highest];

  return SLWR_STATUS_OK;
}

slwr_status slwr_allocation_helper_get_second_highest_activation(
    const slwr_allocation_helper *helper,
    slwr_allocation_helper_result *result) {
  g_return_val_if_fail(helper != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(result != NULL, SLWR_STATUS_ERROR);

  result->activation = helper->activations->data[second_highest];
  result->index = (gsize)helper->indices->data[second_highest];

  return SLWR_STATUS_OK;
}
