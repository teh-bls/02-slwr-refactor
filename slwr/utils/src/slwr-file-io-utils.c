#include "slwr/utils/slwr-file-io-utils.h"

#include <gio/gio.h>

static void report_error(GError *error, const gchar *caller_name) {
  g_return_if_fail(error != NULL);
  if (caller_name) {
    g_critical("%s: %s (code %d)", caller_name, error->message, error->code);
  } else {
    g_critical("%s (code %d)", error->message, error->code);
  }
}

gchar *slwr_get_file_contents(const gchar *filename) {
  g_return_val_if_fail(filename != NULL, NULL);

  gchar *contents = NULL;
  GError *error = NULL;
  if (!g_file_get_contents(filename, &contents, NULL, &error)) {
    report_error(error, __PRETTY_FUNCTION__);
    g_error_free(error);
    goto on_error;
  }
  return contents;

on_error:
  if (contents) {
    g_free(contents);
  }
  return NULL;
}

gchar *slwr_time_now_as_str() {
  GTimeZone *time_zone = NULL;
  GDateTime *date_time = NULL;
  gchar *time_now = NULL;

  time_zone = g_time_zone_new_local();
  if (!time_zone) {
    goto end;
  }
  date_time = g_date_time_new_now(time_zone);
  if (!date_time) {
    goto end;
  }

  time_now = g_date_time_format_iso8601(date_time);

end:
  if (date_time) {
    g_date_time_unref(date_time);
  }
  if (time_zone) {
    g_time_zone_unref(time_zone);
  }
  return time_now;
}

static slwr_status output_str_to_stream(const gchar *str,
                                        GOutputStream *stream) {
  g_return_val_if_fail(str != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(stream != NULL, SLWR_STATUS_ERROR);

  gsize str_len = 0;
  gsize characters_written = 0;
  GError *error = NULL;
  slwr_status status = SLWR_STATUS_OK;

  str_len = g_utf8_strlen(str, G_MAXINT);
  characters_written =
      g_output_stream_write(stream, str, str_len, NULL, &error);
  if (characters_written != str_len && error) {
    report_error(error, __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
  }

  if (error) {
    g_error_free(error);
  }
  return status;
}

slwr_status slwr_write_str_to_file(const gchar *str, const gchar *filename) {
  g_return_val_if_fail(str != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);

  GFile *file = NULL;
  GOutputStream *stream = NULL;
  GError *error = NULL;
  slwr_status status = SLWR_STATUS_OK;

  file = g_file_new_for_path(filename);
  stream = G_OUTPUT_STREAM(
      g_file_replace(file, NULL, FALSE, G_FILE_CREATE_NONE, NULL, &error));
  if (!stream) {
    report_error(error, __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  status = output_str_to_stream(str, stream);
  if (!g_output_stream_close(stream, NULL, &error)) {
    report_error(error, __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
  }

end:
  if (error) {
    g_error_free(error);
  }
  if (stream) {
    g_object_unref(stream);
  }
  if (file) {
    g_object_unref(file);
  }
  return status;
}
