#include "math-utils.h"

#include "slwr/common/src/workspace.h"

#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>

slwr_status slwr_vector_difference(slwr_workspace *ws, const slwr_vector *a,
                                   const slwr_vector *b, slwr_vector *ab) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ab != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size == b->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(a->size == ab->size, SLWR_STATUS_DIMENSION_MISMATCH);

  gsl_vector *ga = workspace_gsl_vector_from_slwr_vector(ws, a, WORKSPACE_N_IN);
  if (!ga) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gb = workspace_gsl_vector_from_slwr_vector(ws, b, WORKSPACE_N_IN);
  if (!gb) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gab =
      workspace_gsl_vector_from_slwr_vector(ws, ab, WORKSPACE_N_IN);
  if (!gab) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = utils_vector_difference(ga, gb, gab);
  if (status == SLWR_STATUS_OK) {
    status = slwr_vector_from_gsl_vector(ab, gab);
  }
  return status;
}

slwr_status slwr_euclidean_distance(slwr_workspace *ws, const slwr_vector *a,
                                    const slwr_vector *b, gdouble *distance) {

  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(distance != NULL, SLWR_STATUS_ERROR);

  gsl_vector *ga = workspace_gsl_vector_from_slwr_vector(ws, a, WORKSPACE_N_IN);
  if (!ga) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gb = workspace_gsl_vector_from_slwr_vector(ws, b, WORKSPACE_N_IN);
  if (!gb) {
    return SLWR_STATUS_ERROR;
  }
  return utils_euclidean_distance(ws, ga, gb, distance);
}

slwr_status utils_vector_difference(const gsl_vector *a, const gsl_vector *b,
                                    gsl_vector *ab) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ab != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size == b->size, SLWR_STATUS_DIMENSION_MISMATCH);

  if (gsl_vector_memcpy(ab, a) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_sub(ab, b) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

slwr_status utils_euclidean_distance(slwr_workspace *ws, const gsl_vector *a,
                                     const gsl_vector *b, gdouble *distance) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(distance != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size == b->size, SLWR_STATUS_DIMENSION_MISMATCH);

  *distance = GSL_NAN;
  gsl_vector *ab = workspace_vector(ws, WORKSPACE_N_IN);
  if (!ab) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_memcpy(ab, a) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_sub(ab, b) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  *distance = gsl_blas_dnrm2(ab);
  return SLWR_STATUS_OK;
}

slwr_vector *slwr_linspace(gdouble start, gdouble stop, gsize num,
                           gboolean include_endpoint) {
  g_return_val_if_fail(num > 0, NULL);

  slwr_vector *v = slwr_vector_alloc(num);

  gdouble denom = include_endpoint ? (gdouble)num - 1 : (gdouble)num;
  gdouble step = (stop - start) / denom;

  v->data[0] = start;
  for (gsize i = 1; i < num; ++i) {
    v->data[i] = v->data[i - 1] + step;
  }

  return v;
}

void slwr_swap_double(gdouble *a, gdouble *b) {
  g_return_if_fail(a != NULL);
  g_return_if_fail(b != NULL);

  gdouble temp = *a;
  *a = *b;
  *b = temp;
}