#pragma once

#include "slwr/utils/slwr-math-utils.h"

#include <gsl/gsl_vector.h>

slwr_status utils_vector_difference(const gsl_vector *a, const gsl_vector *b,
                                    gsl_vector *ab);

slwr_status utils_euclidean_distance(slwr_workspace *ws, const gsl_vector *a,
                                     const gsl_vector *b, gdouble *distance);
