#include "slwr/utils/slwr-synthetic-data.h"

#include "slwr/common/slwr-workspace.h"
#include "slwr/utils/slwr-str-utils.h"

#include <gsl/gsl_math.h>

const gchar *slwr_available_generator_functions[] = {
    "crossed-ridge",   "sigmoid-x1",       "rotated-sine", "beale",
    "styblinski-tang", "three-hump-camel", "himmelblau"};

static slwr_status validate_parameters(gsize n_in, gsize n_out,
                                       const slwr_vector *x, slwr_vector *y) {
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x->size == n_in, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(y->size == n_out, SLWR_STATUS_DIMENSION_MISMATCH);
  return SLWR_STATUS_OK;
}

static slwr_synthetic_data_generator *
slwr_synthetic_data_allocate(const gchar *name, gsize n_in, gsize n_out) {
  slwr_synthetic_data_generator *data =
      (slwr_synthetic_data_generator *)g_malloc0(
          sizeof(slwr_synthetic_data_generator));
  if (!data) {
    goto on_error;
  }
  data->name = g_strdup(name);
  if (!data->name) {
    goto on_error;
  }
  data->n_in = n_in;
  data->n_out = n_out;
  data->domain_min = slwr_vector_alloc(n_in);
  if (!data->domain_min) {
    goto on_error;
  }
  data->domain_max = slwr_vector_alloc(n_in);
  if (!data->domain_max) {
    goto on_error;
  }
  return data;

on_error:
  slwr_synthetic_data_free(data);
  return NULL;
}

gchar *slwr_available_generator_functions_to_str() {
  GString *str = NULL;
  if (G_N_ELEMENTS(slwr_available_generator_functions) == 0) {
    str = g_string_new("");
  } else {
    str = g_string_new(slwr_available_generator_functions[0]);
    for (gsize i = 1; i < G_N_ELEMENTS(slwr_available_generator_functions);
         ++i) {
      g_string_append_printf(str, ", %s",
                             slwr_available_generator_functions[i]);
    }
  }
  return g_string_free(str, FALSE);
}

gboolean slwr_valid_generator_function_name(const gchar *name) {
  for (gsize i = 0; i < G_N_ELEMENTS(slwr_available_generator_functions); ++i) {
    if (slwr_str_equal(slwr_available_generator_functions[i], name)) {
      return TRUE;
    }
  }
  return FALSE;
}

slwr_synthetic_data_generator *
slwr_synthetic_data_generator_from_name(const gchar *name) {
  g_return_val_if_fail(name != NULL, NULL);
  if (!slwr_valid_generator_function_name(name)) {
    return NULL;
  } else if (slwr_str_equal(name, "crossed-ridge")) {
    return slwr_crossed_ridge_data();
  } else if (slwr_str_equal(name, "sigmoid-x1")) {
    return slwr_sigmoid_x1_data();
  } else if (slwr_str_equal(name, "rotated-sine")) {
    return slwr_rotated_sine_data();
  } else if (slwr_str_equal(name, "beale")) {
    return slwr_beale_data();
  } else if (slwr_str_equal(name, "styblinski-tang")) {
    return slwr_styblinski_tang_data();
  } else if (slwr_str_equal(name, "three-hump-camel")) {
    return slwr_three_hump_camel_data();
  } else if (slwr_str_equal(name, "himmelblau")) {
    return slwr_himmelblau_data();
  } else {
    g_error("a valid function has not been implemented");
  }
}

void slwr_synthetic_data_free(slwr_synthetic_data_generator *data) {
  if (!data) {
    return;
  }
  if (data->domain_max) {
    slwr_vector_free(data->domain_max);
  }
  if (data->domain_min) {
    slwr_vector_free(data->domain_min);
  }
  if (data->name) {
    g_free(data->name);
  }
  g_free(data);
}

slwr_status slwr_crossed_ridge(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble x1 = x->data[0];
  const gdouble x2 = x->data[1];
  gdouble *y1 = &y->data[0];
  *y1 = gsl_max(exp(-10 * gsl_pow_2(x1)),
                gsl_max(exp(-50 * gsl_pow_2(x2)),
                        1.25 * exp(-5 * (gsl_pow_2(x1) + gsl_pow_2(x2)))));
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_crossed_ridge_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("crossed-ridge", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -1.0;
    data->domain_max->data[i] = 1.0;
  }
  data->generate = slwr_crossed_ridge;
  return data;
}

slwr_status slwr_sigmoid_x1(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble steepness = 1000.0;
  const gdouble x1 = x->data[0];
  gdouble *y1 = &y->data[0];
  *y1 = 1.0 / (1.0 + exp(-1 * steepness * x1));
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_sigmoid_x1_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("sigmoid-x1", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -1.0;
    data->domain_max->data[i] = 1.0;
  }
  data->generate = slwr_sigmoid_x1;
  return data;
}

slwr_status slwr_rotated_sine(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble x1 = x->data[0];
  const gdouble x2 = x->data[1];
  gdouble *y1 = &y->data[0];
  *y1 = sin(2 * M_PI * (x1 + x2));
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_rotated_sine_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("rotated-sine", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -1.0;
    data->domain_max->data[i] = 1.0;
  }
  data->generate = slwr_rotated_sine;
  return data;
}

slwr_status slwr_beale(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble x1 = x->data[0];
  const gdouble x2 = x->data[1];
  gdouble *y1 = &y->data[0];
  *y1 = gsl_pow_2(1.5 - x1 + x1 * x2) +
        gsl_pow_2(2.25 - x1 + x1 * gsl_pow_2(x2)) +
        gsl_pow_2(2.625 - x1 + x1 * gsl_pow_3(x2));
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_beale_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("beale", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -4.5;
    data->domain_max->data[i] = 4.5;
  }
  data->generate = slwr_beale;
  return data;
}

slwr_status slwr_styblinski_tang(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble x1 = x->data[0];
  const gdouble x2 = x->data[1];
  gdouble *y1 = &y->data[0];
  *y1 = ((gsl_pow_4(x1) - 16.0 * gsl_pow_2(x1) + 5.0 * x1) +
         (gsl_pow_4(x2) - 16.0 * gsl_pow_2(x2) + 5.0 * x2)) /
        2.0;
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_styblinski_tang_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("styblinski-tang", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -5.0;
    data->domain_max->data[i] = 5.0;
  }
  data->generate = slwr_styblinski_tang;
  return data;
}

slwr_status slwr_three_hump_camel(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble x1 = x->data[0];
  const gdouble x2 = x->data[1];
  gdouble *y1 = &y->data[0];
  *y1 = 2 * gsl_pow_2(x1) - 1.05 * gsl_pow_4(x1) + (gsl_pow_6(x1) / 6.0) +
        x1 * x2 + gsl_pow_2(x2);
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_three_hump_camel_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("three-hump-camel", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -5.0;
    data->domain_max->data[i] = 5.0;
  }
  data->generate = slwr_three_hump_camel;
  return data;
}

slwr_status slwr_himmelblau(const slwr_vector *x, slwr_vector *y) {
  slwr_status status;
  if ((status = validate_parameters(2, 1, x, y)) != SLWR_STATUS_OK) {
    return status;
  }

  const gdouble x1 = x->data[0];
  const gdouble x2 = x->data[1];
  gdouble *y1 = &y->data[0];
  *y1 =
      gsl_pow_2(gsl_pow_2(x1) + x2 - 11.0) + gsl_pow_2(x1 - gsl_pow_2(x2) - 7);
  return SLWR_STATUS_OK;
}

slwr_synthetic_data_generator *slwr_himmelblau_data() {
  slwr_synthetic_data_generator *data =
      slwr_synthetic_data_allocate("himmelblau", 2, 1);
  if (!data) {
    return NULL;
  }
  for (gsize i = 0; i < data->n_in; ++i) {
    data->domain_min->data[i] = -5.0;
    data->domain_max->data[i] = 5.0;
  }
  data->generate = slwr_himmelblau;
  return data;
}

slwr_status slwr_meshgrid_2d(const slwr_vector *x1, const slwr_vector *x2,
                             slwr_matrix *xx1, slwr_matrix *xx2) {
  g_return_val_if_fail(x1 != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x2 != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(xx1 != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(xx2 != NULL, SLWR_STATUS_ERROR);

  g_return_val_if_fail(x1->size == x2->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(x1->size == xx1->size1, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(x1->size == xx2->size1, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(xx1->size1 == xx1->size2,
                       SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(xx2->size1 == xx2->size2,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  const gsize dim = x1->size;
  for (gsize i = 0; i < dim; ++i) {
    for (gsize j = 0; j < dim; ++j) {
      xx1->data[i * dim + j] = x1->data[j];
      xx2->data[i * dim + j] = x2->data[i];
    }
  }

  return SLWR_STATUS_OK;
}
