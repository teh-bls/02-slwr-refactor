#include "str-utils.h"

gboolean slwr_str_equal(const gchar *s1, const gchar *s2) {
  return g_strcmp0(s1, s2) == 0;
}

gboolean slwr_is_empty_string(const gchar *s) { return slwr_str_equal(s, ""); }
