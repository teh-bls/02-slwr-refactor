#include "json-utils.h"

#include "slwr/common/slwr-workspace.h"
#include "slwr/gng/slwr-gng.h"
#include "slwr/utils/slwr-str-utils.h"

#include <gsl/gsl_math.h>
#include <jansson.h>

static slwr_status json_number_to_double(const json_t *number, gdouble *value) {
  g_return_val_if_fail(number != NULL, SLWR_STATUS_ERROR);

  if (!json_is_number(number)) {
    return SLWR_STATUS_ERROR;
  }
  *value = json_is_integer(number) ? (gdouble)json_integer_value(number)
                                   : json_real_value(number);
  return SLWR_STATUS_OK;
}

static inline void warn_json_error(const gchar *caller_name,
                                   const json_error_t *error) {
  g_critical("%s: %s (in line %d, column %d)", caller_name, error->text,
             error->line, error->column);
}

json_t *slwr_load_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  json_error_t json_error;
  json_t *obj = json_loads(json, 0, &json_error);
  if (!obj) {
    warn_json_error(__PRETTY_FUNCTION__, &json_error);
  }
  return obj;
}

slwr_gng_parameters *slwr_gng_parameters_from_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  json_t *obj = slwr_load_json_string(json);
  if (!obj) {
    return NULL;
  }

  json_error_t json_error;
  slwr_gng_parameters *params =
      (slwr_gng_parameters *)g_malloc0(sizeof(slwr_gng_parameters));
  if (!params) {
    g_critical("could not allocate memory for slwr_gng_parameters");
    return NULL;
  }
  slwr_gng_parameters_default_values(params);
  const gchar *fmt = "{s?:i, s?:i, s?:f, s?:f, s?:f, s?:f}";
  if (json_unpack_ex(obj, &json_error, 0, fmt, "max-age", &params->max_age,
                     "insertion-interval", &params->insertion_interval,
                     "alpha-error", &params->alpha_error, "epsilon-n",
                     &params->epsilon_n, "epsilon-b", &params->epsilon_b, "d",
                     &params->d) != 0) {
    warn_json_error(__PRETTY_FUNCTION__, &json_error);
    json_decref(obj);
    return NULL;
  }

  json_decref(obj);
  return params;
}

slwr_ols_parameters *slwr_ols_parameters_from_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  json_t *obj = slwr_load_json_string(json);
  if (!obj) {
    return NULL;
  }

  json_error_t json_error;
  gsize n_in = 0;
  gsize n_out = 0;
  gdouble epsilon_A = GSL_NAN;
  gdouble epsilon_w_out = GSL_NAN;
  const gchar *fmt = "{s:i, s:i, s?:f, s?:f}";
  if (json_unpack_ex(obj, &json_error, 0, fmt, "n-in", &n_in, "n-out", &n_out,
                     "epsilon-A", &epsilon_A, "epsilon-w-out",
                     &epsilon_w_out) != 0) {
    warn_json_error(__PRETTY_FUNCTION__, &json_error);
    json_decref(obj);
    return NULL;
  }

  slwr_ols_parameters *params =
      (slwr_ols_parameters *)g_malloc0(sizeof(slwr_ols_parameters));
  if (!params) {
    g_critical("could not allocate memory for slwr_ols_parameters");
    return NULL;
  }
  slwr_ols_parameters_default_values(n_in, n_out, params);
  if (!gsl_isnan(epsilon_A)) {
    params->epsilon_A = epsilon_A;
  }
  if (!gsl_isnan(epsilon_w_out)) {
    params->epsilon_w_out = epsilon_w_out;
  }

  json_decref(obj);
  return params;
}

slwr_pt_modify_parameters *
slwr_pt_modify_parameters_from_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  json_t *obj = slwr_load_json_string(json);
  if (!obj) {
    return NULL;
  }

  json_error_t json_error;
  gsize n_in = 0;
  gdouble epsilon = GSL_NAN;
  gdouble s = GSL_NAN;
  gdouble error_max = GSL_NAN;
  gdouble dist_max = GSL_NAN;
  const gchar *fmt = "{s:i, s?:f, s?:f, s?:f, s?:f}";
  if (json_unpack_ex(obj, &json_error, 0, fmt, "n-in", &n_in, "epsilon",
                     &epsilon, "s", &s, "error-max", &error_max, "dist-max",
                     &dist_max) != 0) {
    warn_json_error(__PRETTY_FUNCTION__, &json_error);
    json_decref(obj);
    return NULL;
  }

  slwr_pt_modify_parameters *params =
      (slwr_pt_modify_parameters *)g_malloc0(sizeof(slwr_pt_modify_parameters));
  if (!params) {
    g_critical("could not allocate memory for slwr_pt_modify_params");
    return NULL;
  }
  slwr_pt_modify_parameters_default_values(n_in, params);
  if (!gsl_isnan(epsilon)) {
    params->epsilon = epsilon;
  }
  if (!gsl_isnan(s)) {
    params->s = s;
  }
  if (!gsl_isnan(error_max)) {
    params->error_max = error_max;
  }
  if (!gsl_isnan(dist_max)) {
    params->dist_max = dist_max;
  }

  json_decref(obj);
  return params;
}

/* TODO: what is the difference between this and slwr_matrix_from_json_array()
 * below?
 * Also: convert to slwr_matrix *slwr_matrix_from_json (similar to slwr_vector
 * *slwr_vector_from_json).*/
static slwr_status populate_slwr_matrix_from_json_array(const json_t *array,
                                                        slwr_matrix *m) {
  gsize i, j;
  json_t *inner_array = NULL;
  json_t *entry = NULL;
  const gsize n_in = json_array_size(array);
  slwr_status status = SLWR_STATUS_OK;

  if (!json_is_array(array)) {
    g_critical("expected matrix as a JSON array");
    status = SLWR_STATUS_ERROR;
    goto end;
  }

  if (m->size1 != n_in) {
    g_critical("number of rows of given slwr_matrix (%lu) does not match the "
               "number of rows of the JSON array (%lu)",
               m->size1, n_in);
    status = SLWR_STATUS_DIMENSION_MISMATCH;
    goto end;
  }

  json_array_foreach(array, i, inner_array) {
    if (!json_is_array(inner_array)) {
      g_critical("expected matrix row as a JSON array");
      status = SLWR_STATUS_ERROR;
      goto end;
    }
    if (json_array_size(inner_array) != n_in) {
      g_critical(
          "inner JSON array dimensions must match the outer JSON array's "
          "dimension");
      status = SLWR_STATUS_DIMENSION_MISMATCH;
      goto end;
    }
    json_array_foreach(inner_array, j, entry) {
      if ((status = json_number_to_double(entry, m->data + (i * n_in + j))) !=
          SLWR_STATUS_OK) {
        g_critical("JSON array entry must be a numeric value");
        goto end;
      }
    }
  }
end:
  return status;
}

static slwr_matrix *slwr_matrix_from_json_array(const json_t *array) {
  slwr_matrix *m = NULL;
  gdouble *array_data = NULL;
  gsize n_in = json_array_size(array);
  gsize i, j;
  json_t *inner_array = NULL;
  json_t *entry = NULL;

  if (!json_is_array(array)) {
    g_critical("expected matrix as a JSON array");
    goto on_error;
  }

  array_data = (gdouble *)g_malloc0(n_in * n_in * sizeof(gdouble));
  if (!array_data) {
    g_critical("could not allocate memory for the slwr_matrix data");
    goto on_error;
  }
  json_array_foreach(array, i, inner_array) {
    if (!json_is_array(inner_array)) {
      g_critical("expected matrix row as a JSON array");
      goto on_error;
    }
    if (json_array_size(inner_array) != n_in) {
      g_critical(
          "inner JSON array dimensions must match the outer JSON array's "
          "dimension");
      goto on_error;
    }
    json_array_foreach(inner_array, j, entry) {
      if (json_number_to_double(entry, array_data + (i * n_in + j)) !=
          SLWR_STATUS_OK) {
        g_critical("JSON array entry must be a numeric value");
        goto on_error;
      }
    }
  }

  m = (slwr_matrix *)g_malloc0(sizeof(slwr_matrix));
  if (!m) {
    g_critical("could not allocate memory for the slwr_matrix");
    goto on_error;
  }
  m->size1 = n_in;
  m->size2 = n_in;
  m->data = array_data;
  return m;

on_error:
  if (array_data) {
    g_free(array_data);
  }
  return NULL;
}

/**
 * A helper for setting the boolean value_to_set to the equivalent of the string
 * value of parameter_key found in JSON object obj.
 *
 * If the value for the given parameter_key in obj is neither "on" nor "off",
 * value_to_set remains unchanged.
 *
 * @param parameter_key The key whose value should be set.
 * @param obj The JSON object containing the value of parameter_key.
 * @param value_to_set The value to set.
 */
static void set_boolean_parameter_from_string(const gchar *parameter_key,
                                              const json_t *obj,
                                              gboolean *value_to_set) {
  json_t *parameter_string = json_object_get(obj, parameter_key);
  if (!parameter_string) {
    return;
  }
  const gchar *value = json_string_value(parameter_string);
  if (slwr_str_equal(value, "on")) {
    *value_to_set = TRUE;
  } else if (slwr_str_equal(value, "off")) {
    *value_to_set = FALSE;
  } else {
    g_critical("unknown value \"%s\" for %s; switching %s %s", value,
               parameter_key, parameter_key, *value_to_set ? "on" : "off");
  }
}

static inline gboolean
is_valid_model_allocation(const gchar *model_allocation) {
  return slwr_str_equal(model_allocation, "default-lwpr") ||
         slwr_str_equal(model_allocation,
                        "error-guided-insertion-and-vector-quantization") ||
         slwr_str_equal(
             model_allocation,
             "error-guided-insertion-and-penalized-vector-quantization");
}

static inline gboolean
is_valid_error_guided_insertion_error_source(const gchar *error_source) {
  return slwr_str_equal(error_source, "input-space") ||
         slwr_str_equal(error_source, "output-space") ||
         slwr_str_equal(error_source, "minimally-activated-models");
}

slwr_network_parameters *
slwr_network_parameters_from_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  slwr_network_parameters *params = NULL;

  json_t *obj = NULL;
  json_t *n_in_integer = NULL;
  json_t *n_out_integer = NULL;
  json_t *D_init_array = NULL;
  json_t *max_models_integer = NULL;
  json_t *rf_update_string = NULL;
  json_t *model_allocation_string = NULL;
  json_t *decay_error_max_tau_float = NULL;
  json_t *error_max_float = NULL;
  json_t *error_guided_insertion_error_source_string = NULL;
  json_t *w_gen_float = NULL;
  json_t *w_prune_float = NULL;
  json_t *lambda_init_float = NULL;
  json_t *lambda_tau_float = NULL;
  json_t *lambda_final_float = NULL;
  gsize n_in;
  gsize n_out;

  obj = slwr_load_json_string(json);
  if (!obj) {
    goto on_error;
  }

  n_in_integer = json_object_get(obj, "n-in");
  if (!n_in_integer) {
    g_critical("%s: required value for \"n-in\" not found",
               __PRETTY_FUNCTION__);
    goto on_error;
  }
  n_in = json_integer_value(n_in_integer);

  n_out_integer = json_object_get(obj, "n-out");
  if (!n_out_integer) {
    g_critical("%s: required value for \"n-out\" not found",
               __PRETTY_FUNCTION__);
    goto on_error;
  }
  n_out = json_integer_value(n_out_integer);

  params =
      (slwr_network_parameters *)g_malloc0(sizeof(slwr_network_parameters));
  if (!params) {
    g_critical("%s: could not allocate memory for slwr_network_parameters",
               __PRETTY_FUNCTION__);
    goto on_error;
  }
  params->n_in = n_in;
  params->n_out = n_out;
  params->D_init = (slwr_matrix *)g_malloc0(sizeof(slwr_matrix));
  if (!params->D_init) {
    g_critical("%s: could not allocate memory for D_init", __PRETTY_FUNCTION__);
    goto on_error;
  }
  params->D_init->size1 = n_in;
  params->D_init->size2 = n_in;
  params->D_init->data = (gdouble *)g_malloc0(sizeof(gdouble) * n_in * n_in);
  if (!params->D_init->data) {
    g_critical("%s: could not allocate memory for D_init data",
               __PRETTY_FUNCTION__);
    goto on_error;
  }
  slwr_network_parameters_default_values(n_in, params);

  D_init_array = json_object_get(obj, "D-init");
  if (D_init_array) {
    populate_slwr_matrix_from_json_array(D_init_array, params->D_init);
  }

  max_models_integer = json_object_get(obj, "max-models");
  if (max_models_integer) {
    params->max_models = json_integer_value(max_models_integer);
  }

  rf_update_string = json_object_get(obj, "receptive-field-update");
  if (rf_update_string) {
    const gchar *rf_update = json_string_value(rf_update_string);
    if (slwr_str_equal(rf_update, "off")) {
      params->rf_update_kind = SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_OFF;
    } else if (slwr_str_equal(rf_update, "pt-modify-rho")) {
      params->rf_update_kind =
          SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_PT_MODIFY_RHO;
    } else if (slwr_str_equal(rf_update, "pt-modify-chi")) {
      params->rf_update_kind =
          SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_PT_MODIFY_CHI;
    } else if (slwr_str_equal(rf_update, "pt-modify-upsilon")) {
      params->rf_update_kind =
          SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_PT_MODIFY_UPSILON;
    } else if (slwr_str_equal(rf_update, "dbd")) {
      params->rf_update_kind = SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_DBD;
    } else if (slwr_str_equal(rf_update, "covariance")) {
      params->rf_update_kind = SLWR_NETWORK_RECEPTIVE_FIELD_UPDATE_COVARIANCE;
    } else {
      g_critical("unknown value \"%s\" for receptive-field-update; switching "
                 "receptive field updates off",
                 rf_update);
    }
  }

  /* params->decay_error_max has a default value set by the call to
   * slwr_network_parameters_default_values() above */
  set_boolean_parameter_from_string("decay-error-max", obj,
                                    &params->decay_error_max);

  /* params->apply_strict_decay_error_max has a default value set by the call to
   * slwr_network_parameters_default_values() above */
  set_boolean_parameter_from_string("apply-strict-decay-error-max", obj,
                                    &params->apply_strict_decay_error_max);

  decay_error_max_tau_float = json_object_get(obj, "decay-error-max-tau");
  if (decay_error_max_tau_float) {
    params->decay_error_max_tau = json_number_value(decay_error_max_tau_float);
  }

  error_max_float = json_object_get(obj, "error-max");
  if (error_max_float) {
    params->error_max = json_number_value(error_max_float);
  }

  model_allocation_string = json_object_get(obj, "model-allocation");
  if (model_allocation_string) {
    const gchar *model_allocation = json_string_value(model_allocation_string);
    if (is_valid_model_allocation(model_allocation)) {
      if (params->model_allocation) {
        g_free(params->model_allocation);
      }
      params->model_allocation =
          g_strdup(json_string_value(model_allocation_string));
    } else {
      /* params->model_allocation has a default value set by the call to
       * slwr_network_parameters_default_values() above */
      g_critical("unknown value \"%s\" for model-allocation; switching to %s",
                 model_allocation, params->model_allocation);
    }
  }

  /* params->model_pruning has a default value set by the call to
   * slwr_network_parameters_default_values() above */
  set_boolean_parameter_from_string("model-pruning", obj,
                                    &params->model_pruning);

  error_guided_insertion_error_source_string =
      json_object_get(obj, "error-guided-insertion-error-source");
  if (error_guided_insertion_error_source_string) {
    const gchar *error_source =
        json_string_value(error_guided_insertion_error_source_string);
    if (is_valid_error_guided_insertion_error_source(error_source)) {
      if (params->error_guided_insertion_error_source) {
        g_free(params->error_guided_insertion_error_source);
      }
      params->error_guided_insertion_error_source = g_strdup(error_source);
    } else {
      /* params->error_guided_insertion_error_source has a default value set by
       * the call to slwr_network_parameters_default_values() above */
      g_critical(
          "unknown value \"%s\" for error-guided-insertion-error-source; "
          "switching to %s",
          error_source, params->error_guided_insertion_error_source);
    }
  }

  w_gen_float = json_object_get(obj, "w-gen");
  if (w_gen_float) {
    params->w_gen = json_number_value(w_gen_float);
  }
  w_prune_float = json_object_get(obj, "w-prune");
  if (w_prune_float) {
    params->w_prune = json_number_value(w_prune_float);
  }
  lambda_init_float = json_object_get(obj, "lambda-init");
  if (lambda_init_float) {
    params->lambda_init = json_number_value(lambda_init_float);
  }
  lambda_tau_float = json_object_get(obj, "lambda-tau");
  if (lambda_tau_float) {
    params->lambda_tau = json_number_value(lambda_tau_float);
  }
  lambda_final_float = json_object_get(obj, "lambda-final");
  if (lambda_final_float) {
    params->lambda_final = json_number_value(lambda_final_float);
  }

  json_decref(obj);
  return params;

on_error:
  if (params) {
    slwr_network_parameters_free(params);
  }
  if (obj) {
    json_decref(obj);
  }
  return NULL;
}

json_t *slwr_gng_parameters_to_json(const slwr_gng_parameters *params) {
  g_return_val_if_fail(params != NULL, NULL);

  json_t *obj = NULL;
  json_t *max_age = NULL;
  json_t *insertion_interval = NULL;
  json_t *alpha_error = NULL;
  json_t *epsilon_n = NULL;
  json_t *epsilon_b = NULL;
  json_t *d = NULL;

  obj = json_object();
  if (!obj) {
    goto on_error;
  }
  max_age = json_integer((json_int_t)params->max_age);
  if (!max_age) {
    goto on_error;
  }
  insertion_interval = json_integer((json_int_t)params->insertion_interval);
  if (!insertion_interval) {
    goto on_error;
  }
  alpha_error = json_real(params->alpha_error);
  if (!alpha_error) {
    goto on_error;
  }
  epsilon_n = json_real(params->epsilon_n);
  if (!epsilon_n) {
    goto on_error;
  }
  epsilon_b = json_real(params->epsilon_b);
  if (!epsilon_b) {
    goto on_error;
  }
  d = json_real(params->d);
  if (!d) {
    goto on_error;
  }

  if (json_object_set(obj, "max-age", max_age) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "insertion-interval", insertion_interval) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "alpha-error", alpha_error) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "epsilon-n", epsilon_n) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "epsilon-b", epsilon_b) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "d", d) != 0) {
    goto on_error;
  }

  json_decref(d);
  json_decref(epsilon_b);
  json_decref(epsilon_n);
  json_decref(alpha_error);
  json_decref(insertion_interval);
  json_decref(max_age);

  return obj;

on_error:
  if (d) {
    json_decref(d);
  }
  if (epsilon_b) {
    json_decref(epsilon_b);
  }
  if (epsilon_n) {
    json_decref(epsilon_n);
  }
  if (alpha_error) {
    json_decref(alpha_error);
  }
  if (insertion_interval) {
    json_decref(insertion_interval);
  }
  if (max_age) {
    json_decref(max_age);
  }
  if (obj) {
    json_decref(obj);
  }
  return NULL;
}

json_t *slwr_ols_parameters_to_json(const slwr_ols_parameters *params) {
  g_return_val_if_fail(params != NULL, NULL);

  json_t *obj = NULL;
  json_t *n_in = NULL;
  json_t *n_out = NULL;
  json_t *epsilon_A = NULL;
  json_t *epsilon_w_out = NULL;

  obj = json_object();
  if (!obj) {
    goto on_error;
  }
  n_in = json_integer((json_int_t)params->n_in);
  if (!n_in) {
    goto on_error;
  }
  n_out = json_integer((json_int_t)params->n_out);
  if (!n_out) {
    goto on_error;
  }
  epsilon_A = json_real(params->epsilon_A);
  if (!epsilon_A) {
    goto on_error;
  }
  epsilon_w_out = json_real(params->epsilon_w_out);
  if (!epsilon_w_out) {
    goto on_error;
  }

  if (json_object_set(obj, "n-in", n_in) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "n-out", n_out) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "epsilon-A", epsilon_A) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "epsilon-w-out", epsilon_w_out) != 0) {
    goto on_error;
  }

  json_decref(epsilon_w_out);
  json_decref(epsilon_A);
  json_decref(n_out);
  json_decref(n_in);

  return obj;

on_error:
  if (epsilon_w_out) {
    json_decref(epsilon_w_out);
  }
  if (epsilon_A) {
    json_decref(epsilon_A);
  }
  if (n_out) {
    json_decref(n_out);
  }
  if (n_in) {
    json_decref(n_in);
  }
  if (obj) {
    json_decref(obj);
  }
  return NULL;
}

json_t *
slwr_pt_modify_parameters_to_json(const slwr_pt_modify_parameters *params) {
  g_return_val_if_fail(params != NULL, NULL);

  json_t *obj = NULL;
  json_t *n_in = NULL;
  json_t *epsilon = NULL;
  json_t *s = NULL;
  json_t *error_max = NULL;
  json_t *dist_max = NULL;

  obj = json_object();
  if (!obj) {
    goto on_error;
  }
  n_in = json_integer((json_int_t)params->n_in);
  if (!n_in) {
    goto on_error;
  }
  epsilon = json_real(params->epsilon);
  if (!epsilon) {
    goto on_error;
  }
  s = json_real(params->s);
  if (!s) {
    goto on_error;
  }
  error_max = json_real(params->error_max);
  if (!error_max) {
    goto on_error;
  }
  dist_max = json_real(params->dist_max);
  if (!dist_max) {
    goto on_error;
  }

  if (json_object_set(obj, "n-in", n_in) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "epsilon", epsilon) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "s", s) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "error-max", error_max) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "dist-max", dist_max) != 0) {
    goto on_error;
  }

  json_decref(dist_max);
  json_decref(error_max);
  json_decref(s);
  json_decref(epsilon);
  json_decref(n_in);

  return obj;

on_error:
  if (dist_max) {
    json_decref(dist_max);
  }
  if (error_max) {
    json_decref(error_max);
  }
  if (s) {
    json_decref(s);
  }
  if (epsilon) {
    json_decref(epsilon);
  }
  if (n_in) {
    json_decref(n_in);
  }
  if (obj) {
    json_decref(obj);
  }
  return NULL;
}

json_t *slwr_network_parameters_to_json(const slwr_network_parameters *params) {
  g_return_val_if_fail(params != NULL, NULL);

  json_t *obj = NULL;
  json_t *n_in = NULL;
  json_t *n_out = NULL;
  json_t *D_init = NULL;
  json_t *max_models = NULL;
  json_t *model_allocation = NULL;
  json_t *decay_error_max = NULL;
  json_t *apply_strict_decay_error_max = NULL;
  json_t *error_max = NULL;
  json_t *model_pruning = NULL;
  json_t *error_guided_insertion_error_source = NULL;
  json_t *w_gen = NULL;
  json_t *w_prune = NULL;
  json_t *lambda_init = NULL;
  json_t *lambda_tau = NULL;
  json_t *lambda_final = NULL;

  obj = json_object();
  if (!obj) {
    goto on_error;
  }
  n_in = json_integer((json_int_t)params->n_in);
  if (!n_in) {
    goto on_error;
  }
  n_out = json_integer((json_int_t)params->n_out);
  if (!n_out) {
    goto on_error;
  }
  D_init = slwr_matrix_to_json(params->D_init);
  if (!D_init) {
    goto on_error;
  }
  max_models = json_integer((json_int_t)params->max_models);
  if (!max_models) {
    goto on_error;
  }
  model_allocation = json_string(params->model_allocation);
  if (!model_allocation) {
    goto on_error;
  }
  decay_error_max = json_string(params->decay_error_max ? "on" : "off");
  if (!decay_error_max) {
    goto on_error;
  }
  apply_strict_decay_error_max =
      json_string(params->apply_strict_decay_error_max ? "on" : "off");
  if (!apply_strict_decay_error_max) {
    goto on_error;
  }
  error_max = json_real(params->error_max);
  if (!error_max) {
    goto on_error;
  }
  model_pruning = json_string(params->model_pruning ? "on" : "off");
  if (!model_pruning) {
    goto on_error;
  }
  error_guided_insertion_error_source =
      json_string(params->error_guided_insertion_error_source);
  if (!error_guided_insertion_error_source) {
    goto on_error;
  }
  w_gen = json_real(params->w_gen);
  if (!w_gen) {
    goto on_error;
  }
  w_prune = json_real(params->w_prune);
  if (!w_prune) {
    goto on_error;
  }
  lambda_init = json_real(params->lambda_init);
  if (!lambda_init) {
    goto on_error;
  }
  lambda_tau = json_real(params->lambda_tau);
  if (!lambda_tau) {
    goto on_error;
  }
  lambda_final = json_real(params->lambda_final);
  if (!lambda_final) {
    goto on_error;
  }

  if (json_object_set(obj, "n-in", n_in) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "n-out", n_out) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "D-init", D_init) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "max-models", max_models) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "model-allocation", model_allocation) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "decay-error-max", decay_error_max) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "apply-strict-decay-error-max",
                      apply_strict_decay_error_max) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "error-max", error_max) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "model-pruning", model_pruning) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "error-guided-insertion-error-source",
                      error_guided_insertion_error_source) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "w-gen", w_gen) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "w-prune", w_prune) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "lambda-init", lambda_init) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "lambda-tau", lambda_tau) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "lambda-final", lambda_final) != 0) {
    goto on_error;
  }

  json_decref(lambda_final);
  json_decref(lambda_tau);
  json_decref(lambda_init);
  json_decref(w_prune);
  json_decref(w_gen);
  json_decref(error_guided_insertion_error_source);
  json_decref(model_pruning);
  json_decref(error_max);
  json_decref(apply_strict_decay_error_max);
  json_decref(decay_error_max);
  json_decref(model_allocation);
  json_decref(max_models);
  json_decref(D_init);
  json_decref(n_out);
  json_decref(n_in);

  return obj;

on_error:
  if (lambda_final) {
    json_decref(lambda_final);
  }
  if (lambda_tau) {
    json_decref(lambda_tau);
  }
  if (lambda_init) {
    json_decref(lambda_init);
  }
  if (w_prune) {
    json_decref(w_prune);
  }
  if (w_gen) {
    json_decref(w_gen);
  }
  if (error_guided_insertion_error_source) {
    json_decref(error_guided_insertion_error_source);
  }
  if (model_pruning) {
    json_decref(model_pruning);
  }
  if (error_max) {
    json_decref(error_max);
  }
  if (apply_strict_decay_error_max) {
    json_decref(apply_strict_decay_error_max);
  }
  if (decay_error_max) {
    json_decref(decay_error_max);
  }
  if (model_allocation) {
    json_decref(model_allocation);
  }
  if (max_models) {
    json_decref(max_models);
  }
  if (D_init) {
    json_decref(D_init);
  }
  if (n_out) {
    json_decref(n_out);
  }
  if (n_in) {
    json_decref(n_in);
  }
  if (obj) {
    json_decref(obj);
  }
  return NULL;
}

json_t *slwr_vector_to_json(const slwr_vector *v) {
  g_return_val_if_fail(v != NULL, NULL);
  g_return_val_if_fail(v->data != NULL, NULL);

  json_t *obj = json_array();
  for (gsize i = 0; i < v->size; ++i) {
    json_t *col = json_array();
    json_t *entry = json_real(v->data[i]);
    json_array_append(col, entry);
    json_array_append(obj, col);
    json_decref(entry);
    json_decref(col);
  }
  return obj;
}

slwr_vector *slwr_vector_from_json(const json_t *json) {
  g_return_val_if_fail(json != NULL, NULL);
  g_return_val_if_fail(json_is_array(json), NULL);

  slwr_vector *v = NULL;
  gsize n_in = json_array_size(json);
  gsize i;
  json_t *inner_array;
  json_t *element;

  v = slwr_vector_alloc(n_in);
  if (!v) {
    return NULL;
  }
  json_array_foreach(json, i, inner_array) {
    if (!json_is_array(inner_array)) {
      g_critical("%s: expected array elements to be arrays",
                 __PRETTY_FUNCTION__);
      goto on_error;
    }
    if (json_array_size(inner_array) != 1) {
      g_critical("%s: expected inner array elements to have size 1",
                 __PRETTY_FUNCTION__);
      goto on_error;
    }
    element = json_array_get(inner_array, 0);
    if (json_number_to_double(element, v->data + i) != SLWR_STATUS_OK) {
      goto on_error;
    }
  }
  return v;

on_error:
  if (v) {
    slwr_vector_free(v);
  }
  return NULL;
}

json_t *slwr_matrix_to_json(const slwr_matrix *m) {
  g_return_val_if_fail(m != NULL, NULL);
  g_return_val_if_fail(m->data != NULL, NULL);

  json_t *obj = json_array();
  for (gsize i = 0; i < m->size1; ++i) {
    json_t *D_row = json_array();
    for (gsize j = 0; j < m->size2; ++j) {
      json_t *entry = json_real(m->data[i * m->size2 + j]);
      json_array_append(D_row, entry);
      json_decref(entry);
    }
    json_array_append(obj, D_row);
    json_decref(D_row);
  }
  return obj;
}

json_t *slwr_receptive_field_to_json(const slwr_receptive_field *rf) {
  g_return_val_if_fail(rf != NULL, NULL);
  g_return_val_if_fail(rf->D != NULL, NULL);
  g_return_val_if_fail(rf->c != NULL, NULL);

  json_t *obj = json_object();

  json_t *c = slwr_vector_to_json(rf->c);
  json_t *D = slwr_matrix_to_json(rf->D);
  json_object_set(obj, "c", c);
  json_object_set(obj, "D", D);
  json_decref(D);
  json_decref(c);

  return obj;
}

json_t *slwr_gng_context_to_json(const slwr_gng_context *ctx) {
  g_return_val_if_fail(ctx != NULL, NULL);
  g_return_val_if_fail(ctx->nodes != NULL, NULL);
  g_return_val_if_fail(ctx->edges != NULL, NULL);

  json_t *obj = json_object();
  json_t *node_array = json_array();
  json_t *edge_array = json_array();

  for (gsize i = 0; i < ctx->nodes->len; ++i) {
    const slwr_gng_node *n = g_ptr_array_index(ctx->nodes, i);

    json_t *node_obj = json_object();
    json_t *rf_obj = slwr_receptive_field_to_json(n->rf);
    json_object_set(node_obj, "receptive-field", rf_obj);
    json_decref(rf_obj);

    json_t *input_space_error = json_real(n->input_space_error);
    json_object_set(node_obj, "input-space-error", input_space_error);
    json_decref(input_space_error);

    json_t *output_space_error = json_real(n->output_space_error);
    json_object_set(node_obj, "output-space-error", output_space_error);
    json_decref(output_space_error);

    json_array_append(node_array, node_obj);
    json_decref(node_obj);
  }

  for (gsize i = 0; i < ctx->edges->len; ++i) {
    const slwr_gng_edge *e = g_ptr_array_index(ctx->edges, i);

    json_t *edge_obj = json_object();
    json_t *ac_obj = slwr_vector_to_json(e->a->rf->c);
    json_t *bc_obj = slwr_vector_to_json(e->b->rf->c);

    json_object_set(edge_obj, "a", ac_obj);
    json_decref(ac_obj);
    json_object_set(edge_obj, "b", bc_obj);
    json_decref(bc_obj);

    json_array_append(edge_array, edge_obj);
    json_decref(edge_obj);
  }

  json_object_set(obj, "nodes", node_array);
  json_object_set(obj, "edges", edge_array);
  json_decref(edge_array);
  json_decref(node_array);

  return obj;
}

json_t *slwr_ols_context_to_json(const slwr_ols_context *ctx) {
  g_return_val_if_fail(ctx != NULL, NULL);
  g_return_val_if_fail(ctx->A != NULL, NULL);
  g_return_val_if_fail(ctx->w_out != NULL, NULL);

  json_t *obj = json_object();

  json_t *A_obj = slwr_matrix_to_json(ctx->A);
  json_object_set(obj, "A", A_obj);
  json_decref(A_obj);

  json_t *w_out_obj = slwr_vector_to_json(ctx->w_out);
  json_object_set(obj, "w_out", w_out_obj);
  json_decref(w_out_obj);

  return obj;
}

json_t *slwr_model_population_to_json(const slwr_model_population *population) {
  g_return_val_if_fail(population != NULL, NULL);
  g_return_val_if_fail(population->allocated_models != NULL, NULL);

  json_t *obj = NULL;
  json_t *rf_array = NULL;

  rf_array = json_array();
  for (gsize i = 0; i < population->allocated_models->len; ++i) {
    const slwr_network_model *model =
        (const slwr_network_model *)g_ptr_array_index(
            population->allocated_models, i);
    if (!model) {
      goto on_error;
    }
    obj = slwr_receptive_field_to_json(model->rf);
    if (!obj) {
      goto on_error;
    }
    json_array_append(rf_array, obj);
    json_decref(obj);
  }

  obj = json_object();
  json_object_set(obj, "receptive-fields", rf_array);
  json_decref(rf_array);
  return obj;

on_error:
  if (rf_array) {
    json_decref(rf_array);
  }
  return NULL;
}

json_t *slwr_mesh_2d_to_json(const slwr_matrix *xx1, const slwr_matrix *xx2,
                             const slwr_matrix *z) {
  g_return_val_if_fail(xx1 != NULL, NULL);
  g_return_val_if_fail(xx2 != NULL, NULL);
  g_return_val_if_fail(z != NULL, NULL);
  g_return_val_if_fail(xx1->size1 == xx2->size1, NULL);
  g_return_val_if_fail(xx1->size1 == z->size1, NULL);
  g_return_val_if_fail(xx1->size1 == xx1->size2, NULL);
  g_return_val_if_fail(xx2->size1 == xx2->size2, NULL);
  g_return_val_if_fail(z->size1 == z->size2, NULL);

  json_t *obj = NULL;
  json_t *xx1_obj = NULL;
  json_t *xx2_obj = NULL;
  json_t *z_obj = NULL;

  xx1_obj = slwr_matrix_to_json(xx1);
  if (!xx1_obj) {
    goto on_error;
  }
  xx2_obj = slwr_matrix_to_json(xx2);
  if (!xx2_obj) {
    goto on_error;
  }
  z_obj = slwr_matrix_to_json(z);
  if (!z_obj) {
    goto on_error;
  }

  obj = json_object();
  if (json_object_set(obj, "xx1", xx1_obj) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "xx2", xx2_obj) != 0) {
    goto on_error;
  }
  if (json_object_set(obj, "z", z_obj) != 0) {
    goto on_error;
  }

  json_decref(z_obj);
  json_decref(xx1_obj);
  json_decref(xx2_obj);
  return obj;

on_error:
  if (z_obj) {
    json_decref(z_obj);
  }
  if (xx2_obj) {
    json_decref(xx2_obj);
  }
  if (xx1_obj) {
    json_decref(xx1_obj);
  }
  if (obj) {
    json_decref(obj);
  }
  return NULL;
}

static slwr_status write_json_to_file(const json_t *obj, const gchar *filename,
                                      gsize flags) {
  if (json_dump_file(obj, filename, flags) != 0) {
    g_critical("failed writing json to file");
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

slwr_status slwr_write_json_to_file(const json_t *obj, const gchar *filename) {
  g_return_val_if_fail(obj != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);

  return write_json_to_file(obj, filename, JSON_INDENT(2));
}

slwr_status slwr_write_compact_json_to_file(const json_t *obj,
                                            const gchar *filename) {
  g_return_val_if_fail(obj != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);

  return write_json_to_file(obj, filename, JSON_COMPACT);
}
