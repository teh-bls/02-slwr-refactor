#pragma once

#include "../common/slwr-types.h"

slwr_status slwr_vector_difference(slwr_workspace *ws, const slwr_vector *a,
                                   const slwr_vector *b, slwr_vector *ab);

slwr_status slwr_euclidean_distance(slwr_workspace *ws, const slwr_vector *a,
                                    const slwr_vector *b, gdouble *distance);

slwr_vector *slwr_linspace(gdouble start, gdouble stop, gsize num,
                           gboolean include_endpoint);

void slwr_swap_double(gdouble *a, gdouble *b);
