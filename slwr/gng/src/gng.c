#include "gng.h"

#include "slwr/common/src/workspace.h"
#include "slwr/utils/src/math-utils.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>

static inline gsl_vector *node_center(slwr_workspace *ws, slwr_gng_node *node) {
  return workspace_gsl_vector_from_slwr_vector(ws, node->rf->c, WORKSPACE_N_IN);
}

slwr_status gng_find_closest_nodes(slwr_workspace *ws, const gsl_vector *x,
                                   GPtrArray *nodes,
                                   struct gng_node_pair *closest_nodes) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(nodes != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(closest_nodes != NULL, SLWR_STATUS_ERROR);

  closest_nodes->s1 = NULL;
  closest_nodes->s2 = NULL;

  gdouble min_dist1, min_dist2;
  gsl_vector *node_0_c;
  gsl_vector *node_1_c;

  if (nodes->len == 1) {
    closest_nodes->s1 = g_ptr_array_index(nodes, 0);
    return SLWR_STATUS_OK;
  }

  node_0_c = node_center(ws, g_ptr_array_index(nodes, 0));
  if (!node_0_c) {
    return SLWR_STATUS_ERROR;
  }
  node_1_c = node_center(ws, g_ptr_array_index(nodes, 1));
  if (!node_1_c) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = utils_euclidean_distance(ws, node_0_c, x, &min_dist1);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  status = utils_euclidean_distance(ws, node_1_c, x, &min_dist2);
  if (status != SLWR_STATUS_OK) {
    return status;
  }

  if (min_dist1 < min_dist2) {
    closest_nodes->s1 = g_ptr_array_index(nodes, 0);
    closest_nodes->s2 = g_ptr_array_index(nodes, 1);
  } else {
    closest_nodes->s1 = g_ptr_array_index(nodes, 1);
    closest_nodes->s2 = g_ptr_array_index(nodes, 0);
    slwr_swap_double(&min_dist1, &min_dist2);
  }

  for (size_t i = 2; i < nodes->len; ++i) {
    slwr_gng_node *n = g_ptr_array_index(nodes, i);
    gdouble dist_n;
    if (utils_euclidean_distance(ws, node_center(ws, n), x, &dist_n) !=
            SLWR_STATUS_OK ||
        gsl_isnan(dist_n)) {
      return SLWR_STATUS_ERROR;
    }
    if (dist_n < min_dist1) {
      closest_nodes->s2 = closest_nodes->s1;
      min_dist2 = min_dist1;
      closest_nodes->s1 = n;
      min_dist1 = dist_n;
    } else if (dist_n < min_dist2) {
      closest_nodes->s2 = n;
      min_dist2 = dist_n;
    }
  }

  return SLWR_STATUS_OK;
}

void gng_increment_edge_age(const slwr_gng_node *n, GPtrArray *edges) {
  g_return_if_fail(n != NULL);
  g_return_if_fail(edges != NULL);

  for (gsize i = 0; i < edges->len; ++i) {
    slwr_gng_edge *e = g_ptr_array_index(edges, i);
    if (e->a == n || e->b == n) {
      ++e->age;
    }
  }
}

slwr_status gng_accumulate_input_space_node_error(slwr_workspace *ws,
                                                  slwr_gng_node *n,
                                                  const gsl_vector *x) {
  gdouble distance;
  if (utils_euclidean_distance(ws, x, node_center(ws, n), &distance) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  n->input_space_error += distance * distance;
  return SLWR_STATUS_OK;
}

slwr_status gng_update_node_center(slwr_workspace *ws, slwr_gng_node *n,
                                   const gsl_vector *x, double epsilon) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(n != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);

  gsl_vector *c = node_center(ws, n);
  if (!c) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *delta = workspace_vector(ws, WORKSPACE_N_IN);
  if (!delta) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_memcpy(delta, x) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_sub(delta, c) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_scale(delta, epsilon) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_add(c, delta) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }

  if (slwr_vector_from_gsl_vector(n->rf->c, c) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }

  return SLWR_STATUS_OK;
}

static slwr_status update_neighborhood(slwr_workspace *ws, slwr_gng_node *n,
                                       const slwr_gng_parameters *params,
                                       const gsl_vector *x, GPtrArray *edges) {
  if (gng_update_node_center(ws, n, x, params->epsilon_n) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  for (gsize i = 0; i < edges->len; ++i) {
    const slwr_gng_edge *e = g_ptr_array_index(edges, i);
    if (e->a == n) {
      if (gng_update_node_center(ws, e->b, x, params->epsilon_b) !=
          SLWR_STATUS_OK) {
        return SLWR_STATUS_ERROR;
      }
    }
    if (e->b == n) {
      if (gng_update_node_center(ws, e->a, x, params->epsilon_b) !=
          SLWR_STATUS_OK) {
        return SLWR_STATUS_ERROR;
      }
    }
  }

  return SLWR_STATUS_OK;
}

static inline gboolean is_edge_between(const slwr_gng_edge *edge,
                                       const slwr_gng_node *a,
                                       const slwr_gng_node *b) {
  return (edge->a == a && edge->b == b) || (edge->a == b && edge->b == a);
}

slwr_status gng_update_edge_between(slwr_gng_node *a, slwr_gng_node *b,
                                    GPtrArray *edges) {
  gboolean edge_exists = FALSE;
  for (gsize i = 0; i < edges->len; ++i) {
    slwr_gng_edge *e = g_ptr_array_index(edges, i);
    if (is_edge_between(e, a, b)) {
      edge_exists = TRUE;
      e->age = 0;
      break;
    }
  }
  if (!edge_exists) {
    slwr_gng_edge *new_e = (slwr_gng_edge *)g_malloc0(sizeof(slwr_gng_edge));
    if (!new_e) {
      return SLWR_STATUS_ERROR;
    }
    new_e->a = a;
    new_e->b = b;
    new_e->age = 0;
    g_ptr_array_add(edges, new_e);
  }
  return SLWR_STATUS_OK;
}

slwr_status gng_prune_edges(slwr_gng_context *ctx,
                            const slwr_gng_parameters *params) {
  /* TODO: I believe that I'm missing the removal of the nodes that have no
     edges emanating from them; I shouldn't introduce this (since I would like
     to prune nodes based on the output space error), but I should make an
     explicit note about this */
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->edges != NULL, SLWR_STATUS_ERROR);
  for (gsize i = 0; i < ctx->edges->len; ++i) {
    slwr_gng_edge *e = g_ptr_array_index(ctx->edges, i);
    if (e->age > params->max_age) {
      g_ptr_array_remove_index(ctx->edges, i);
      ++i;
    }
  }
  return SLWR_STATUS_OK;
}

slwr_status gng_decrease_input_space_errors(slwr_gng_context *ctx,
                                            const slwr_gng_parameters *params) {
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);

  for (gsize i = 0; i < ctx->nodes->len; ++i) {
    slwr_gng_node *n = g_ptr_array_index(ctx->nodes, i);
    n->input_space_error *= params->d;
  }
  return SLWR_STATUS_OK;
}

static slwr_status gng_update(slwr_workspace *ws, slwr_gng_context *ctx,
                              const slwr_gng_parameters *params,
                              const gsl_vector *x, gsize sample_count) {
  struct gng_node_pair closest_nodes = {.s1 = NULL, .s2 = NULL};
  if (gng_find_closest_nodes(ws, x, ctx->nodes, &closest_nodes) !=
      SLWR_STATUS_OK) {
    g_critical("could not find two closest nodes to the given input x");
    return SLWR_STATUS_ERROR;
  }

  gng_increment_edge_age(closest_nodes.s1, ctx->edges);

  if (gng_accumulate_input_space_node_error(ws, closest_nodes.s1, x) !=
      SLWR_STATUS_OK) {
    g_critical("could not accumulate the input space error");
    return SLWR_STATUS_ERROR;
  }

  if (update_neighborhood(ws, closest_nodes.s1, params, x, ctx->edges) !=
      SLWR_STATUS_OK) {
    g_critical("could not update the neighborhood");
    return SLWR_STATUS_ERROR;
  }

  if (gng_update_edge_between(closest_nodes.s1, closest_nodes.s2, ctx->edges) !=
      SLWR_STATUS_OK) {
    g_critical("could not update the edge between closest nodes");
    return SLWR_STATUS_ERROR;
  }

  if (sample_count > 0 && sample_count % params->insertion_interval == 0) {
    if (!gng_insert_node_with_respect_to_input_error(ws, ctx, params)) {
      g_critical("could not insert a new node");
      return SLWR_STATUS_ERROR;
    }
  }

  if (gng_prune_edges(ctx, params) != SLWR_STATUS_OK) {
    g_critical("could not prune edges");
    return SLWR_STATUS_ERROR;
  }

  if (gng_decrease_input_space_errors(ctx, params) != SLWR_STATUS_OK) {
    g_critical("could not decrease input space errors");
    return SLWR_STATUS_ERROR;
  }

  return SLWR_STATUS_OK;
}

slwr_status slwr_gng_update(slwr_workspace *ws, slwr_gng_context *ctx,
                            const slwr_gng_parameters *params,
                            const slwr_vector *x, gsize sample_count) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->nodes != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->edges != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);

  g_return_val_if_fail(ctx->nodes->len > 1, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->edges->len > 0, SLWR_STATUS_ERROR);

  slwr_workspace_reset(ws);
  gsl_vector *gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = gng_update(ws, ctx, params, gx, sample_count);
  slwr_workspace_reset(ws);
  return status;
}

/**
 * Remove a given node and its associated edges from the GNG context.
 *
 * @param ctx The GNG context to remove the node from.
 * @param node The node to remove.
 * @return SLWR_STATUS_OK on successful removal of node from the context,
 * SLWR_STATUS_NOT_FOUND if the node to remove is not present in the context.
 */
slwr_status gng_remove_node(slwr_gng_context *ctx, slwr_gng_node *node) {
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->nodes != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->edges != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(node != NULL, SLWR_STATUS_ERROR);

  gint removal_index = -1;
  for (gint i = 0; i < ctx->nodes->len; ++i) {
    if (node == g_ptr_array_index(ctx->nodes, i)) {
      removal_index = i;
      break;
    }
  }
  if (removal_index < 0) {
    g_critical("node to remove is not in the nodes array");
    return SLWR_STATUS_NOT_FOUND;
  }
  g_ptr_array_remove_index_fast(ctx->nodes, removal_index);

  for (gsize i = 0; i < ctx->edges->len;) {
    slwr_gng_edge *e = g_ptr_array_index(ctx->edges, i);
    if (e->a == node || e->b == node) {
      g_ptr_array_remove_index_fast(ctx->edges, i);
    } else {
      ++i;
    }
  }
  return SLWR_STATUS_OK;
}

#define FIND_MAX_ERROR_NODE(node_array, max_error_node, error_to_use)          \
  do {                                                                         \
    for (gsize i = 1; i < node_array->len; ++i) {                              \
      if (((slwr_gng_node *)g_ptr_array_index(node_array, i))->error_to_use >  \
          (max_error_node)->error_to_use) {                                    \
        (max_error_node) = (slwr_gng_node *)g_ptr_array_index(node_array, i);  \
      }                                                                        \
    }                                                                          \
  } while (0)

slwr_gng_node *gng_find_max_input_error_node(GPtrArray *nodes) {
  g_return_val_if_fail(nodes != NULL, NULL);
  g_return_val_if_fail(nodes->len > 0, NULL);

  slwr_gng_node *max_error_node = g_ptr_array_index(nodes, 0);
  FIND_MAX_ERROR_NODE(nodes, max_error_node, input_space_error);

  return max_error_node;
}

slwr_gng_node *gng_find_max_output_error_node(GPtrArray *nodes) {
  g_return_val_if_fail(nodes != NULL, NULL);
  g_return_val_if_fail(nodes->len > 0, NULL);

  slwr_gng_node *max_error_node = g_ptr_array_index(nodes, 0);
  FIND_MAX_ERROR_NODE(nodes, max_error_node, output_space_error);

  return max_error_node;
}
#undef FIND_MAX_ERROR_NODE

slwr_status gng_find_neighborhood(const slwr_gng_node *node,
                                  const GPtrArray *edges,
                                  GPtrArray *neighborhood) {
  g_return_val_if_fail(node != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(edges != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(edges->len > 0, SLWR_STATUS_ERROR);
  g_return_val_if_fail(neighborhood != NULL, SLWR_STATUS_ERROR);

  g_ptr_array_remove_range(neighborhood, 0, neighborhood->len);
  for (gsize i = 0; i < edges->len; ++i) {
    const slwr_gng_edge *e = g_ptr_array_index(edges, i);
    if (e->a == node) {
      g_ptr_array_add(neighborhood, e->b);
    }
    if (e->b == node) {
      g_ptr_array_add(neighborhood, e->a);
    }
  }

  return SLWR_STATUS_OK;
}

static slwr_gng_node *insert_node_between(slwr_workspace *ws,
                                          const slwr_gng_node *a,
                                          const slwr_gng_node *b,
                                          GPtrArray *nodes,
                                          slwr_matrix *D_template) {
  slwr_gng_node *n = (slwr_gng_node *)g_malloc0(sizeof(slwr_gng_node));
  if (!n) {
    return NULL;
  }
  const gsize n_in = a->rf->c->size;
  n->rf = slwr_receptive_field_alloc(n_in);
  n->input_space_error = 0.0;
  n->output_space_error = 0.0;

  gsl_vector *gn_c =
      workspace_gsl_vector_from_slwr_vector(ws, a->rf->c, WORKSPACE_N_IN);
  if (!gn_c) {
    return NULL;
  }
  gsl_vector *gb_c =
      workspace_gsl_vector_from_slwr_vector(ws, b->rf->c, WORKSPACE_N_IN);
  if (!gb_c) {
    return NULL;
  }

  if (gsl_vector_add(gn_c, gb_c) != GSL_SUCCESS) {
    return NULL;
  }
  if (gsl_vector_scale(gn_c, 0.5) != GSL_SUCCESS) {
    return NULL;
  }
  if (slwr_vector_from_gsl_vector(n->rf->c, gn_c) != SLWR_STATUS_OK) {
    return NULL;
  }

  if (D_template) {
    if (slwr_matrix_copy(n->rf->D, D_template) != SLWR_STATUS_OK) {
      return NULL;
    }
  }

  g_ptr_array_add(nodes, n);
  return n;
}

slwr_gng_node *gng_insert_node_between(slwr_workspace *ws,
                                       slwr_gng_context *ctx,
                                       const slwr_gng_node *a,
                                       const slwr_gng_node *b,
                                       slwr_matrix *D_template) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(ctx != NULL, NULL);
  g_return_val_if_fail(ctx->nodes != NULL, NULL);
  g_return_val_if_fail(a != NULL, NULL);
  g_return_val_if_fail(b != NULL, NULL);

  return insert_node_between(ws, a, b, ctx->nodes, D_template);
}

static slwr_status modify_edges_after_node_insertion(slwr_gng_node *q,
                                                     slwr_gng_node *f,
                                                     slwr_gng_node *r,
                                                     GPtrArray *edges) {
  slwr_gng_edge *q_f_edge = NULL;
  for (size_t i = 0; i < edges->len; ++i) {
    slwr_gng_edge *e = g_ptr_array_index(edges, i);
    if (is_edge_between(e, q, f)) {
      q_f_edge = e;
      break;
    }
  }
  g_assert_nonnull(q_f_edge);

  /* The edge between q and f becomes the edge between q and r */
  q_f_edge->a = q;
  q_f_edge->b = r;

  slwr_gng_edge *r_f_edge = (slwr_gng_edge *)g_malloc0(sizeof(slwr_gng_edge));
  if (!r_f_edge) {
    return SLWR_STATUS_ERROR;
  }
  r_f_edge->a = r;
  r_f_edge->b = f;
  r_f_edge->age = 0;

  g_ptr_array_add(edges, r_f_edge);
  return SLWR_STATUS_OK;
}

slwr_status gng_modify_edges_after_node_insertion(slwr_gng_context *ctx,
                                                  slwr_gng_node *q,
                                                  slwr_gng_node *f,
                                                  slwr_gng_node *r) {
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->edges != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx->edges->len > 0, SLWR_STATUS_ERROR);
  g_return_val_if_fail(q != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(f != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(r != NULL, SLWR_STATUS_ERROR);

  return modify_edges_after_node_insertion(q, f, r, ctx->edges);
}

typedef slwr_gng_node *(*find_node_fn)(GPtrArray *);
static slwr_gng_node *insert_node(slwr_workspace *ws, slwr_gng_context *ctx,
                                  const slwr_gng_parameters *params,
                                  const find_node_fn find_node) {
  slwr_gng_node *q = find_node(ctx->nodes);
  if (!q) {
    return NULL;
  }

  if (gng_find_neighborhood(q, ctx->edges, ctx->neighborhood) !=
      SLWR_STATUS_OK) {
    return NULL;
  }
  if (ctx->neighborhood->len == 0) {
    /* TODO: address this properly, i.e. by reconsidering the allocation
     * procedure. */
    g_critical("The node with the highest error does not have a neighborhood. "
               "This is a consequence of allowing \"dead\" nodes to remain in "
               "the population after edge removal. It needs to be addressed.");
    return NULL;
  }
  slwr_gng_node *f = find_node(ctx->neighborhood);
  if (!f) {
    return NULL;
  }

  slwr_gng_node *r = gng_insert_node_between(ws, ctx, q, f, NULL);
  if (!r) {
    return NULL;
  }
  if (gng_modify_edges_after_node_insertion(ctx, q, f, r) != SLWR_STATUS_OK) {
    return NULL;
  }

  q->input_space_error *= params->alpha_error;
  f->input_space_error *= params->alpha_error;
  r->input_space_error = q->input_space_error;

  return r;
}

slwr_gng_node *
gng_insert_node_with_respect_to_input_error(slwr_workspace *ws,
                                            slwr_gng_context *ctx,
                                            const slwr_gng_parameters *params) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(params != NULL, NULL);
  g_return_val_if_fail(ctx->nodes != NULL, NULL);
  g_return_val_if_fail(ctx->edges != NULL, NULL);

  return insert_node(ws, ctx, params, gng_find_max_input_error_node);
}

slwr_gng_node *gng_insert_node_with_respect_to_output_error(
    slwr_workspace *ws, slwr_gng_context *ctx,
    const slwr_gng_parameters *params) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(params != NULL, NULL);
  g_return_val_if_fail(ctx->nodes != NULL, NULL);
  g_return_val_if_fail(ctx->edges != NULL, NULL);

  return insert_node(ws, ctx, params, gng_find_max_output_error_node);
}

void slwr_gng_parameters_default_values(slwr_gng_parameters *params) {
  g_return_if_fail(params != NULL);

  params->max_age = 100;
  params->insertion_interval = 600;
  params->alpha_error = 0.5;
  params->epsilon_n = 0.05;
  params->epsilon_b = 0.0001;
  params->d = 0.995;
}

slwr_gng_context *slwr_gng_context_alloc() {
  slwr_gng_context *ctx =
      (slwr_gng_context *)g_malloc0(sizeof(slwr_gng_context));
  if (!ctx) {
    return NULL;
  }

  ctx->nodes =
      g_ptr_array_new_with_free_func((GDestroyNotify)slwr_gng_node_free);
  ctx->edges =
      g_ptr_array_new_with_free_func((GDestroyNotify)slwr_gng_edge_free);
  ctx->neighborhood = g_ptr_array_new();

  return ctx;
}

void slwr_gng_context_free(slwr_gng_context *ctx) {
  if (!ctx) {
    return;
  }
  if (ctx->neighborhood) {
    g_ptr_array_free(ctx->neighborhood, TRUE);
  }
  if (ctx->edges) {
    g_ptr_array_free(ctx->edges, TRUE);
  }
  if (ctx->nodes) {
    g_ptr_array_free(ctx->nodes, TRUE);
  }
  g_free(ctx);
}

slwr_gng_node *slwr_gng_node_alloc(gsize n_in) {
  slwr_gng_node *n = (slwr_gng_node *)g_malloc0(sizeof(slwr_gng_node));
  if (!n) {
    return NULL;
  }
  n->rf = slwr_receptive_field_alloc(n_in);
  if (!n->rf) {
    g_free(n);
    return NULL;
  }
  return n;
}

void slwr_gng_node_free(slwr_gng_node *node) {
  if (!node) {
    return;
  }
  slwr_receptive_field_free(node->rf);
  g_free(node);
}

slwr_gng_edge *slwr_gng_edge_alloc() {
  slwr_gng_edge *e = (slwr_gng_edge *)g_malloc0(sizeof(slwr_gng_edge));
  return e;
}

void slwr_gng_edge_free(slwr_gng_edge *edge) {
  if (!edge) {
    return;
  }
  g_free(edge);
}
