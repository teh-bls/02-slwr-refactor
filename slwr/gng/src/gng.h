#pragma once

#include "slwr/gng/slwr-gng.h"

#include <gsl/gsl_vector.h>

struct gng_node_pair {
  slwr_gng_node *s1;
  slwr_gng_node *s2;
};

slwr_status gng_remove_node(slwr_gng_context *ctx, slwr_gng_node *node);

slwr_gng_node *gng_find_max_input_error_node(GPtrArray *nodes);
slwr_gng_node *gng_find_max_output_error_node(GPtrArray *nodes);

slwr_status gng_update_node_center(slwr_workspace *ws, slwr_gng_node *n,
                                   const gsl_vector *x, double epsilon);

slwr_status gng_find_closest_nodes(slwr_workspace *ws, const gsl_vector *x,
                                   GPtrArray *nodes,
                                   struct gng_node_pair *closest_nodes);

void gng_increment_edge_age(const slwr_gng_node *n, GPtrArray *edges);

slwr_status gng_accumulate_input_space_node_error(slwr_workspace *ws,
                                                  slwr_gng_node *n,
                                                  const gsl_vector *x);

slwr_status gng_update_edge_between(slwr_gng_node *a, slwr_gng_node *b,
                                    GPtrArray *edges);

slwr_status gng_find_neighborhood(const slwr_gng_node *node,
                                  const GPtrArray *edges,
                                  GPtrArray *neighborhood);

slwr_gng_node *gng_insert_node_between(slwr_workspace *ws,
                                       slwr_gng_context *ctx,
                                       const slwr_gng_node *a,
                                       const slwr_gng_node *b,
                                       slwr_matrix *D_template);

slwr_status gng_modify_edges_after_node_insertion(slwr_gng_context *ctx,
                                                  slwr_gng_node *q,
                                                  slwr_gng_node *f,
                                                  slwr_gng_node *r);

slwr_gng_node *
gng_insert_node_with_respect_to_input_error(slwr_workspace *ws,
                                            slwr_gng_context *ctx,
                                            const slwr_gng_parameters *params);

slwr_gng_node *
gng_insert_node_with_respect_to_output_error(slwr_workspace *ws,
                                             slwr_gng_context *ctx,
                                             const slwr_gng_parameters *params);

slwr_status gng_prune_edges(slwr_gng_context *ctx,
                            const slwr_gng_parameters *params);

slwr_status gng_decrease_input_space_errors(slwr_gng_context *ctx, const slwr_gng_parameters *params);
