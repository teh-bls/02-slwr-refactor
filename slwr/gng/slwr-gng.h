#pragma once

#include "../common/slwr-types.h"
#include "../receptive-field/slwr-receptive-field.h"

#include <glib.h>

typedef struct {
  gdouble input_space_error;
  gdouble output_space_error;
  slwr_receptive_field *rf;
} slwr_gng_node;

typedef struct {
  slwr_gng_node *a;
  slwr_gng_node *b;
  gsize age;
} slwr_gng_edge;

typedef struct {
  gsize max_age;
  gsize insertion_interval;
  gdouble alpha_error;
  gdouble epsilon_n;
  gdouble epsilon_b;
  gdouble d;
} slwr_gng_parameters;

typedef struct {
  GPtrArray *nodes;
  GPtrArray *edges;
  GPtrArray *neighborhood;
} slwr_gng_context;

slwr_gng_context *slwr_gng_context_alloc();
void slwr_gng_context_free(slwr_gng_context *ctx);

slwr_gng_node *slwr_gng_node_alloc(gsize n_in);
void slwr_gng_node_free(slwr_gng_node *node);

slwr_gng_edge *slwr_gng_edge_alloc();
void slwr_gng_edge_free(slwr_gng_edge *edge);

slwr_status slwr_gng_update(slwr_workspace *ws, slwr_gng_context *ctx,
                            const slwr_gng_parameters *params,
                            const slwr_vector *x, gsize sample_count);

void slwr_gng_parameters_default_values(slwr_gng_parameters *params);
