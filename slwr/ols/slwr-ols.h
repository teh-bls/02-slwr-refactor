#pragma once

#include "slwr/common/slwr-types.h"
#include "slwr/receptive-field/slwr-receptive-field.h"

#include <glib.h>

typedef struct {
  gsize n_in;
  gsize n_out;
  gdouble epsilon_A;
  gdouble epsilon_w_out;
} slwr_ols_parameters;

typedef struct {
  slwr_vector *w_out;
  slwr_matrix *A;
} slwr_ols_context;

slwr_ols_context *slwr_ols_context_alloc(const slwr_ols_parameters *params);
void slwr_ols_context_free(slwr_ols_context *ctx);

slwr_status slwr_ols_update(slwr_workspace *ws, slwr_ols_context *ctx,
                            const slwr_ols_parameters *params,
                            const slwr_receptive_field *rf,
                            const slwr_vector *x, const slwr_vector *y);

slwr_status slwr_ols_predict(slwr_workspace *ws, const slwr_ols_context *ctx,
                             const slwr_vector *c, const slwr_vector *x,
                             slwr_vector *y_pred);

void slwr_ols_parameters_default_values(gsize n_in, gsize n_out,
                                        slwr_ols_parameters *params);
