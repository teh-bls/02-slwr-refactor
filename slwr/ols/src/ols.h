#pragma once

#include "slwr/common/slwr-workspace.h"
#include "slwr/ols/slwr-ols.h"
#include "slwr/receptive-field/slwr-receptive-field.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

slwr_status ols_update_slwr(slwr_workspace *ws, slwr_ols_context *ctx,
                            const slwr_ols_parameters *params,
                            const slwr_vector *c, gdouble w,
                            const slwr_vector *x, const slwr_vector *y);

slwr_status ols_predict_slwr(slwr_workspace *ws, const slwr_ols_context *ctx,
                             const slwr_vector *c, const slwr_vector *x,
                             slwr_vector *y_pred);

slwr_status ols_update_gsl(slwr_workspace *ws, slwr_ols_context *ctx,
                           const slwr_ols_parameters *params,
                           const gsl_vector *c, gdouble w, const gsl_vector *x,
                           const gsl_vector *y);

slwr_status ols_predict_gsl(slwr_workspace *ws, const gsl_vector *c,
                            const gsl_matrix *A, const gsl_vector *w_out,
                            const gsl_vector *x, gsl_vector *y_pred);
