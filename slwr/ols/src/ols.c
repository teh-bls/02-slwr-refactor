#include "ols.h"

#include "slwr/common/src/workspace.h"
#include "slwr/ols/slwr-ols.h"
#include "slwr/receptive-field/src/receptive-field.h"
#include "slwr/utils/src/math-utils.h"

#include <gsl/gsl_blas.h>

slwr_ols_context *slwr_ols_context_alloc(const slwr_ols_parameters *params) {
  g_return_val_if_fail(params != NULL, NULL);

  slwr_ols_context *ctx =
      (slwr_ols_context *)g_malloc0(sizeof(slwr_ols_context));
  if (!ctx) {
    goto on_error;
  }
  ctx->A = slwr_matrix_alloc(params->n_in, params->n_out);
  if (!ctx->A) {
    goto on_error;
  }
  ctx->w_out = slwr_vector_alloc(params->n_out);
  if (!ctx->w_out) {
    goto on_error;
  }

  return ctx;

on_error:
  slwr_ols_context_free(ctx);
  return NULL;
}

void slwr_ols_context_free(slwr_ols_context *ctx) {
  if (!ctx) {
    return;
  }
  if (ctx->w_out) {
    slwr_vector_free(ctx->w_out);
  }
  if (ctx->A) {
    slwr_matrix_free(ctx->A);
  }
  g_free(ctx);
}

slwr_status ols_update_slwr(slwr_workspace *ws, slwr_ols_context *ctx,
                            const slwr_ols_parameters *params,
                            const slwr_vector *c, gdouble w,
                            const slwr_vector *x, const slwr_vector *y) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x->size == params->n_in, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(y != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y->size == params->n_out,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  gsl_vector *gc = workspace_gsl_vector_from_slwr_vector(ws, c, WORKSPACE_N_IN);
  if (!gc) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gy =
      workspace_gsl_vector_from_slwr_vector(ws, y, WORKSPACE_N_OUT);
  if (!gy) {
    return SLWR_STATUS_ERROR;
  }
  return ols_update_gsl(ws, ctx, params, gc, w, gx, gy);
}

slwr_status ols_predict_slwr(slwr_workspace *ws, const slwr_ols_context *ctx,
                             const slwr_vector *c, const slwr_vector *x,
                             slwr_vector *y_pred) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  gsl_vector *gc = workspace_gsl_vector_from_slwr_vector(ws, c, WORKSPACE_N_IN);
  if (!gc) {
    return SLWR_STATUS_ERROR;
  }
  gsl_matrix *A =
      workspace_gsl_matrix_from_slwr_matrix(ws, ctx->A, WORKSPACE_N_OUT);
  if (!A) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *w_out =
      workspace_gsl_vector_from_slwr_vector(ws, ctx->w_out, WORKSPACE_N_OUT);
  if (!w_out) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gy_pred = workspace_vector(ws, WORKSPACE_N_OUT);
  if (!gy_pred) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = ols_predict_gsl(ws, gc, A, w_out, gx, gy_pred);
  if (status == SLWR_STATUS_OK) {
    status = slwr_vector_from_gsl_vector(y_pred, gy_pred);
  }
  return status;
}

slwr_status ols_update_gsl(slwr_workspace *ws, slwr_ols_context *ctx,
                           const slwr_ols_parameters *params,
                           const gsl_vector *c, gdouble w, const gsl_vector *x,
                           const gsl_vector *y) {

  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y != NULL, SLWR_STATUS_ERROR);

  gsl_vector *y_pred = workspace_vector(ws, WORKSPACE_N_OUT);
  if (!y_pred) {
    return SLWR_STATUS_ERROR;
  }
  gsl_matrix *A =
      workspace_gsl_matrix_from_slwr_matrix(ws, ctx->A, WORKSPACE_N_OUT);
  if (!A) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *w_out =
      workspace_gsl_vector_from_slwr_vector(ws, ctx->w_out, WORKSPACE_N_OUT);
  if (!w_out) {
    return SLWR_STATUS_ERROR;
  }
  if (ols_predict_gsl(ws, c, A, w_out, x, y_pred) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *xc = workspace_vector(ws, WORKSPACE_N_IN);
  if (utils_vector_difference(x, c, xc) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *y_y_pred = workspace_vector(ws, WORKSPACE_N_OUT);
  if (!y_y_pred) {
    return SLWR_STATUS_ERROR;
  }
  if (utils_vector_difference(y, y_pred, y_y_pred) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_dger(w * params->epsilon_A, xc, y_y_pred, A) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_daxpy(w * params->epsilon_w_out, y_y_pred, w_out) !=
      GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }

  if (slwr_vector_from_gsl_vector(ctx->w_out, w_out) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (slwr_matrix_from_gsl_matrix(ctx->A, A) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }

  return SLWR_STATUS_OK;
}

slwr_status ols_predict_gsl(slwr_workspace *ws, const gsl_vector *c,
                            const gsl_matrix *A, const gsl_vector *w_out,
                            const gsl_vector *x, gsl_vector *y_pred) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(A != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(w_out != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  gsl_vector *xc = workspace_vector(ws, WORKSPACE_N_IN);
  if (!xc) {
    return SLWR_STATUS_ERROR;
  }
  if (utils_vector_difference(x, c, xc) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_memcpy(y_pred, w_out) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_dgemv(CblasTrans, 1.0, A, xc, 1.0, y_pred) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }

  return SLWR_STATUS_OK;
}

slwr_status slwr_ols_update(slwr_workspace *ws, slwr_ols_context *ctx,
                            const slwr_ols_parameters *params,
                            const slwr_receptive_field *rf,
                            const slwr_vector *x, const slwr_vector *y) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x->size == params->n_in, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(y != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y->size == params->n_out,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  slwr_workspace_reset(ws);
  gdouble w;
  if (determine_activation_slwr(ws, rf, x, SLWR_KERNEL_GAUSSIAN, &w) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = ols_update_slwr(ws, ctx, params, rf->c, w, x, y);
  slwr_workspace_reset(ws);
  return status;
}

/**
 * Predicts the output y for input x from the given OLS context.
 *
 * Resets the workspace.
 *
 * @param ws The workspace.
 * @param ctx The OLS context to predict from.
 * @param c The input vector offset (e.g. the center of a receptive field).
 * @param x The input vector.
 * @param y_pred The predicted output.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating error otherwise.
 */
slwr_status slwr_ols_predict(slwr_workspace *ws, const slwr_ols_context *ctx,
                             const slwr_vector *c, const slwr_vector *x,
                             slwr_vector *y_pred) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  slwr_workspace_reset(ws);
  slwr_status status = ols_predict_slwr(ws, ctx, c, x, y_pred);
  slwr_workspace_reset(ws);
  return status;
}

void slwr_ols_parameters_default_values(gsize n_in, gsize n_out,
                                        slwr_ols_parameters *params) {
  g_return_if_fail(params != NULL);

  params->n_in = n_in;
  params->n_out = n_out;
  params->epsilon_A = 0.300;
  params->epsilon_w_out = 0.300;
}
