#pragma once

#include "slwr/pt-modify/slwr-pt-modify.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

gdouble pt_modify_phi(gdouble s, gdouble error_max, gdouble error);
gdouble pt_modify_psi(gdouble s, gdouble dist_max, gdouble dist);
gdouble pt_modify_chi(gdouble s, gdouble error_max, gdouble dist_max,
                      gdouble error, gdouble dist);
gdouble pt_modify_upsilon(gdouble s, gdouble error_max, gdouble dist_max,
                          gdouble error, gdouble dist);
gdouble pt_modify_tau(gdouble s, gdouble error_max, gdouble dist_max,
                      gdouble error, gdouble dist);
gdouble pt_modify_rho(gdouble s, gdouble error_max, gdouble dist_max,
                      gdouble error, gdouble dist);

slwr_status pt_modify_update_slwr(slwr_workspace *ws,
                                  const slwr_pt_modify_parameters *params,
                                  slwr_pt_modify_weighting_function wf,
                                  slwr_receptive_field *rf,
                                  const slwr_vector *x, gdouble error);

slwr_status pt_modify_update_gsl(slwr_workspace *ws,
                                 const slwr_pt_modify_parameters *params,
                                 slwr_pt_modify_weighting_function wf,
                                 const gsl_vector *c, gsl_matrix *D,
                                 const gsl_vector *x, gdouble error);
