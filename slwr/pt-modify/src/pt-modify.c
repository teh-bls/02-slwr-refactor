#include "pt-modify.h"

#include "slwr/common/slwr-workspace.h"
#include "slwr/common/src/workspace.h"
#include "slwr/receptive-field/src/receptive-field.h"
#include "slwr/utils/src/math-utils.h"

#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include <math.h>

gdouble pt_modify_phi(gdouble s, gdouble error_max, gdouble error) {
  return -1.0 / (1.0 + exp(-s * (error - error_max))) + 1;
}

gdouble pt_modify_psi(gdouble s, gdouble dist_max, gdouble dist) {
  return 1.0 / (1.0 + exp(-s * (dist - dist_max)));
}

gdouble pt_modify_chi(gdouble s, gdouble error_max, gdouble dist_max,
                      gdouble error, gdouble dist) {
  gdouble phi = pt_modify_phi(s, error_max, error);
  gdouble psi = pt_modify_psi(s, dist_max, dist);
  return psi * phi + (1.0 - psi) * (1.0 - phi);
}

static inline gdouble phi_A(gdouble s, gdouble error_max, gdouble error) {
  return 1.0 - (2.0 / (1.0 + exp(-1.0 * s * (error - error_max))));
}

gdouble pt_modify_upsilon(gdouble s, gdouble error_max, gdouble dist_max,
                          gdouble error, gdouble dist) {
  return phi_A(s, error_max, error) * pt_modify_psi(s, dist_max, dist);
}

static inline gdouble phi_A_pos(double s, double error_max, double error) {
  return (2.0 / (1.0 + exp(-1.0 * s * (error - error_max)))) - 1.0;
}

gdouble pt_modify_tau(gdouble s, gdouble error_max, gdouble dist_max,
                      gdouble error, gdouble dist) {
  return phi_A_pos(s, error_max, error) *
         (1 - pt_modify_psi(s, dist_max, dist));
}

gdouble pt_modify_rho(gdouble s, gdouble error_max, gdouble dist_max,
                      gdouble error, gdouble dist) {
  gdouble psi = pt_modify_psi(s, dist_max, dist);
  return (phi_A(s, error_max, error) * psi) +
         (1 - pt_modify_phi(s, error_max, error)) * (1 - psi);
}

static slwr_status calculate_q(slwr_workspace *ws, const gsl_vector *c,
                               const gsl_matrix *D, const gsl_vector *xc,
                               gsl_vector *q) {
  if (gsl_vector_memcpy(q, xc) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_scale(q, 1 / gsl_blas_dnrm2(xc)) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *v = workspace_vector(ws, WORKSPACE_N_IN);
  if (!v) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_dgemv(CblasNoTrans, 1.0, D, q, 0, v) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  gdouble alpha;
  if (gsl_blas_ddot(q, v, &alpha) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_scale(q, sqrt(1 / alpha)) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_add(q, c) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

static slwr_status calculate_p_near(slwr_workspace *ws,
                                    const slwr_pt_modify_parameters *params,
                                    const gsl_vector *c, const gsl_matrix *D,
                                    const gsl_vector *x, const gsl_vector *q,
                                    gsl_vector *p_near) {
  gdouble w;
  if (determine_activation_gsl(ws, D, c, x, SLWR_KERNEL_GAUSSIAN, &w) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *xq = workspace_vector(ws, WORKSPACE_N_IN);
  if (!xq) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = utils_vector_difference(x, q, xq);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  if (gsl_vector_scale(xq, w * params->epsilon) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_memcpy(p_near, q) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_add(p_near, xq) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

static gdouble mahalanobis_distance_xc(slwr_workspace *ws, const gsl_matrix *D,
                                       const gsl_vector *xc) {
  double distance = GSL_NAN;
  gsl_vector *v = workspace_vector(ws, WORKSPACE_N_IN);
  if (!v) {
    goto end;
  }
  if (gsl_blas_dgemv(CblasNoTrans, 1.0, D, xc, 0, v) != GSL_SUCCESS) {
    goto end;
  }
  if (gsl_blas_ddot(xc, v, &distance) != GSL_SUCCESS) {
    goto end;
  }
end:
  return distance;
}

static inline gdouble
apply_weighting_function(const slwr_pt_modify_parameters *params,
                         slwr_pt_modify_weighting_function wf, gdouble error,
                         gdouble dist) {
  switch (wf) {
  case SLWR_PT_MODIFY_PHI:
    return pt_modify_phi(params->s, params->error_max, error);
  case SLWR_PT_MODIFY_PSI:
    return pt_modify_psi(params->s, params->dist_max, dist);
  case SLWR_PT_MODIFY_CHI:
    return pt_modify_chi(params->s, params->error_max, params->dist_max, error,
                         dist);
  case SLWR_PT_MODIFY_UPSILON:
    return pt_modify_upsilon(params->s, params->error_max, params->dist_max,
                             error, dist);
  case SLWR_PT_MODIFY_TAU:
    return pt_modify_tau(params->s, params->error_max, params->dist_max, error,
                         dist);
  case SLWR_PT_MODIFY_RHO:
    return pt_modify_rho(params->s, params->error_max, params->dist_max, error,
                         dist);
  default:
    return GSL_NAN;
  }
}

static slwr_status pt_modify(slwr_workspace *ws, const gsl_vector *c,
                             const gsl_vector *p, gsl_matrix *D) {
  gsl_matrix *D_decomp = workspace_matrix(ws, WORKSPACE_N_IN);
  if (!D_decomp) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_matrix_memcpy(D_decomp, D) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_linalg_cholesky_decomp1(D_decomp) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *pc = workspace_vector(ws, WORKSPACE_N_IN);
  if (!pc) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = utils_vector_difference(p, c, pc);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  if (gsl_blas_dtrmv(CblasLower, CblasTrans, CblasNonUnit, D_decomp, pc) !=
      GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  gdouble pc_norm = gsl_blas_dnrm2(pc);
  gdouble gamma = (1 / pc_norm - 1.0) / gsl_pow_2(pc_norm);
  gsl_matrix *G = workspace_matrix(ws, WORKSPACE_N_IN);
  if (!G) {
    return SLWR_STATUS_ERROR;
  }
  gsl_matrix_set_identity(G);
  if (gsl_blas_dger(gamma, pc, pc, G) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_dtrmm(CblasLeft, CblasLower, CblasNoTrans, CblasNonUnit, 1.0,
                     D_decomp, G) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, G, G, 0.0, D) !=
      GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

/**
 * Apply the pt-modify update rule to the given receptive field.
 *
 * @param ws The workspace.
 * @param params Parameters for the pt-modify update.
 * @param wf The error and distance weighting function.
 * @param rf The receptive field to update.
 * @param x The input vector.
 * @param error The error of the input vector.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating error otherwise.
 */
slwr_status pt_modify_update_slwr(slwr_workspace *ws,
                                  const slwr_pt_modify_parameters *params,
                                  slwr_pt_modify_weighting_function wf,
                                  slwr_receptive_field *rf,
                                  const slwr_vector *x, gdouble error) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf->c->size == x->size, SLWR_STATUS_DIMENSION_MISMATCH);

  gsl_vector *c =
      workspace_gsl_vector_from_slwr_vector(ws, rf->c, WORKSPACE_N_IN);
  if (!c) {
    return SLWR_STATUS_ERROR;
  }
  gsl_matrix *D =
      workspace_gsl_matrix_from_slwr_matrix(ws, rf->D, WORKSPACE_N_IN);
  if (!D) {
    return SLWR_STATUS_ERROR;
  }
  gsl_vector *gx = workspace_gsl_vector_from_slwr_vector(ws, x, WORKSPACE_N_IN);
  if (!gx) {
    return SLWR_STATUS_ERROR;
  }

  slwr_status status = pt_modify_update_gsl(ws, params, wf, c, D, gx, error);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  return slwr_matrix_from_gsl_matrix(rf->D, D);
}

slwr_status pt_modify_update_gsl(slwr_workspace *ws,
                                 const slwr_pt_modify_parameters *params,
                                 slwr_pt_modify_weighting_function wf,
                                 const gsl_vector *c, gsl_matrix *D,
                                 const gsl_vector *x, gdouble error) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(c != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(D != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x->size == c->size, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(x->size == D->size1, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(x->size == D->size2, SLWR_STATUS_DIMENSION_MISMATCH);

  gsl_vector *xc = workspace_vector(ws, WORKSPACE_N_IN);
  if (!xc) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = utils_vector_difference(x, c, xc);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  if (gsl_vector_isnull(xc)) {
    return SLWR_STATUS_OK;
  }

  gsl_vector *q = workspace_vector(ws, WORKSPACE_N_IN);
  if (!q) {
    return SLWR_STATUS_ERROR;
  }
  status = calculate_q(ws, c, D, xc, q);
  if (status != SLWR_STATUS_OK) {
    return status;
  }
  gsl_vector *p_near = workspace_vector(ws, WORKSPACE_N_IN);
  if (!p_near) {
    return SLWR_STATUS_ERROR;
  }
  status = calculate_p_near(ws, params, c, D, x, q, p_near);
  if (status != SLWR_STATUS_OK) {
    return status;
  }

  gdouble dist = mahalanobis_distance_xc(ws, D, xc);
  if (gsl_isnan(dist)) {
    return SLWR_STATUS_ERROR;
  }
  gdouble wf_output = apply_weighting_function(params, wf, error, dist);
  if (gsl_isnan(wf_output)) {
    return SLWR_STATUS_ERROR;
  }

  if (gsl_vector_sub(p_near, q) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_scale(p_near, wf_output) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }
  if (gsl_vector_add(q, p_near) != GSL_SUCCESS) {
    return SLWR_STATUS_ERROR;
  }

  return pt_modify(ws, c, q, D);
}

/**
 * Apply the pt-modify update rule to the given receptive field.
 *
 * Resets the workspace.
 *
 * @param ws The workspace.
 * @param params Parameters for the pt-modify update.
 * @param wf The error and distance weighting function.
 * @param rf The receptive field to update.
 * @param x The input vector.
 * @param error The error of the input vector.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating error otherwise.
 */
slwr_status slwr_pt_modify_update(slwr_workspace *ws,
                                  const slwr_pt_modify_parameters *params,
                                  slwr_pt_modify_weighting_function wf,
                                  slwr_receptive_field *rf,
                                  const slwr_vector *x, gdouble error) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(x != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(rf->c->size == x->size, SLWR_STATUS_DIMENSION_MISMATCH);

  slwr_workspace_reset(ws);
  slwr_status status = pt_modify_update_slwr(ws, params, wf, rf, x, error);
  slwr_workspace_reset(ws);
  return status;
}

void slwr_pt_modify_parameters_default_values(
    gsize n_in, slwr_pt_modify_parameters *params) {
  g_return_if_fail(params != NULL);

  params->n_in = n_in;
  params->epsilon = 0.150;
  params->s = 1000.0;
  params->error_max = 0.02;
  params->dist_max = 1.0;
}
