#pragma once

#include "../common/slwr-types.h"
#include "../receptive-field/slwr-receptive-field.h"

#include <glib.h>

typedef enum {
  SLWR_PT_MODIFY_PHI = 0,
  SLWR_PT_MODIFY_PSI,
  SLWR_PT_MODIFY_CHI,
  SLWR_PT_MODIFY_UPSILON,
  SLWR_PT_MODIFY_TAU,
  SLWR_PT_MODIFY_RHO
} slwr_pt_modify_weighting_function;

typedef struct {
  gsize n_in;
  gdouble epsilon;
  gdouble s;
  gdouble error_max;
  gdouble dist_max;
} slwr_pt_modify_parameters;

slwr_status slwr_pt_modify_update(slwr_workspace *ws,
                                  const slwr_pt_modify_parameters *params,
                                  slwr_pt_modify_weighting_function wf,
                                  slwr_receptive_field *rf,
                                  const slwr_vector *x, gdouble error);

void slwr_pt_modify_parameters_default_values(
    gsize n_in, slwr_pt_modify_parameters *params);
