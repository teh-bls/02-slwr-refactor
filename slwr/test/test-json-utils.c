#include "slwr/common/slwr-workspace.h"
#include "slwr/gng/slwr-gng.h"
#include "slwr/network/slwr-network.h"
#include "slwr/utils/slwr-json-utils.h"

#include <glib.h>
#include <jansson.h>

static void print_json_object(json_t *obj) {
  gchar *s = json_dumps(obj, 0);
  g_message("%s", s);
  g_free(s);
}

static void test_gng_parameters_malformed_object() {
  if (g_test_subprocess()) {
    slwr_gng_parameters *params = slwr_gng_parameters_from_json_string("");
    g_assert_nonnull(params);
    g_free(params);
  }
}

static void test_gng_parameters_empty_object() {
  if (g_test_subprocess()) {
    slwr_gng_parameters *params = slwr_gng_parameters_from_json_string("{}");
    g_assert_nonnull(params);

    slwr_gng_parameters default_params;
    slwr_gng_parameters_default_values(&default_params);

    g_assert_cmpuint(params->insertion_interval, ==,
                     default_params.insertion_interval);
    g_assert_cmpuint(params->max_age, ==, default_params.max_age);
    g_assert_cmpfloat(params->alpha_error, ==, default_params.alpha_error);
    g_assert_cmpfloat(params->epsilon_n, ==, default_params.epsilon_n);
    g_assert_cmpfloat(params->epsilon_b, ==, default_params.epsilon_b);
    g_assert_cmpfloat(params->d, ==, default_params.d);

    g_free(params);
  }
}

static void test_gng_parameters() {
  g_test_trap_subprocess("/json-utils/gng-parameters/malformed-object", 0, 0);
  g_test_trap_assert_failed();

  g_test_trap_subprocess("/json-utils/gng-parameters/empty-object", 0, 0);
  g_test_trap_assert_passed();
}

static void test_slwr_vector_to_json() {
  gdouble v_data[] = {1.2, 3.4, 5.68};
  slwr_vector v = {.size = G_N_ELEMENTS(v_data), .data = v_data};
  json_t *v_obj = slwr_vector_to_json(&v);
  g_assert_nonnull(v_obj);

  // print_json_object(v_obj);

  json_decref(v_obj);
}

static void test_slwr_matrix_to_json() {
  gdouble m_data[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  slwr_matrix m = {.size1 = 3, .size2 = 3, .data = m_data};
  json_t *m_obj = slwr_matrix_to_json(&m);
  g_assert_nonnull(m_obj);

  // print_json_object(m_obj);

  json_decref(m_obj);
}

static void test_slwr_receptive_field_to_json() {
  slwr_receptive_field *rf = slwr_receptive_field_alloc(4);
  g_assert_nonnull(rf);

  json_t *rf_obj = slwr_receptive_field_to_json(rf);
  g_assert_nonnull(rf);

  // print_json_object(rf_obj);

  json_decref(rf_obj);
  slwr_receptive_field_free(rf);
}

static void test_slwr_network_parameters_from_json_string() {
  const gsize n_in = 3;
  gdouble default_D_init_data[n_in];
  memset(default_D_init_data, 0, sizeof(gdouble));
  slwr_matrix default_D_init = {
      .size1 = n_in, .size2 = n_in, .data = default_D_init_data};
  slwr_network_parameters default_params = {
      .n_in = n_in, .D_init = &default_D_init, .max_models = 0};
  slwr_network_parameters_default_values(n_in, &default_params);

  {
    const gchar *param_string = "{\"n-in\" : 3, \"n-out\": 2}";
    slwr_network_parameters *params =
        slwr_network_parameters_from_json_string(param_string);
    g_assert_nonnull(params);
    g_assert_cmpuint(params->n_in, ==, 3);
    g_assert_cmpuint(params->n_out, ==, 2);
    g_assert_cmpuint(params->D_init->size1, ==, n_in);
    g_assert_cmpuint(params->D_init->size2, ==, n_in);
    for (gsize i = 0; i < n_in; ++i) {
      for (gsize j = 0; j < n_in; ++j) {
        gdouble entry = i == j ? 1.0 : 0.0;
        g_assert_cmpfloat(params->D_init->data[i * n_in + j], ==, entry);
      }
    }
    g_assert_cmpint(params->max_models, ==, default_params.max_models);
    slwr_network_parameters_free(params);
  }

  {
    const gchar *param_string =
        "{\"n-in\" : 3, \"n-out\": 2, \"D-init\" : [[1.0, 2.0, 3.0], [4.0, "
        "5.0, 6.0], [7, 8, 9]]}";
    slwr_network_parameters *params =
        slwr_network_parameters_from_json_string(param_string);
    g_assert_nonnull(params);
    g_assert_cmpuint(params->D_init->size1, ==, n_in);
    g_assert_cmpuint(params->D_init->size2, ==, n_in);
    for (gsize i = 0; i < n_in * n_in; ++i) {
      g_assert_cmpfloat(params->D_init->data[i], ==, i + 1);
    }
    g_assert_cmpint(params->max_models, ==, default_params.max_models);
    slwr_network_parameters_free(params);
  }
}

static void test_slwr_vector_from_json() {
  {
    gdouble real_data[] = {1.11, 2, 3.3, 4, 5.99};
    gint integer_data[] = {6, 7, 8};
    json_t *array = json_array();
    for (gsize i = 0; i < G_N_ELEMENTS(real_data); ++i) {
      json_t *inner_array = json_array();
      json_t *element = json_real(real_data[i]);
      json_array_append(inner_array, element);
      json_array_append(array, inner_array);
      json_decref(element);
      json_decref(inner_array);
    }
    for (gsize i = 0; i < G_N_ELEMENTS(integer_data); ++i) {
      json_t *inner_array = json_array();
      json_t *element = json_integer(integer_data[i]);
      json_array_append(inner_array, element);
      json_array_append(array, inner_array);
      json_decref(element);
      json_decref(inner_array);
    }

    slwr_vector *v = slwr_vector_from_json(array);
    g_assert_nonnull(v);
    g_assert_cmpint(G_N_ELEMENTS(real_data) + G_N_ELEMENTS(integer_data), ==,
                    v->size);
    gsize i = 0;
    for (int j = 0; j < G_N_ELEMENTS(real_data); ++j) {
      g_assert_cmpfloat(real_data[j], ==, v->data[i++]);
    }
    for (int j = 0; j < G_N_ELEMENTS(integer_data); ++j) {
      g_assert_cmpfloat((gdouble)integer_data[j], ==, v->data[i++]);
    }

    slwr_vector_free(v);
    json_decref(array);
  }
}

int main(int argc, char *argv[]) {
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/json-utils/gng-parameters", test_gng_parameters);
  g_test_add_func("/json-utils/gng-parameters/malformed-object",
                  test_gng_parameters_malformed_object);
  g_test_add_func("/json-utils/gng-parameters/empty-object",
                  test_gng_parameters_empty_object);
  g_test_add_func("/json-utils/slwr-vector-to-json", test_slwr_vector_to_json);
  g_test_add_func("/json-utils/slwr-matrix-to-json", test_slwr_matrix_to_json);
  g_test_add_func("/json-utils/slwr-receptive-field-to-json",
                  test_slwr_receptive_field_to_json);
  g_test_add_func("/json-utils/slwr-network-parameters-from-json-string",
                  test_slwr_network_parameters_from_json_string);
  g_test_add_func("/json-utils/slwr-vector-from-json",
                  test_slwr_vector_from_json);

  return g_test_run();
}
