#include "slwr/gng/slwr-gng.h"
#include "slwr/gng/src/gng.h"

#include <glib.h>

static void test_find_neighborhood() {
  const gsize n_in = 5;

  slwr_gng_context *ctx = slwr_gng_context_alloc();
  g_assert_nonnull(ctx);
  g_assert_nonnull(ctx->nodes);
  g_assert_nonnull(ctx->edges);

  slwr_gng_node *n1 = slwr_gng_node_alloc(n_in);
  g_assert_nonnull(n1);
  g_ptr_array_add(ctx->nodes, n1);
  slwr_gng_node *n2 = slwr_gng_node_alloc(n_in);
  g_assert_nonnull(n2);
  g_ptr_array_add(ctx->nodes, n2);
  slwr_gng_node *n3 = slwr_gng_node_alloc(n_in);
  g_assert_nonnull(n3);
  g_ptr_array_add(ctx->nodes, n3);

  /*   n1 --- n2
   *   |
   *   n3
   */
  slwr_gng_edge *e1 = slwr_gng_edge_alloc();
  g_assert_nonnull(e1);
  e1->a = n1;
  e1->b = n2;
  g_ptr_array_add(ctx->edges, e1);
  slwr_gng_edge *e2 = slwr_gng_edge_alloc();
  e2->a = n1;
  e2->b = n3;
  g_ptr_array_add(ctx->edges, e2);

  {
    slwr_status status =
        gng_find_neighborhood(n1, ctx->edges, ctx->neighborhood);
    g_assert_cmpuint(status, ==, SLWR_STATUS_OK);
    g_assert_nonnull(ctx->neighborhood);
    g_assert_cmpuint(ctx->neighborhood->len, ==, 2);
    /* TODO: this is bad and may fail at some point since
     * gng_find_neighborhood() does not guarantee node order
     */
    g_assert(n2 == (slwr_gng_node *)g_ptr_array_index(ctx->neighborhood, 0));
    g_assert(n3 == (slwr_gng_node *)g_ptr_array_index(ctx->neighborhood, 1));
  }

  {
    slwr_status status =
        gng_find_neighborhood(n2, ctx->edges, ctx->neighborhood);
    g_assert_cmpuint(status, ==, SLWR_STATUS_OK);
    g_assert_nonnull(ctx->neighborhood);
    g_assert_cmpuint(ctx->neighborhood->len, ==, 1);
    g_assert(n1 == (slwr_gng_node *)g_ptr_array_index(ctx->neighborhood, 0));
  }

  {
    slwr_status status =
        gng_find_neighborhood(n3, ctx->edges, ctx->neighborhood);
    g_assert_cmpuint(status, ==, SLWR_STATUS_OK);
    g_assert_nonnull(ctx->neighborhood);
    g_assert_cmpuint(ctx->neighborhood->len, ==, 1);
    g_assert(n1 == (slwr_gng_node *)g_ptr_array_index(ctx->neighborhood, 0));
  }

  slwr_gng_context_free(ctx);
}

static void test_edge_allocation() {
  slwr_gng_edge *e = slwr_gng_edge_alloc();
  g_assert_nonnull(e);
  slwr_gng_edge_free(e);
}

static void test_node_allocation() {
  slwr_gng_node *n = slwr_gng_node_alloc(3);
  g_assert_nonnull(n);
  slwr_gng_node_free(n);
}

static void test_context_allocation() {
  slwr_gng_context *ctx = slwr_gng_context_alloc();
  g_assert_nonnull(ctx);
  g_assert_nonnull(ctx->nodes);
  g_assert_nonnull(ctx->edges);

  slwr_gng_context_free(ctx);
}

int main(int argc, char *argv[]) {
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/gng/allocation/context", test_context_allocation);
  g_test_add_func("/gng/allocation/node", test_node_allocation);
  g_test_add_func("/gng/allocation/edge", test_edge_allocation);
  g_test_add_func("/gng/find_neighborhood", test_find_neighborhood);

  return g_test_run();
}