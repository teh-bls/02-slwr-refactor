#include "slwr/common/slwr-workspace.h"
#include "slwr/receptive-field/slwr-receptive-field.h"

#include <glib.h>

static void test_receptive_field_allocation() {
  slwr_receptive_field *rf = slwr_receptive_field_alloc(3);
  g_assert_nonnull(rf);
  g_assert_nonnull(rf->D);
  g_assert_nonnull(rf->c);

  slwr_receptive_field_free(rf);
}

int main(int argc, char *argv[]) {
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/receptive-field/allocation",
                  test_receptive_field_allocation);

  return g_test_run();
}