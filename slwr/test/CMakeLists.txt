project(slwr-tests VERSION 0.1)

macro(add_test_executable executable_name source_list)
    add_executable(${executable_name})
    target_include_directories(${executable_name} PUBLIC
            ${GLIB_INCLUDE_DIRS}
            ${GSL_INCLUDE_DIRS}
            ${JANSSON_INCLUDE_DIRS}
            $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}>)
    target_link_libraries(${executable_name} PUBLIC
            ${GLIB_LIBRARIES}
            ${GSL_LIBRARIES}
            ${JANSSON_LIBRARIES}
            02-slwr-refactor_static)
    target_sources(${executable_name} PRIVATE ${source_list})
endmacro()

add_test_executable(test-workspace "test-workspace.c")
add_test_executable(test-json-utils "test-json-utils.c")
add_test_executable(test-math-utils "test-math-utils.c")
add_test_executable(test-receptive-field "test-receptive-field.c")
add_test_executable(test-gng "test-gng.c")
