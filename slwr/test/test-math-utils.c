#include "slwr/common/src/workspace.h"
#include "slwr/utils/slwr-math-utils.h"
#include "slwr/utils/src/math-utils.h"

#include <glib.h>
#include <gsl/gsl_vector.h>

static void test_utils_euclidean_distance() {
  const gsize n_in = 2;
  slwr_workspace *ws = slwr_workspace_alloc(n_in, 0);

  gsl_vector *a = workspace_vector(ws, WORKSPACE_N_IN);
  gsl_vector_set_zero(a);
  gsl_vector *b = workspace_vector(ws, WORKSPACE_N_IN);
  gsl_vector_set(b, 0, 3.0);
  gsl_vector_set(b, 1, 4.0);

  gdouble distance;
  slwr_status status = utils_euclidean_distance(ws, a, b, &distance);
  g_assert_cmpuint(status, ==, SLWR_STATUS_OK);
  g_assert_cmpfloat(distance, ==, 5.0);

  slwr_workspace_free(ws);
}

int main(int argc, char *argv[]) {
  g_test_init(&argc, &argv, NULL);

  g_test_add_func("/math-utils/utils-euclidean-distance",
                  test_utils_euclidean_distance);

  return g_test_run();
}