#include "slwr/common/slwr-types.h"
#include "slwr/common/src/workspace.h"

#include <glib.h>

static void test_workspace_allocation() {
  {
    slwr_workspace *ws = slwr_workspace_alloc(10, 10);
    g_assert_nonnull(ws);
    slwr_workspace_free(ws);
  }

  {
    slwr_workspace *ws = slwr_workspace_alloc(0, 0);
    g_assert_nonnull(ws);
    slwr_workspace_free(ws);
  }
}

static void test_dimensionality() {
  const gsize n_in = 12;
  const gsize n_out = 8;
  slwr_workspace *ws = slwr_workspace_alloc(n_in, n_out);
  g_assert_nonnull(ws);

  {
    gsl_vector *v = workspace_vector(ws, WORKSPACE_N_IN);
    g_assert_nonnull(v);
    g_assert_cmpuint(v->size, ==, n_in);
  }

  {
    gsl_vector *v = workspace_vector(ws, WORKSPACE_N_OUT);
    g_assert_nonnull(v);
    g_assert_cmpuint(v->size, ==, n_out);
  }

  {
    gsl_matrix *m = workspace_matrix(ws, WORKSPACE_N_IN);
    g_assert_nonnull(m);
    g_assert_cmpuint(m->size1, ==, n_in);
    g_assert_cmpuint(m->size2, ==, n_in);
  }

  {
    gsl_matrix *m = workspace_matrix(ws, WORKSPACE_N_OUT);
    g_assert_nonnull(m);
    g_assert_cmpuint(m->size1, ==, n_in);
    g_assert_cmpuint(m->size2, ==, n_out);
  }

  slwr_workspace_free(ws);
}

static void test_capacity_and_reset() {
  slwr_workspace *ws = slwr_workspace_alloc(20, 25);

  {
    gsl_vector *v = NULL;
    for (gsize i = 0; i < workspace_get_max_vectors(); ++i) {
      v = workspace_vector(ws, WORKSPACE_N_IN);
      g_assert_nonnull(v);
    }
    v = workspace_vector(ws, WORKSPACE_N_IN);
    g_assert_null(v);

    slwr_workspace_reset(ws);

    v = NULL;
    for (gsize i = 0; i < workspace_get_max_vectors(); ++i) {
      v = workspace_vector(ws, WORKSPACE_N_IN);
      g_assert_nonnull(v);
    }
    v = workspace_vector(ws, WORKSPACE_N_IN);
    g_assert_null(v);
  }

  {
    gsl_vector *v = NULL;
    for (gsize i = 0; i < workspace_get_max_vectors(); ++i) {
      v = workspace_vector(ws, WORKSPACE_N_OUT);
      g_assert_nonnull(v);
    }
    v = workspace_vector(ws, WORKSPACE_N_OUT);
    g_assert_null(v);

    slwr_workspace_reset(ws);

    v = NULL;
    for (gsize i = 0; i < workspace_get_max_vectors(); ++i) {
      v = workspace_vector(ws, WORKSPACE_N_OUT);
      g_assert_nonnull(v);
    }
    v = workspace_vector(ws, WORKSPACE_N_OUT);
    g_assert_null(v);
  }

  {
    gsl_matrix *m = NULL;
    for (gsize i = 0; i < workspace_get_max_matrices(); ++i) {
      m = workspace_matrix(ws, WORKSPACE_N_IN);
      g_assert_nonnull(m);
    }
    m = workspace_matrix(ws, WORKSPACE_N_IN);
    g_assert_null(m);

    slwr_workspace_reset(ws);

    m = NULL;
    for (gsize i = 0; i < workspace_get_max_matrices(); ++i) {
      m = workspace_matrix(ws, WORKSPACE_N_IN);
      g_assert_nonnull(m);
    }
    m = workspace_matrix(ws, WORKSPACE_N_IN);
    g_assert_null(m);
  }

  {
    gsl_matrix *m = NULL;
    for (gsize i = 0; i < workspace_get_max_matrices(); ++i) {
      m = workspace_matrix(ws, WORKSPACE_N_OUT);
      g_assert_nonnull(m);
    }
    m = workspace_matrix(ws, WORKSPACE_N_OUT);
    g_assert_null(m);

    slwr_workspace_reset(ws);

    m = NULL;
    for (gsize i = 0; i < workspace_get_max_matrices(); ++i) {
      m = workspace_matrix(ws, WORKSPACE_N_OUT);
      g_assert_nonnull(m);
    }
    m = workspace_matrix(ws, WORKSPACE_N_OUT);
    g_assert_null(m);
  }

  slwr_workspace_free(ws);
}

static void test_slwr_to_gsl_valid_conversions() {
  if (g_test_subprocess()) {
    const gsize n_in = 3;
    const gsize n_out = 2;

    slwr_workspace *ws = slwr_workspace_alloc(n_in, n_out);
    {
      gdouble data[] = {3.4, 5.6, 7.8};
      slwr_vector from = {.size = n_in, .data = data};

      gsl_vector *to =
          workspace_gsl_vector_from_slwr_vector(ws, &from, WORKSPACE_N_IN);
      g_assert_nonnull(to);
      for (gsize i = 0; i < n_in; ++i) {
        g_assert_cmpfloat(gsl_vector_get(to, i), ==, from.data[i]);
      }
    }
    {
      gdouble data[] = {5.6, 13.14};
      slwr_vector from = {.size = n_out, .data = data};

      gsl_vector *to =
          workspace_gsl_vector_from_slwr_vector(ws, &from, WORKSPACE_N_OUT);
      g_assert_nonnull(to);
      for (gsize i = 0; i < n_out; ++i) {
        g_assert_cmpfloat(gsl_vector_get(to, i), ==, from.data[i]);
      }
    }
    {
      gdouble data[] = {3.4, 5.6, 7.8, 9.1};
      slwr_vector from = {.size = 4, .data = data};

      gsl_vector *to =
          workspace_gsl_vector_from_slwr_vector(ws, &from, WORKSPACE_N_IN);
      g_assert_null(to);
    }
    {
      gdouble data[] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};
      slwr_matrix from = {.size1 = n_in, .size2 = n_in, .data = data};

      gsl_matrix *to =
          workspace_gsl_matrix_from_slwr_matrix(ws, &from, WORKSPACE_N_IN);
      g_assert_nonnull(to);
      for (gsize i = 0; i < n_in * n_in; ++i) {
        g_assert_cmpfloat(to->data[i], ==, from.data[i]);
      }
    }
    {
      gdouble data[] = {3.3, 4.4, 5.5, 6.6, 7.7, -12.3};
      slwr_matrix from = {.size1 = n_in, .size2 = n_out, .data = data};

      gsl_matrix *to =
          workspace_gsl_matrix_from_slwr_matrix(ws, &from, WORKSPACE_N_OUT);
      g_assert_nonnull(to);
      for (gsize i = 0; i < n_in * n_out; ++i) {
        g_assert_cmpfloat(to->data[i], ==, from.data[i]);
      }
    }
    {
      gdouble data[] = {1.1, 2.2};
      slwr_matrix from = {.size1 = 1, .size2 = 1, .data = data};

      gsl_matrix *to =
          workspace_gsl_matrix_from_slwr_matrix(ws, &from, WORKSPACE_N_IN);
      g_assert_null(to);
    }
    slwr_workspace_free(ws);
  }
}

static void test_gsl_to_slwr_valid_conversion() {
  if (g_test_subprocess()) {
    gdouble from_data[] = {1.1, 2.2, 4.4};
    gsl_vector *from = gsl_vector_alloc(G_N_ELEMENTS(from_data));
    memcpy(from->data, from_data, G_N_ELEMENTS(from_data) * sizeof(gdouble));

    gdouble to_data[] = {0.0, 0.0, 0.0};
    slwr_vector to = {.size = G_N_ELEMENTS(to_data), .data = to_data};

    slwr_status status = slwr_vector_from_gsl_vector(&to, from);
    g_assert_cmpuint(status, ==, SLWR_STATUS_OK);
    for (gsize i = 0; i < G_N_ELEMENTS(to_data); ++i) {
      g_assert_cmpfloat(to_data[i], ==, from_data[i]);
    }
    gsl_vector_free(from);
  }
}

static void test_gsl_to_slwr_invalid_conversion() {
  if (g_test_subprocess()) {
    gdouble from_data[] = {1.1, 2.2, 4.4, 5.5, 9.9};
    gsl_vector *from = gsl_vector_alloc(G_N_ELEMENTS(from_data));
    memcpy(from->data, from_data, 5 * sizeof(gdouble));

    gdouble to_data[] = {0.0, 0.0, 0.0};
    slwr_vector to = {.size = G_N_ELEMENTS(to_data), .data = to_data};

    slwr_status status = slwr_vector_from_gsl_vector(&to, from);
    g_assert_cmpuint(status, ==, SLWR_STATUS_DIMENSION_MISMATCH);
    gsl_vector_free(from);
  }
}

static void test_gsl_to_slwr() {
  g_test_trap_subprocess("/workspace/gsl-to-slwr/valid-conversion", 0, 0);
  g_test_trap_assert_passed();

  g_test_trap_subprocess("/workspace/gsl-to-slwr/invalid-conversion", 0, 0);
  g_test_trap_assert_failed();
}

static void test_slwr_vector_allocation() {
  gsize size = 123;
  slwr_vector *v = slwr_vector_alloc(size);
  g_assert_nonnull(v);
  g_assert_cmpuint(v->size, ==, size);
  g_assert_nonnull(v->data);
  slwr_vector_free(v);
}

static void test_slwr_matrix_allocation() {
  gsize size1 = 23;
  gsize size2 = 5;
  slwr_matrix *m = slwr_matrix_alloc(size1, size2);
  g_assert_nonnull(m);
  g_assert_cmpuint(m->size1, ==, size1);
  g_assert_cmpuint(m->size2, ==, size2);
  g_assert_nonnull(m->data);
  slwr_matrix_free(m);
}

static void test_slwr_vector_copy_valid() {
  if (g_test_subprocess()) {
    gdouble from_data[] = {12.0, 13.1, -3.1415, 0.0};
    slwr_vector from = {.size = G_N_ELEMENTS(from_data), .data = from_data};

    gdouble to_data[G_N_ELEMENTS(from_data)];
    slwr_vector to = {.size = G_N_ELEMENTS(to_data), .data = to_data};

    slwr_status status = slwr_vector_copy(&to, &from);
    g_assert_cmpuint(status, ==, SLWR_STATUS_OK);
    for (gsize i = 0; i < to.size; ++i) {
      g_assert_cmpfloat(from.data[i], ==, to.data[i]);
    }
  }
}

static void test_slwr_vector_copy_invalid() {
  if (g_test_subprocess()) {
    gdouble from_data[] = {12.0, 13.1, -3.1415, 0.0};
    slwr_vector from = {.size = G_N_ELEMENTS(from_data), .data = from_data};

    gdouble to_data[G_N_ELEMENTS(from_data) + 3];
    slwr_vector to = {.size = G_N_ELEMENTS(to_data), .data = to_data};

    slwr_status status = slwr_vector_copy(&to, &from);
    g_assert_cmpuint(status, ==, SLWR_STATUS_DIMENSION_MISMATCH);
  }
}

static void test_slwr_vector_copy() {
  g_test_trap_subprocess("/workspace/slwr-vector-copy/valid", 0, 0);
  g_test_trap_assert_passed();

  g_test_trap_subprocess("/workspace/slwr-vector-copy/invalid", 0, 0);
  g_test_trap_assert_failed();
}

static void test_slwr_vector_to_str() {
  gdouble v_data[] = {1.0, 2.0, 3.0, 4.0, 9.0};
  slwr_vector v = {.size = G_N_ELEMENTS(v_data), .data = v_data};
  gchar *v_str = slwr_vector_to_str(&v);
  g_assert_nonnull(v_str);
  // g_message("%s", v_str);
  g_free(v_str);
}

static void test_slwr_matrix_to_str() {
  gdouble m_data[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  slwr_matrix m = {.size1 = 3, .size2 = 3, .data = m_data};
  gchar *m_str = slwr_matrix_to_str(&m);
  g_assert_nonnull(m_str);
  g_message("%s", m_str);
  g_free(m_str);
}

static void test_slwr_vector_from_str() {
  {
    slwr_vector *v = slwr_vector_from_str("[1, 2, 3.1415]");
    g_assert_nonnull(v);
    g_assert_cmpint(v->size, ==, 3);
    g_assert_cmpfloat(v->data[0], ==, 1.0);
    g_assert_cmpfloat(v->data[1], ==, 2.0);
    g_assert_cmpfloat(v->data[2], ==, 3.1415);
    slwr_vector_free(v);
  }
  {
    slwr_vector *v = slwr_vector_from_str("[1,     23.1415]");
    g_assert_nonnull(v);
    g_assert_cmpint(v->size, ==, 2);
    g_assert_cmpfloat(v->data[0], ==, 1.0);
    g_assert_cmpfloat(v->data[1], ==, 23.1415);
    slwr_vector_free(v);
  }
  {
    slwr_vector *v = slwr_vector_from_str("1, 2, 3.1415]");
    g_assert_null(v);
  }
  {
    slwr_vector *v = slwr_vector_from_str("[]");
    g_assert_null(v);
  }
  {
    slwr_vector *v = slwr_vector_from_str("[1, 2], 3.1415]");
    g_assert_null(v);
  }
  {
    slwr_vector *v = slwr_vector_from_str("[abc, 3.1415]");
    g_assert_null(v);
  }
}

static void test_slwr_matrix_set_identity() {
  gdouble m_data[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  slwr_matrix m = {.size1 = 3, .size2 = 3, .data = m_data};

  slwr_status status = slwr_matrix_set_identity(&m);
  g_assert_cmpint(status, ==, SLWR_STATUS_OK);
  for (gsize i = 0; i < 3; ++i) {
    for (gsize j = 0; j < 3; ++j) {
      if (i == j) {
        g_assert_cmpfloat(m.data[0], ==, 1.0);
      } else {
        g_assert_cmpfloat(m.data[i * m.size1 + j], ==, 0.0);
      }
    }
  }
}

static void test_slwr_zero_matrices() {
  gdouble m_data[] = {0.0, 0.0, 0.0, 0.0};
  slwr_matrix m = {.size1 = 2, .size2 = 2, .data = m_data};
  g_assert_true(slwr_matrix_is_zero(&m));
  m_data[0] = DBL_MIN;
  g_assert_false(slwr_matrix_is_zero(&m));

  slwr_status status = slwr_matrix_set_zero(&m);
  g_assert_cmpint(status, ==, SLWR_STATUS_OK);
  g_assert_true(slwr_matrix_is_zero(&m));
}

static void test_slwr_vector_add() {
  {
    gdouble data1[] = {1.0, 11.0, -22.0, 34.90};
    gdouble data2[] = {12.3, 4.567, 8.91, 23.4567};

    gdouble v1_data[4];
    memcpy(v1_data, data1, 4 * sizeof(gdouble));
    gdouble v2_data[4];
    memcpy(v2_data, data2, 4 * sizeof(gdouble));

    slwr_vector v1 = {.size = 4, .data = v1_data};
    slwr_vector v2 = {.size = 4, .data = v2_data};

    slwr_status status = slwr_vector_add(&v1, &v2);
    g_assert_true(status == SLWR_STATUS_OK);
    for (gsize i = 0; i < v1.size; ++i) {
      g_assert_cmpfloat(v1.data[i], ==, data1[i] + data2[i]);
    }
  }
}

static void test_slwr_vector_scale() {
  {
    gdouble data[] = {1.0, 11.0, -22.0, 34.90};
    gdouble s = 123.4;

    gdouble v_data[4];
    memcpy(v_data, data, 4 * sizeof(gdouble));

    slwr_vector v = {.size = 4, .data = v_data};

    slwr_status status = slwr_vector_scale(&v, s);
    g_assert_true(status == SLWR_STATUS_OK);
    for (gsize i = 0; i < v.size; ++i) {
      g_assert_cmpfloat(v.data[i], ==, data[i] * s);
    }
  }
}

int main(int argc, char *argv[]) {
  g_test_init(&argc, &argv, NULL);
  g_test_add_func("/workspace/workspace-allocation", test_workspace_allocation);
  g_test_add_func("/workspace/dimensionality", test_dimensionality);
  g_test_add_func("/workspace/capacity-and-reset", test_capacity_and_reset);
  g_test_add_func("/workspace/slwr-to-gsl/valid-conversions",
                  test_slwr_to_gsl_valid_conversions);
  g_test_add_func("/workspace/gsl-to-slwr", test_gsl_to_slwr);
  g_test_add_func("/workspace/gsl-to-slwr/valid-conversion",
                  test_gsl_to_slwr_valid_conversion);
  g_test_add_func("/workspace/gsl-to-slwr/invalid-conversion",
                  test_gsl_to_slwr_invalid_conversion);
  g_test_add_func("/workspace/slwr-vector-allocation",
                  test_slwr_vector_allocation);
  g_test_add_func("/workspace/slwr-matrix-allocation",
                  test_slwr_matrix_allocation);
  g_test_add_func("/workspace/slwr-vector-copy", test_slwr_vector_copy);
  g_test_add_func("/workspace/slwr-vector-copy/valid",
                  test_slwr_vector_copy_valid);
  g_test_add_func("/workspace/slwr-vector-copy/invalid",
                  test_slwr_vector_copy_invalid);
  g_test_add_func("/workspace/slwr-vector-to-str", test_slwr_vector_to_str);
  g_test_add_func("/workspace/slwr-matrix-to-str", test_slwr_matrix_to_str);
  g_test_add_func("/workspace/slwr-vector-from-str", test_slwr_vector_from_str);
  g_test_add_func("/workspace/slwr-matrix-set-identity",
                  test_slwr_matrix_set_identity);
  g_test_add_func("/workspace/slwr-zero-matrices", test_slwr_zero_matrices);
  g_test_add_func("/workspace/slwr-vector-add", test_slwr_vector_add);
  g_test_add_func("/workspace/slwr-vector-scale", test_slwr_vector_scale);

  return g_test_run();
}
