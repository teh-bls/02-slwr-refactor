#pragma once

#include "slwr-types.h"

#include <glib.h>

#include <stdio.h>

slwr_workspace *slwr_workspace_alloc(gsize n_in, gsize n_out);
void slwr_workspace_free(slwr_workspace *ws);

void slwr_workspace_reset(slwr_workspace *ws);

slwr_matrix *slwr_matrix_alloc(gsize size1, gsize size2);
void slwr_matrix_free(slwr_matrix *m);

slwr_vector *slwr_vector_alloc(gsize size);
void slwr_vector_free(slwr_vector *v);

slwr_status slwr_vector_copy(slwr_vector *to, const slwr_vector *from);
slwr_status slwr_matrix_copy(slwr_matrix *to, const slwr_matrix *from);

slwr_status slwr_vector_add(slwr_vector *a, const slwr_vector *b);
slwr_status slwr_vector_sub(slwr_vector *a, const slwr_vector *b);
slwr_status slwr_vector_scale(slwr_vector *a, gdouble s);
slwr_status slwr_vector_add_constant(slwr_vector *a, gdouble c);
slwr_status slwr_vector_mul(slwr_vector *a, const slwr_vector *b);

gchar *slwr_matrix_to_str(const slwr_matrix *m);
gchar *slwr_vector_to_str(const slwr_vector *v);

slwr_vector* slwr_vector_from_str(const gchar *s);

void slwr_matrix_printf(FILE *file, const slwr_matrix *m);
void slwr_vector_printf(FILE *file, const slwr_vector *v);

slwr_status slwr_matrix_sub(slwr_matrix *a, const slwr_matrix *b);

slwr_status slwr_matrix_set_identity(slwr_matrix *m);
slwr_status slwr_matrix_set_zero(slwr_matrix *m);
slwr_status slwr_vector_set_zero(slwr_vector *v);
slwr_status slwr_vector_set_all(slwr_vector *v, gdouble x);

gboolean slwr_matrix_is_zero(const slwr_matrix *m);

void slwr_incremental_weighted_mean_update(
    slwr_incremental_weighted_mean *incremental_mean, gdouble mean, gdouble w);
void slwr_incremental_weighted_mean_reset(
    slwr_incremental_weighted_mean *incremental_mean);
