#pragma once

#include "slwr/common/slwr-workspace.h"

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

typedef enum { WORKSPACE_N_IN, WORKSPACE_N_OUT } workspace_dimensionality;

gsize workspace_get_max_vectors();
gsize workspace_get_max_matrices();

gsl_vector *workspace_vector(slwr_workspace *ws,
                             workspace_dimensionality num_rows);
gsl_matrix *workspace_matrix(slwr_workspace *ws,
                             workspace_dimensionality num_cols);

gsl_vector *
workspace_gsl_vector_from_slwr_vector(slwr_workspace *ws,
                                      const slwr_vector *vector,
                                      workspace_dimensionality num_rows);
gsl_matrix *
workspace_gsl_matrix_from_slwr_matrix(slwr_workspace *ws,
                                      const slwr_matrix *matrix,
                                      workspace_dimensionality num_cols);

slwr_status slwr_vector_from_gsl_vector(slwr_vector *to,
                                        const gsl_vector *from);

slwr_status slwr_matrix_from_gsl_matrix(slwr_matrix *to,
                                        const gsl_matrix *from);
