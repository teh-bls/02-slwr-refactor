#include "workspace.h"

#include <gsl/gsl_block.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include <glib/gprintf.h>

#include <errno.h>
#include <math.h>

static const gsize max_vectors = 5000;
static const gsize max_matrices = 5000;

struct slwr_workspace {
  gsl_vector **n_in_vectors;
  gsl_vector **n_out_vectors;
  gsl_matrix **n_in_n_in_matrices;
  gsl_matrix **n_in_n_out_matrices;
  gsize n_in_vectors_offset;
  gsize n_out_vectors_offset;
  gsize n_in_n_in_matrices_offset;
  gsize n_in_n_out_matrices_offset;
};

slwr_workspace *slwr_workspace_alloc(gsize n_in, gsize n_out) {
  slwr_workspace *ws = (slwr_workspace *)g_malloc0(sizeof(slwr_workspace));
  if (!ws) {
    goto on_error;
  }

  ws->n_in_vectors =
      (gsl_vector **)g_malloc0(sizeof(gsl_vector *) * max_vectors);
  if (!ws->n_in_vectors) {
    goto on_error;
  }
  ws->n_out_vectors =
      (gsl_vector **)g_malloc0(sizeof(gsl_vector *) * max_vectors);
  if (!ws->n_out_vectors) {
    goto on_error;
  }
  ws->n_in_n_in_matrices =
      (gsl_matrix **)g_malloc0(sizeof(gsl_matrix *) * max_matrices);
  if (!ws->n_in_n_in_matrices) {
    goto on_error;
  }
  ws->n_in_n_out_matrices =
      (gsl_matrix **)g_malloc0(sizeof(gsl_matrix *) * max_matrices);
  if (!ws->n_in_n_out_matrices) {
    goto on_error;
  }

  for (gsize i = 0; i < max_vectors; ++i) {
    gsl_vector **v = ws->n_in_vectors + i;
    *v = gsl_vector_calloc(n_in);
    if (!*v) {
      goto on_error;
    }
    v = ws->n_out_vectors + i;
    *v = gsl_vector_calloc(n_out);
    if (!*v) {
      goto on_error;
    }
  }
  for (gsize i = 0; i < max_matrices; ++i) {
    gsl_matrix **m = ws->n_in_n_in_matrices + i;
    *m = gsl_matrix_calloc(n_in, n_in);
    if (!*m) {
      goto on_error;
    }
    m = ws->n_in_n_out_matrices + i;
    *m = gsl_matrix_calloc(n_in, n_out);
    if (!*m) {
      goto on_error;
    }
  }

  ws->n_in_vectors_offset = 0;
  ws->n_out_vectors_offset = 0;
  ws->n_in_n_in_matrices_offset = 0;
  ws->n_in_n_out_matrices_offset = 0;

  return ws;

on_error:
  slwr_workspace_free(ws);
  return NULL;
}

void slwr_workspace_free(slwr_workspace *ws) {
  g_return_if_fail(ws != NULL);

  if (ws->n_in_n_out_matrices) {
    for (gsize i = 0; i < max_matrices; ++i) {
      gsl_matrix *m = *(ws->n_in_n_out_matrices + i);
      if (m) {
        gsl_matrix_free(m);
      }
    }
    g_free(ws->n_in_n_out_matrices);
  }

  if (ws->n_in_n_in_matrices) {
    for (gsize i = 0; i < max_matrices; ++i) {
      gsl_matrix *m = *(ws->n_in_n_in_matrices + i);
      if (m) {
        gsl_matrix_free(m);
      }
    }
    g_free(ws->n_in_n_in_matrices);
  }

  if (ws->n_out_vectors) {
    for (gsize i = 0; i < max_vectors; ++i) {
      gsl_vector *v = *(ws->n_out_vectors + i);
      if (v) {
        gsl_vector_free(v);
      }
    }
    g_free(ws->n_out_vectors);
  }

  if (ws->n_in_vectors) {
    for (gsize i = 0; i < max_vectors; ++i) {
      gsl_vector *v = *(ws->n_in_vectors + i);
      if (v) {
        gsl_vector_free(v);
      }
    }
    g_free(ws->n_in_vectors);
  }

  g_free(ws);
}

void slwr_workspace_reset(slwr_workspace *ws) {
  g_return_if_fail(ws != NULL);

  ws->n_in_vectors_offset = 0;
  ws->n_out_vectors_offset = 0;
  ws->n_in_n_in_matrices_offset = 0;
  ws->n_in_n_out_matrices_offset = 0;
}

gsize workspace_get_max_vectors() { return max_vectors; }

gsize workspace_get_max_matrices() { return max_matrices; }

static gsl_vector *get_vector(gsl_vector *vectors[], gsize *offset) {
  gsl_vector *v = NULL;
  if (*offset < max_vectors) {
    v = *(vectors + *offset);
    ++*offset;
  }
  return v;
}

gsl_vector *workspace_vector(slwr_workspace *ws,
                             workspace_dimensionality num_rows) {
  g_return_val_if_fail(ws != NULL, NULL);

  if (num_rows == WORKSPACE_N_IN) {
    return get_vector(ws->n_in_vectors, &ws->n_in_vectors_offset);
  } else if (num_rows == WORKSPACE_N_OUT) {
    return get_vector(ws->n_out_vectors, &ws->n_out_vectors_offset);
  }

  return NULL;
}

static gsl_matrix *get_matrix(gsl_matrix *matrices[], gsize *offset) {
  gsl_matrix *m = NULL;
  if (*offset < max_matrices) {
    m = *(matrices + *offset);
    ++*offset;
  }
  return m;
}

gsl_matrix *workspace_matrix(slwr_workspace *ws,
                             workspace_dimensionality num_cols) {
  g_return_val_if_fail(ws != NULL, NULL);

  if (num_cols == WORKSPACE_N_IN) {
    return get_matrix(ws->n_in_n_in_matrices, &ws->n_in_n_in_matrices_offset);
  } else if (num_cols == WORKSPACE_N_OUT) {
    return get_matrix(ws->n_in_n_out_matrices, &ws->n_in_n_out_matrices_offset);
  }

  return NULL;
}

gsl_vector *
workspace_gsl_vector_from_slwr_vector(slwr_workspace *ws,
                                      const slwr_vector *vector,
                                      workspace_dimensionality num_rows) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(vector != NULL, NULL);

  gsl_vector *v = workspace_vector(ws, num_rows);
  if (!v) {
    return NULL;
  }
  if (v->size != vector->size) {
    return NULL;
  }
  memcpy(v->data, vector->data, vector->size * sizeof(gdouble));
  return v;
}

gsl_matrix *
workspace_gsl_matrix_from_slwr_matrix(slwr_workspace *ws,
                                      const slwr_matrix *matrix,
                                      workspace_dimensionality num_cols) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(matrix != NULL, NULL);

  gsl_matrix *m = workspace_matrix(ws, num_cols);
  if (!m) {
    return NULL;
  }
  if (m->size1 != matrix->size1 || m->size2 != matrix->size2) {
    return NULL;
  }
  /* TODO: what about the stride? */
  memcpy(m->data, matrix->data,
         matrix->size1 * matrix->size2 * sizeof(gdouble));
  return m;
}

slwr_status slwr_vector_from_gsl_vector(slwr_vector *to,
                                        const gsl_vector *from) {
  g_return_val_if_fail(to != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(from != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(to->size == from->size, SLWR_STATUS_DIMENSION_MISMATCH);

  memcpy(to->data, from->data, from->size * sizeof(gdouble));
  return SLWR_STATUS_OK;
}

slwr_status slwr_matrix_from_gsl_matrix(slwr_matrix *to,
                                        const gsl_matrix *from) {
  g_return_val_if_fail(to != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(from != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(to->size1 == from->size1,
                       SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(to->size2 == from->size2,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  memcpy(to->data, from->data, from->size1 * from->size2 * sizeof(gdouble));
  return SLWR_STATUS_OK;
}

slwr_matrix *slwr_matrix_alloc(gsize size1, gsize size2) {
  slwr_matrix *m = (slwr_matrix *)g_malloc0(sizeof(slwr_matrix));
  if (!m) {
    goto on_error;
  }
  m->size1 = size1;
  m->size2 = size2;
  m->data = (gdouble *)g_malloc0(size1 * size2 * sizeof(gdouble));
  if (!m->data) {
    goto on_error;
  }
  return m;

on_error:
  if (m) {
    if (m->data) {
      g_free(m->data);
    }
    g_free(m);
  }
  return NULL;
}

void slwr_matrix_free(slwr_matrix *m) {
  if (!m) {
    return;
  }
  g_free(m->data);
  g_free(m);
}

slwr_vector *slwr_vector_alloc(gsize size) {
  slwr_vector *v = (slwr_vector *)g_malloc0(sizeof(slwr_vector));
  if (!v) {
    goto on_error;
  }
  v->size = size;
  v->data = (gdouble *)g_malloc0(size * sizeof(gdouble));
  if (!v->data) {
    goto on_error;
  }
  return v;

on_error:
  if (v) {
    if (v->data) {
      g_free(v->data);
    }
    g_free(v);
  }
  return NULL;
}

void slwr_vector_free(slwr_vector *v) {
  if (!v) {
    return;
  }

  g_free(v->data);
  g_free(v);
}

slwr_status slwr_vector_copy(slwr_vector *to, const slwr_vector *from) {
  g_return_val_if_fail(to != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(from != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(to->size == from->size, SLWR_STATUS_DIMENSION_MISMATCH);

  memcpy(to->data, from->data, to->size * sizeof(gdouble));
  return SLWR_STATUS_OK;
}

slwr_status slwr_matrix_copy(slwr_matrix *to, const slwr_matrix *from) {
  g_return_val_if_fail(to != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(from != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(to->size1 == from->size1,
                       SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(to->size2 == from->size2,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  memcpy(to->data, from->data, to->size1 * to->size2 * sizeof(gdouble));
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_add(slwr_vector *a, const slwr_vector *b) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size == b->size, SLWR_STATUS_DIMENSION_MISMATCH);

  for (gsize i = 0; i < a->size; ++i) {
    a->data[i] += b->data[i];
  }
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_sub(slwr_vector *a, const slwr_vector *b) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size == b->size, SLWR_STATUS_DIMENSION_MISMATCH);

  for (gsize i = 0; i < a->size; ++i) {
    a->data[i] -= b->data[i];
  }
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_scale(slwr_vector *a, gdouble s) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->data != NULL, SLWR_STATUS_ERROR);

  for (gsize i = 0; i < a->size; ++i) {
    a->data[i] *= s;
  }
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_add_constant(slwr_vector *a, gdouble c) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  for (gsize i = 0; i < a->size; ++i) {
    a->data[i] += c;
  }
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_mul(slwr_vector *a, const slwr_vector *b) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size == b->size, SLWR_STATUS_DIMENSION_MISMATCH);
  for (gsize i = 0; i < a->size; ++i) {
    a->data[i] *= b->data[i];
  }
  return SLWR_STATUS_OK;
}

static void append_double_to_g_string(GString *str, double entry,
                                      const gchar *prefix,
                                      const gchar *suffix) {

  g_string_append(str, prefix);
  gchar *entry_str = g_strdup_printf("%f", entry);
  g_string_append(str, entry_str);
  g_string_append(str, suffix);
  g_free(entry_str);
}

gchar *slwr_matrix_to_str(const slwr_matrix *m) {
  g_return_val_if_fail(m != NULL, NULL);
  g_return_val_if_fail(m->data != NULL, NULL);

  GString *str = g_string_new(NULL);
  g_string_append(str, "[");
  if (m->size2 > 0) {
    append_double_to_g_string(str, m->data[0], "[", "");
    for (gsize i = 1; i < m->size2; ++i) {
      append_double_to_g_string(str, m->data[i], ", ", "");
    }
    g_string_append(str, "]");
  }
  for (gsize i = 1; i < m->size1; ++i) {
    append_double_to_g_string(str, m->data[i * m->size2], ", [", "");
    for (gsize j = 1; j < m->size2; ++j) {
      append_double_to_g_string(str, m->data[i * m->size2 + j], ", ", "");
    }
    g_string_append(str, "]");
  }
  g_string_append(str, "]");
  return g_string_free(str, FALSE);
}

gchar *slwr_vector_to_str(const slwr_vector *v) {
  g_return_val_if_fail(v != NULL, NULL);
  g_return_val_if_fail(v->data != NULL, NULL);

  GString *str = g_string_new(NULL);
  g_string_append(str, "[");
  if (v->size > 0) {
    append_double_to_g_string(str, v->data[0], "[", "]");
    for (gsize i = 1; i < v->size; ++i) {
      append_double_to_g_string(str, v->data[i], ", [", "]");
    }
  }
  g_string_append(str, "]");
  return g_string_free(str, FALSE);
}

slwr_vector *slwr_vector_from_str(const gchar *s) {
  g_return_val_if_fail(s != NULL, NULL);

  gchar *s_dup = NULL;
  gchar **elements = NULL;
  slwr_vector *v = NULL;
  gsize len = strlen(s);

  if (s[0] != '[' || s[len - 1] != ']') {
    goto on_error;
  }

  s_dup = g_strdup(s);
  s_dup[len - 1] = '\0';
  elements = g_strsplit(s_dup + 1, ",", 0);
  gsize dim;
  for (dim = 0; elements[dim]; ++dim)
    ;
  v = slwr_vector_alloc(dim);
  if (!v) {
    goto on_error;
  }
  for (gsize i = 0; i < dim; ++i) {
    for (gsize j = 0; j < strlen(elements[i]); ++j) {
      if (!g_unichar_isspace(elements[i][j]) &&
          !g_unichar_isdigit(elements[i][j]) && elements[i][j] != '.') {
        goto on_error;
      }
    }
    v->data[i] = strtod(elements[i], NULL);
    if (errno == ERANGE) {
      goto on_error;
    }
  }
  printf("\n");

  g_strfreev(elements);
  g_free(s_dup);
  return v;

on_error:
  if (elements) {
    g_strfreev(elements);
  }
  if (s_dup) {
    g_free(s_dup);
  }
  if (v) {
    slwr_vector_free(v);
  }
  return NULL;
}

void slwr_vector_printf(FILE *file, const slwr_vector *v) {
  g_return_if_fail(file != NULL);
  g_return_if_fail(v != NULL);
  g_return_if_fail(v->data != NULL);

  gchar *v_str = slwr_vector_to_str(v);
  g_fprintf(file, "%s\n", v_str);
  g_free(v_str);
}

void slwr_matrix_printf(FILE *file, const slwr_matrix *m) {
  g_return_if_fail(file != NULL);
  g_return_if_fail(m != NULL);
  g_return_if_fail(m->data != NULL);

  gchar *m_str = slwr_matrix_to_str(m);
  g_fprintf(file, "%s\n", m_str);
  g_free(m_str);
}

slwr_status slwr_matrix_sub(slwr_matrix *a, const slwr_matrix *b) {
  g_return_val_if_fail(a != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(b->data != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(a->size1 == b->size1, SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(a->size2 == b->size2, SLWR_STATUS_DIMENSION_MISMATCH);

  for (gsize i = 0; i < a->size1 * a->size2; ++i) {
    a->data[i] -= b->data[i];
  }
  return SLWR_STATUS_OK;
}

slwr_status slwr_matrix_set_identity(slwr_matrix *m) {
  g_return_val_if_fail(m != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(m->size1 == m->size2, SLWR_STATUS_DIMENSION_MISMATCH);

  memset(m->data, 0, sizeof(gdouble) * m->size1 * m->size2);
  for (gsize i = 0; i < m->size1; ++i) {
    m->data[i * m->size1 + i] = 1.0;
  }

  return SLWR_STATUS_OK;
}

slwr_status slwr_matrix_set_zero(slwr_matrix *m) {
  g_return_val_if_fail(m != NULL, SLWR_STATUS_ERROR);
  memset(m->data, 0, sizeof(gdouble) * m->size1 * m->size2);
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_set_zero(slwr_vector *v) {
  g_return_val_if_fail(v != NULL, SLWR_STATUS_ERROR);
  memset(v->data, 0, sizeof(gdouble) * v->size);
  return SLWR_STATUS_OK;
}

slwr_status slwr_vector_set_all(slwr_vector *v, gdouble x) {
  g_return_val_if_fail(v != NULL, SLWR_STATUS_ERROR);
  for (gsize i = 0; i < v->size; ++i) {
    v->data[i] = x;
  }
  return SLWR_STATUS_OK;
}

gboolean slwr_matrix_is_zero(const slwr_matrix *m) {
  g_return_val_if_fail(m != NULL, FALSE);
  for (gsize i = 0; i < m->size1 * m->size2; ++i) {
    if (fpclassify(m->data[i]) != FP_ZERO) {
      return FALSE;
    }
  }
  return TRUE;
}

void slwr_incremental_weighted_mean_update(
    slwr_incremental_weighted_mean *incremental_mean, gdouble mean, gdouble w) {
  g_return_if_fail(incremental_mean != NULL);

  gdouble *W = &incremental_mean->sum_of_weights;
  gdouble *m = &incremental_mean->mean;

  *W += w;
  *m += (w / *W) * (mean - *m);
}

void slwr_incremental_weighted_mean_reset(
    slwr_incremental_weighted_mean *incremental_mean) {
  g_return_if_fail(incremental_mean != NULL);

  incremental_mean->sum_of_weights = 0.00001;
  incremental_mean->mean = 0.0;
}
