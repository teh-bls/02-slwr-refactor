#pragma once

#include <glib.h>

#include <stddef.h>

typedef struct slwr_workspace slwr_workspace;

typedef enum {
  SLWR_KERNEL_GAUSSIAN,
  SLWR_KERNEL_BISQUARE,
} slwr_kernel_type;

typedef struct {
  gsize size;
  gdouble *data;
} slwr_vector;

typedef struct {
  gsize size1;
  gsize size2;
  gdouble *data;
} slwr_matrix;

typedef enum {
  SLWR_STATUS_OK = 0,
  SLWR_STATUS_ERROR,
  SLWR_STATUS_DIMENSION_MISMATCH,
  SLWR_STATUS_NOT_FOUND,
} slwr_status;

typedef struct {
  gdouble sum_of_weights;
  gdouble mean;
} slwr_incremental_weighted_mean;
