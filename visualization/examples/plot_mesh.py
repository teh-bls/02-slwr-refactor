import argparse
import json
import sys

import matplotlib.pyplot as plt
import numpy as np

import slwr.utils as utils


def _parse_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-m',
                        '--mesh',
                        type=str,
                        required=True,
                        help='Mesh file name')
    return parser.parse_args()


def _main(mesh_filename: str):
    with open(mesh_filename) as f:
        mesh = json.load(f)
        fig = plt.figure()
        fig.canvas.manager.set_window_title(mesh_filename)
        try:
            utils.plot_mesh(fig, np.array(mesh['xx1']), np.array(mesh['xx2']),
                            np.array(mesh['z']))
        except KeyError as e:
            print(f'{e}', file=sys.stderr)
            exit(-1)
        plt.show()


if __name__ == '__main__':
    argv = vars(_parse_options())
    _main(argv['mesh'])
