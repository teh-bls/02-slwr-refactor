import slwr.utils as utils
import slwr.functions_2d as fn2d


def _main():
    for f in fn2d.available_functions().keys():
        utils.plot_available_function(f)


if __name__ == '__main__':
    _main()
