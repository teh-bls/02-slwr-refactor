from typing import List
import json

import matplotlib.pyplot as plt
import numpy as np

import slwr.utils as utils


def _plot_line(pt_from: List[List[float]], pt_to: List[List[float]]) -> None:
    a, b = [pt_from[0], pt_to[0]], [pt_from[1], pt_to[1]]
    plt.plot(np.matrix(a), np.matrix(b), color='blue', alpha=0.5)


def _plot_gng(gng_filename: str, xlims: List[float]):
    utils.prepare_figure([xlims[0] - 0.5, xlims[1] + 0.5, xlims[2] - 0.5, xlims[3] + 0.5])

    plt.xticks([xlims[0], 0.0, xlims[1]], fontsize=12)
    plt.xlabel('$x_1$', fontsize=14)
    plt.yticks([xlims[2], 0.0, xlims[3]], fontsize=12)
    plt.ylabel('$x_2$', fontsize=14)

    with open(gng_filename, 'r') as f:
        gng = json.load(f)
        for node in gng['nodes']:
            utils.plot_pt(np.matrix(node['receptive-field']['c']), size=50)
        for edge in gng['edges']:
            _plot_line(edge['a'], edge['b'])

    plt.show()


def _main():
    _plot_gng('gng-example.json', [-1.0, 1.0, -1.0, 1.0])


if __name__ == '__main__':
    _main()
