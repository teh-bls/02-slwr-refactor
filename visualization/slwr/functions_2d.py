import math
from typing import Callable, Dict, List, Union


def _sigmoid_x1(x1: float, _: float, steepness: int = 10) -> float:
    """
    Logistics sigmoid in the x1 direction.

    :param x1: the x1 value
    :param steepness: steepness of the sigmoid
    :return: the function value at (x1, x2)
    """
    result = 0
    try:
        result = 1.0 / (1.0 + math.exp(-steepness * x1))
    except OverflowError:
        pass
    return result


def _crossed_ridge(x1: float, x2: float) -> float:
    """
    The Crossed Ridge function.

    :param x1: the x1 value
    :param x2: the x2 value
    :return: the function value at (x1, x2)
    """
    return max(
        math.exp(-10 * x1**2),
        max(math.exp(-50 * x2**2), 1.25 * math.exp(-5 * (x1**2 + x2**2))))


def _rotated_sine(x1: float, x2: float) -> float:
    """
    The Rotated Sine function.

    :param x1: the x1 value
    :param x2: the x2 value
    :return: the function value at (x1, x2)
    """
    return math.sin(2 * math.pi * (x1 + x2))


def available_functions(
) -> Dict[str, Dict[str, Union[List[float], Callable]]]:
    return {
        'sigmoid-x1': {
            'domain': [-1.0, 1.0, -1.0, 1.0],
            'range': [0, 1],
            'fn': _sigmoid_x1
        },
        'crossed-ridge': {
            'domain': [-1.0, 1.0, -1.0, 1.0],
            'range': [0, 1],
            'fn': _crossed_ridge
        },
        'rotated-sine': {
            'domain': [-1.0, 1.0, -1.0, 1.0],
            'range': [-1, 1],
            'fn': _rotated_sine
        }
    }
