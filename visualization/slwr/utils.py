from typing import Dict, List, Tuple
import math
import numpy

import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np

import slwr.ellipsoid as el
import slwr.functions_2d as fn2d


def _xlims_valid(xlims: List[float]) -> bool:
    """
    Checks whether the given list of x-space limits is valid, i.e. if the list
    is of form [min_x, max_x, min_y, max_y]

    :param lims: the list of limits
    :return: True on a successful check, False otherwise
    """
    return True if len(
        xlims) == 4 and xlims[0] < xlims[1] and xlims[2] < xlims[3] else False


def _gaussian_weighted_distance(E: el.Ellipsoid, p: np.matrix) -> float:
    """
    Returns the Gaussian weighted distance from center of ellipsoid E to point
    p. The weighted distance at the center of the ellipsoid is 1.

    :param E: the ellipsoid
    :param p: the point
    :return: weighted distance between E and p in half-open interval (0, 1]
    """
    return math.exp(-0.5 * _euclidean_distance(E, p))


def _euclidean_distance(E: el.Ellipsoid, p: np.matrix) -> float:
    """
    Returns the Euclidean distance from center of ellipsoid E to point p.

    :param E: the ellipsoid
    :param p: the point
    :return: the Euclidean distance from E.c to p
    """
    return (p - E.c).transpose() * E.A * (p - E.c)


def prepare_subfigure(m: int, n: int, xlims: List[float], window_title: str = '', figure_size: List[int] = None,
                      use_tex: bool = True) -> np.ndarray:
    """
    Helper function to prepare an m x n grid of subfigures.abs

    :param m: number of rows
    :param n: number of columns
    :param xlims: a list of [min_x, max_x, min_y, max_y] limits
    :param window_title: an optional window title
    :param figure_size: [width, height] figure size in inches, None for default
    :param use_tex: whether tex mode should be used when rendering text
    :return: the array of subplot axes
    """
    if m == 1 and n == 1:
        raise Exception('both m and n are 1, use prepare_figure instead')

    if not _xlims_valid(xlims):
        raise Exception('the given limits {0} are not valid'.format(xlims))

    plt.rc('text', usetex=use_tex)

    fig, axarr = plt.subplots(m, n, sharex='col', sharey='row')

    if window_title:
        fig.canvas.manager.set_window_title(window_title)

    if figure_size:
        fig.set_size_inches(figure_size[0], figure_size[1])

    # It seems that plt.subplots simply returns a list when either m or n is 1.
    # This is why I need these special cases.
    if m == 1:
        for j in range(n):
            axarr[j].set_xlim(xlims[:2])
            axarr[j].set_ylim(xlims[2:])
            axarr[j].set_aspect('equal')
    elif n == 1:
        for i in range(m):
            axarr[i].set_xlim(xlims[:2])
            axarr[i].set_ylim(xlims[2:])
            axarr[i].set_aspect('equal')
    else:
        for i in range(m):
            for j in range(n):
                axarr[i, j].set_xlim(xlims[:2])
                axarr[i, j].set_ylim(xlims[2:])
                axarr[i, j].set_aspect('equal')

    return axarr


def prepare_figure(xlims: List[float], window_title: str = '', use_tex: bool = True) -> plt.Figure:
    """
    Helper function to prepare the figures in this file

    :param xlims: a list of [min_x, max_x, min_y, max_y] limits
    :param window_title: an optional window title
    :param use_tex: whether tex mode should be used when rendering text
    :return: the prepared figure
    """
    if not _xlims_valid(xlims):
        raise Exception('the given limits {0} are not valid'.format(xlims))

    plt.rc('text', usetex=use_tex)

    fig = plt.figure()

    if window_title:
        fig.canvas.manager.set_window_title(window_title)

    axes = fig.gca()
    axes.set_xlim(xlims[:2])
    axes.set_ylim(xlims[2:])
    axes.set_aspect('equal', adjustable='box')

    return fig


def plot_pt(pt: np.matrix, axes=None, color: str = 'blue', size: int = 1, marker: str = '.') -> None:
    """
    Helper function to plot the point pt on an existing plot

    :param pt: the point to plot
    :param axes: the axes to plot to
    :param color: color of the plotted point
    :param size: size of the plotted point
    :param marker: the marker for the point
    """
    if axes is not None:
        axes.scatter(pt.getA()[0], pt.getA()[1], color=color, marker='.', s=size)
    else:
        plt.scatter(pt.getA()[0], pt.getA()[1], color=color, marker=marker, s=size)


def apply_linear_color_gradient(c1: List[float], c2: List[float], reference: float) -> List[float]:
    """
    Helper function that takes two colors and returns a third color that lies on 
    the clamped reference point between the two colors.
    The clamped reference is forced to the interval [0.0, 1.0], where 0 means 
    total c1 color and 1 means total c2 color.

    :param c1: the first color as [r, g, b] list
    :param c2: the second color as [r, g, b] list
    :param reference: value that will be clamped to interval [0, 1]
    :return: the new color between c1 and c2
    """
    if len(c1) != 3:
        raise Exception(f'size of c1 must be 3 (got {len(c1)}')
    if len(c2) != 3:
        raise Exception(f'size of c2 must be 3 (got {len(c2)}')

    reference = np.clip(reference, 0, 1)
    c = [0, 0, 0]
    for i in range(3):
        c[i] = c1[i] * reference + c2[i] * (1 - reference)

    return c


def _append_ellipsoid_distance_to_point(E: el.Ellipsoid, pt: np.matrix) -> Tuple[np.matrix, float]:
    """
    Utility function that returns a tuple of a point and its ellipsoid weighted
    distance.

    :param E: the ellipsoid to weight the input point by
    :param pt: the input point as a np.matrix of shape (2, 1)
    :return: tuple of pt and its weighted distance
    """
    return pt, _gaussian_weighted_distance(E, pt)


def generate_ellipsoid_distribution_random_standard_normal(E: el.Ellipsoid, num_rows: int = 100, num_cols: int = 100) -> \
        List[Tuple[np.matrix, float]]:
    """
    Generates a grid of points representing a two-dimensional random standard
    normal distribution given the ellipsoid E and the number of rows and cols.

    TODO: enable mean and stddev other than [0, 1]

    :param E: The ellipsoid used to generate the distribution
    :param num_rows: integer number of rows
    :param num_cols: integer number of columns
    :return: a list of tuples (np.matrix, weight) consisting of the point and
             its weight
    """
    data = [(0.0, 0.0)] * num_rows * num_cols
    data = list(
        map(lambda x: np.matrix((np.random.standard_normal(), np.random.standard_normal())).reshape(2, 1), data))
    return list(map(lambda x: _append_ellipsoid_distance_to_point(E, x), data))


def generate_ellipsoid_distribution_random_uniform(
        E: el.Ellipsoid,
        xlims: List[float],
        num_rows: int = 100,
        num_cols: int = 100) -> List[Tuple[np.matrix, float]]:
    """
    Generates a grid of points representing a two-dimensional random uniform
    distribution given the ellipsoid E, the two-dimensional limits, and the
    number of rows and cols.

    :param E: The ellipsoid used to generate the distribution
    :param xlims: limits of form [x_min, x_max, y_min, y_max]
    :param num_rows: integer number of rows
    :param num_cols: integer number of columns
    :return: a list of tuples (np.matrix, weight) consisting of the point and
             its weight
    """
    if not _xlims_valid(xlims):
        raise Exception('given limits {0} are not valid'.format(xlims))

    data = [(0.0, 0.0)] * num_rows * num_cols
    data = list(
        map(lambda x: np.matrix((np.random.uniform(xlims[0], xlims[1]), np.random.uniform(xlims[2], xlims[3]))).reshape(
            2, 1), data))
    return list(map(lambda x: _append_ellipsoid_distance_to_point(E, x), data))


def generate_ellipsoid_distribution_grid(
        E: el.Ellipsoid,
        xlims: List[float],
        num_rows: int = 100,
        num_cols: int = 100,
        vertical: bool = True) -> List[Tuple[np.matrix, float]]:
    """
    Generates a grid of points representing a two-dimensional ellipsoidal
    distribution given the ellipsoid E, the two-dimensional limits, and the
    number or rows and columns.

    TODO: admittedly, I am not entirely sure how np.mgrid and np.vstack work

    :param E: The ellipsoid used to generate the distribution
    :param xlims: limits of form [x_min, x_max, y_min, y_max]
    :param num_rows: integer number of rows
    :param num_cols: integer number of columns
    :param vertical: whether the points returned should be ordered vertically (True) or horizontally (False)
    :return: a list of tuples (np.matrix, weight) consisting of the point and
             its weight
    """
    if not _xlims_valid(xlims):
        raise Exception('given limits {0} are not valid'.format(xlims))

    X, Y = None, None
    if vertical:
        X, Y = np.mgrid[xlims[0]:xlims[1]:complex(0, num_cols), xlims[2]:xlims[3]:complex(0, num_rows)]
    else:
        Y, X = np.mgrid[xlims[0]:xlims[1]:complex(0, num_rows), xlims[2]:xlims[3]:complex(0, num_cols)]

    grid = np.vstack((X.flatten(), Y.flatten())).T

    res = []

    for pt in grid:
        input_pt = np.matrix(pt.reshape((2, 1)))
        weight = _gaussian_weighted_distance(E, input_pt)

        res.append((input_pt, weight))

    return res


def generate_square_meshgrid(num: int, xlims: List[float]) -> numpy.ndarray:
    """
    Generates a square num x num meshgrid from the given limits.

    :param num: dimension of the meshgrid
    :param xlims: limits of the meshgrid
    :return: the generated meshgrid
    """
    x1 = np.linspace(xlims[0], xlims[1], num=num)
    x2 = np.linspace(xlims[2], xlims[3], num=num)
    return np.meshgrid(x1, x2, sparse=False)


def plot_mesh(fig: matplotlib.figure.Figure, xx1: np.array, xx2: np.array, z: np.array) -> matplotlib.pyplot.axes:
    """
    Plots the mesh represented by xx1, xx1, and z.

    :param fig: Figure to plot the mesh to
    :param xx1: The x1 mesh coordinates (non-sparse)
    :param xx2: The x2 mesh coordinates (non-sparse)
    :param z: The (x1, x2) value
    :return: The axes containing the mesh
    """
    plt.rc('text', usetex=True)
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(azim=-135, elev=45)

    # Set tick parameters up
    ax.tick_params(axis='both', which='both', bottom=False, top=False, labelbottom=True, right=False, left=False,
                   labelleft=True)
    # Clear ticks - should be set by the caller
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])

    # Clear background colors
    ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
    ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))

    ax.plot_trisurf(xx1.flatten(), xx2.flatten(), z.flatten(), color='white', edgecolors=['black'])
    return ax


def plot_available_function(fn_name: str, grid_size: int = 15) -> None:
    try:
        fn = fn2d.available_functions()[fn_name]['fn']
        domain = fn2d.available_functions()[fn_name]['domain']
        xx1, xx2 = generate_square_meshgrid(grid_size, domain)
        rnge = fn2d.available_functions()[fn_name]['range']

        v_fn = np.vectorize(lambda x1, x2, fnc: fnc(x1, x2))
        z = v_fn(xx1, xx2, fn)

        fig = plt.figure()
        ax = plot_mesh(fig, xx1, xx2, z)

        ax.set_yticks([domain[2], 0, domain[3]])
        ax.set_yticklabels([domain[2], 0, domain[3]], fontsize=12)
        ax.set_ylabel('$x_2$', fontsize=14)

        ax.set_xticks([domain[0], 0, domain[1]])
        ax.set_xticklabels([domain[1], 0, domain[2]], fontsize=12)
        ax.set_xlabel('$x_1$', fontsize=14)

        ax.set_zticks([rnge[0], rnge[1]])
        ax.set_zticklabels([rnge[0], rnge[1]], fontsize=12)
        ax.set_zlabel('$y$', fontsize=14)

        plt.show()
    except KeyError:
        print(f'function "{fn_name}" not found; available functions are {list(fn2d.available_functions().keys())}')


def plot_receptive_field_population(ax: plt.Axes, population: Dict[str, List[Dict[str, List[List[float]]]]]) -> None:
    try:
        for rf in population['receptive-fields']:
            E = el.Ellipsoid(A=np.matrix(rf['D']), c=np.matrix(rf['c']))
            x, y = E.uniformly_distribute_points()
            ax.plot(x, y, color='blue')
    except KeyError:
        print(f'required key "receptive-fields" not found in {population}')
