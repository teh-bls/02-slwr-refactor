from typing import List, Tuple, Union
import random

import numpy as np


class Ellipsoid:
    """
    A representation of a 2D or a 3D ellipsoid.
    """

    def __init__(self, A: np.matrix = np.matrix([[1.0, 0.0], [0.0, 1.0]]), c: np.matrix = np.matrix([[0.0], [0.0]])):
        if not (A.shape == (2, 2) or A.shape == (3, 3)):
            raise Exception('A must be either 2x2 or 3x3')
        if A.shape[0] != c.shape[0]:
            raise Exception('The rows of c must match the dimensionality of A')
        if not np.all(np.linalg.eigvals(A)) > 0:
            raise Exception('A is not PSD')
        self.A = A
        self.c = c

    def uniformly_distribute_points(self, num_pts: int = 50) -> Union[Tuple[List[float], List[float]], None]:
        """
        Uniformly distribute points along the edge of this ellipsoid.

        :param num_pts: the number of points to uniformly distribute
        :return: the points as two lists of x and y coordinates, suitable for plotting
        """
        if self.A.shape == (2, 2):
            return self._uniformly_distribute_points_2d(num_pts)
        elif self.A.shape == (3, 3):
            return self._uniformly_distribute_points_3d(num_pts)
        return None

    def randomly_sample_edge_points(self, num_pts: int = 50) -> List[np.matrix]:
        """
        Randomly sample points from the edge of this ellipsoid.

        :param: num_pts: the number of points to sample
        :return: a list of np.matrix elements representing randomly sampled points from the edge of this ellipsoid
        """
        if self.A.size == (2, 2):
            return self._randomly_sample_edge_points_2d(num_pts)
        else:
            raise Exception(f'Randomly sampling points for {self.A.size[0]}-dimensional ellipsoid not implemented')

    def _uniformly_distribute_points_2d(self, num_pts: int) -> Tuple[List[float], List[float]]:
        """
        Uniformly distribute 2d points along the edge of this ellipsoid.

        :param num_pts: the number of points to uniformly distribute
        :return: the points as two lists of x and y coordinates, suitable for plotting
        """
        ellipsoid_pts = self._distribute_points_2d(
            np.linspace(0.0, 2 * np.pi, num=num_pts))
        return self._flatten_to_xs_and_ys(ellipsoid_pts)

    def _randomly_sample_edge_points_2d(self, num_pts: int) -> List[np.matrix]:
        """
        Randomly sample points from the edge of this ellipsoid.

        :param num_pts: the number of points to sample
        :return: a list of np.matrix elements representing randomly sampled points from the edge of this ellipsoid
        """

        def random_angle() -> float:
            return random.uniform(0, 2 * np.pi)

        return self._distribute_points_2d([random_angle() for _ in range(num_pts)])

    def _distribute_points_2d(self, angles: Union[List[float], np.ndarray]) -> List[np.matrix]:
        """
        Distributes the points along the edge of this ellipsoid from a list of angles.

        :param angles: the list of angles
        :return: a list of np.matrix elements representing the points corresponding to the angles
        """
        unit_circle = list(map(lambda x: np.matrix([[np.cos(x)], [np.sin(x)]]), angles))

        np.linalg.cholesky(self.A)
        e, V = np.linalg.eig(self.A)
        E = np.zeros((2, 2))
        for i in range(2):
            E[i, i] = np.sqrt(1 / e[i])
        X = V.dot(E)

        return [X.dot(i) + self.c for i in unit_circle]
        # for i in unit_circle:
        #     ellipsoid_pts.append(X.dot(i) + self.c)
        # return ellipsoid_pts

    @staticmethod
    def _uniformly_distribute_points_3d(num_pts: int) -> None:
        return None

    @staticmethod
    def _flatten_to_xs_and_ys(ellipsoid_pts: List[np.matrix]) -> Tuple[List[float], List[float]]:
        """
        Flattens a list of np.matrices to two lists representing the x and y coordinates.

        :param ellipsoid_pts: the list of np.matrix points
        :return: two lists of x and y coordinates, respectively
        """
        # x, y = [], []
        # for pt in ellipsoid_pts:
        #     x.append(pt[0, 0])
        #     y.append(pt[1, 0])

        # return x, y
        return [p[0, 0] for p in ellipsoid_pts], [p[1, 0] for p in ellipsoid_pts]
