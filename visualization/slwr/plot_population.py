from typing import Dict, List
import argparse
import json
import math
import sys

import matplotlib.pyplot as plt

import slwr.utils as utils


def _parse_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-p',
                        '--population',
                        type=str,
                        required=True,
                        help='Population file name')
    return parser.parse_args()


def _find_xlims(
        population: Dict[str, List[Dict[str,
                                        List[List[float]]]]]) -> List[float]:
    xlims = None
    try:
        centers = list(map(lambda x: x['c'], population['receptive-fields']))
        x1, x2 = list(map(lambda x: x[0],
                          centers)), list(map(lambda x: x[1], centers))
        xlims = [
            math.floor(min(x1)[0]),
            math.ceil(max(x1)[0]),
            math.floor(min(x2)[0]),
            math.ceil(max(x1)[0])
        ]
    except KeyError as e:
        print(e, file=sys.stderr)
    return xlims


def _main(population_filename: str):
    with open(population_filename) as f:
        population = json.load(f)
        xlims = _find_xlims(population)

        fig = utils.prepare_figure(
            [xlims[0] - 1.0, xlims[1] + 1.0, xlims[2] - 1.0, xlims[3] + 1.0])
        plt.xticks([xlims[0], 0.0, xlims[1]], fontsize=12)
        plt.xlabel('$x_1$', fontsize=14)
        plt.yticks([xlims[2], 0.0, xlims[3]], fontsize=12)
        plt.ylabel('$x_2$', fontsize=14)
        fig.canvas.manager.set_window_title(population_filename)
        utils.plot_receptive_field_population(population)
        plt.show()


if __name__ == '__main__':
    argv = vars(_parse_options())
    _main(argv['population'])
