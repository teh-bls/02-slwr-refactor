#include "evaluation-dataset.h"

#include <jansson.h>
#include <slwr/slwr.h>

#include <stdlib.h>

evaluation_dataset_statistics *
evaluation_dataset_statistics_alloc(gsize n_in, gsize n_out) {
  evaluation_dataset_statistics *stats =
      (evaluation_dataset_statistics *)g_malloc0(
          sizeof(evaluation_dataset_statistics));
  if (!stats) {
    goto on_error;
  }

  stats->in_min = slwr_vector_alloc(n_in);
  if (!stats->in_min) {
    goto on_error;
  }
  slwr_vector_set_all(stats->in_min, DBL_MAX);

  stats->in_max = slwr_vector_alloc(n_in);
  if (!stats->in_max) {
    goto on_error;
  }
  slwr_vector_set_all(stats->in_max, DBL_MIN);

  stats->out_min = slwr_vector_alloc(n_out);
  if (!stats->out_min) {
    goto on_error;
  }
  slwr_vector_set_all(stats->out_min, DBL_MAX);

  stats->out_max = slwr_vector_alloc(n_out);
  if (!stats->out_max) {
    goto on_error;
  }
  slwr_vector_set_all(stats->out_max, DBL_MIN);

  return stats;

on_error:
  evaluation_dataset_statistics_free(stats);
  return NULL;
}

void evaluation_dataset_statistics_free(evaluation_dataset_statistics *stats) {
  if (!stats) {
    return;
  }
  if (stats->out_max) {
    slwr_vector_free(stats->out_max);
  }
  if (stats->out_min) {
    slwr_vector_free(stats->out_min);
  }
  if (stats->in_max) {
    slwr_vector_free(stats->in_max);
  }
  if (stats->in_min) {
    slwr_vector_free(stats->in_min);
  }
  g_free(stats);
}

static void randomize_input(const slwr_synthetic_data_generator *desc,
                            slwr_vector *x) {
  for (gsize i = 0; i < desc->n_in; ++i) {
    x->data[i] = g_random_double_range(desc->domain_min->data[i],
                                       desc->domain_max->data[i]);
  }
}

static void update_min_vector(const slwr_vector *a, slwr_vector *min) {
  g_return_if_fail(a->size == min->size);
  for (gsize i = 0; i < a->size; ++i) {
    if (a->data[i] < min->data[i]) {
      min->data[i] = a->data[i];
    }
  }
}

static void update_max_vector(const slwr_vector *a, slwr_vector *max) {
  g_return_if_fail(a->size == max->size);
  for (gsize i = 0; i < a->size; ++i) {
    if (a->data[i] > max->data[i]) {
      max->data[i] = a->data[i];
    }
  }
}

/**
 *
 * Expects that the dataset has been allocated and the "metadata" initialized.
 *
 * @param generating_fn
 * @param dataset
 * @return SLWR_STATUS_OK on success, SLWR_STATUS_ERROR on failure
 */
static slwr_status
generate_data_for_dataset(const slwr_synthetic_data_generator *generating_fn,
                          evaluation_dataset *dataset) {
  g_return_val_if_fail(generating_fn != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(dataset != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(dataset->data->size !=
                           dataset->size * dataset->n_in * dataset->n_out,
                       SLWR_STATUS_ERROR);

  slwr_vector x = {.size = dataset->n_in, .data = dataset->data->data};
  slwr_vector y = {.size = dataset->n_out,
                   .data = dataset->data->data + dataset->n_in};
  for (gsize i = 0; i < dataset->size; ++i) {
    randomize_input(generating_fn, &x);
    slwr_status status = generating_fn->generate(&x, &y);
    if (status != SLWR_STATUS_OK) {
      return status;
    }
    update_min_vector(&x, dataset->stats->in_min);
    update_max_vector(&x, dataset->stats->in_max);
    update_min_vector(&y, dataset->stats->out_min);
    update_max_vector(&y, dataset->stats->out_max);
    x.data += dataset->n_in + dataset->n_out;
    y.data = x.data + dataset->n_in;
  }
  return SLWR_STATUS_OK;
}

evaluation_dataset *evaluation_dataset_alloc(const gchar *name, gsize n_in,
                                             gsize n_out, gsize size) {
  g_return_val_if_fail(name != NULL, NULL);

  evaluation_dataset *dataset = g_malloc0(sizeof(evaluation_dataset));
  if (!dataset) {
    goto on_error;
  }
  dataset->name = g_strdup(name);
  if (!dataset->name) {
    goto on_error;
  }
  dataset->size = size;
  dataset->created_at = slwr_time_now_as_str();
  if (!dataset->created_at) {
    goto on_error;
  }
  dataset->n_in = n_in;
  dataset->n_out = n_out;
  dataset->data = slwr_vector_alloc((dataset->n_in + dataset->n_out) * size);
  if (!dataset->data) {
    goto on_error;
  }
  dataset->stats =
      evaluation_dataset_statistics_alloc(dataset->n_in, dataset->n_out);
  if (!dataset->stats) {
    goto on_error;
  }
  return dataset;

on_error:
  evaluation_dataset_free(dataset);
  return NULL;
}

evaluation_dataset *generate_evaluation_dataset(
    const slwr_synthetic_data_generator *generating_function_metadata,
    gsize size) {
  g_return_val_if_fail(generating_function_metadata != NULL, NULL);

  evaluation_dataset *dataset = evaluation_dataset_alloc(
      generating_function_metadata->name, generating_function_metadata->n_in,
      generating_function_metadata->n_out, size);
  if (!dataset) {
    goto on_error;
  }
  if (generate_data_for_dataset(generating_function_metadata, dataset) !=
      SLWR_STATUS_OK) {
    goto on_error;
  }
  return dataset;

on_error:
  evaluation_dataset_free(dataset);
  return NULL;
}

void evaluation_dataset_free(evaluation_dataset *dataset) {
  if (!dataset) {
    return;
  }
  if (dataset->stats) {
    evaluation_dataset_statistics_free(dataset->stats);
  }
  if (dataset->data) {
    slwr_vector_free(dataset->data);
  }
  if (dataset->created_at) {
    g_free(dataset->created_at);
  }
  if (dataset->name) {
    g_free(dataset->name);
  }
  g_free(dataset);
}

static json_t *
dataset_statistics_to_json(const evaluation_dataset_statistics *stats) {
  g_return_val_if_fail(stats != NULL, NULL);

  json_t *stats_obj = NULL;
  json_t *entry = NULL;

  stats_obj = json_object();
  if (!stats_obj) {
    goto on_error;
  }
  entry = slwr_vector_to_json(stats->in_min);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(stats_obj, "data-in-min", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = slwr_vector_to_json(stats->in_max);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(stats_obj, "data-in-max", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = slwr_vector_to_json(stats->out_min);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(stats_obj, "data-out-min", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = slwr_vector_to_json(stats->out_max);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(stats_obj, "data-out-max", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  return stats_obj;

on_error:
  if (entry) {
    json_decref(entry);
  }
  if (stats_obj) {
    json_decref(stats_obj);
  }
  return NULL;
}

json_t *evaluation_dataset_to_json(const evaluation_dataset *dataset) {
  g_return_val_if_fail(dataset != NULL, NULL);

  json_t *dataset_obj = NULL;
  json_t *entry = NULL;

  dataset_obj = json_object();
  if (!dataset_obj) {
    goto on_error;
  }
  entry = json_string(dataset->name);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "name", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = json_integer((gint)dataset->size);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "size", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = json_string(dataset->created_at);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "created-at", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = json_integer((gint)dataset->n_in);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "n-in", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = json_integer((gint)dataset->n_out);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "n-out", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = slwr_vector_to_json(dataset->data);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "data", entry) != 0) {
    goto on_error;
  }
  json_decref(entry);
  entry = dataset_statistics_to_json(dataset->stats);
  if (!entry) {
    goto on_error;
  }
  if (json_object_set(dataset_obj, "statistics", entry)) {
    goto on_error;
  }
  json_decref(entry);
  return dataset_obj;

on_error:
  if (entry) {
    json_decref(entry);
  }
  if (dataset_obj) {
    json_decref(dataset_obj);
  }
  return NULL;
}

/**
 * Creates an slwr_vector from the JSON array at the given key in the given JSON
 * object. The pointer to the created vector is returned.
 *
 * @param obj The JSON object that contains the JSON array.
 * @param key The key at which the JSON array is located.
 * @return (transfer full) The pointer to the created vector on success, NULL on
 * failure.
 */
static slwr_vector *create_vector_from_json_array_at_key(const json_t *obj,
                                                         const gchar *key) {
  g_return_val_if_fail(obj != NULL, NULL);
  g_return_val_if_fail(json_is_object(obj), NULL);
  g_return_val_if_fail(key != NULL, NULL);

  json_t *array = json_object_get(obj, key);
  if (!array || !json_is_array(array)) {
    return NULL;
  }
  return slwr_vector_from_json(array);
}

static evaluation_dataset_statistics *
dataset_statistics_from_json(const json_t *json) {
  g_return_val_if_fail(json != NULL, NULL);
  g_return_val_if_fail(json_is_object(json), NULL);

  evaluation_dataset_statistics *stats =
      (evaluation_dataset_statistics *)g_malloc0(
          sizeof(evaluation_dataset_statistics));
  if (!stats) {
    goto on_error;
  }
  stats->in_min = create_vector_from_json_array_at_key(json, "data-in-min");
  if (!stats->in_min) {
    goto on_error;
  }
  stats->in_max = create_vector_from_json_array_at_key(json, "data-in-max");
  if (!stats->in_max) {
    goto on_error;
  }
  stats->out_min = create_vector_from_json_array_at_key(json, "data-out-min");
  if (!stats->out_min) {
    goto on_error;
  }
  stats->out_max = create_vector_from_json_array_at_key(json, "data-out-max");
  if (!stats->out_max) {
    goto on_error;
  }
  return stats;

on_error:
  if (!stats) {
    evaluation_dataset_statistics_free(stats);
  }
  return NULL;
}

evaluation_dataset *evaluation_dataset_from_json_string(const gchar *json) {
  g_return_val_if_fail(json != NULL, NULL);

  json_t *root_obj = NULL;
  json_t *entry = NULL;
  json_error_t json_error;
  evaluation_dataset *dataset = NULL;

  root_obj = slwr_load_json_string(json);
  if (!root_obj) {
    goto on_error;
  }

  dataset = (evaluation_dataset *)g_malloc0(sizeof(evaluation_dataset));
  if (!dataset) {
    goto on_error;
  }
  const gchar *fmt = "{s:i, s:i, s:i}";
  if (json_unpack_ex(root_obj, &json_error, 0, fmt, "size", &dataset->size,
                     "n-in", &dataset->n_in, "n-out", &dataset->n_out) != 0) {
    goto on_error;
  }
  entry = json_object_get(root_obj, "name");
  if (!entry || !json_is_string(entry)) {
    goto on_error;
  }
  dataset->name = g_strdup(json_string_value(entry));
  if (!dataset->name) {
    goto on_error;
  }
  entry = json_object_get(root_obj, "created-at");
  if (!entry || !json_is_string(entry)) {
    goto on_error;
  }
  dataset->created_at = g_strdup(json_string_value(entry));
  if (!dataset->created_at) {
    goto on_error;
  }
  entry = json_object_get(root_obj, "data");
  if (!entry || !json_is_array(entry)) {
    goto on_error;
  }
  dataset->data = slwr_vector_from_json(entry);
  if (!dataset->data) {
    goto on_error;
  }
  entry = json_object_get(root_obj, "statistics");
  if (!entry || !json_is_object(entry)) {
    goto on_error;
  }
  dataset->stats = dataset_statistics_from_json(entry);
  if (!dataset->stats) {
    goto on_error;
  }

  json_decref(root_obj);
  return dataset;

on_error:
  if (dataset) {
    evaluation_dataset_free(dataset);
  }
  if (root_obj) {
    json_decref(root_obj);
  }
  return NULL;
}
