#pragma once

#include <slwr/slwr.h>

#include <glib.h>
#include <jansson.h>

typedef struct {
  slwr_vector *in_min;
  slwr_vector *in_max;
  slwr_vector *out_min;
  slwr_vector *out_max;
} evaluation_dataset_statistics;

typedef struct {
  gchar *name;
  gsize size;
  gchar *created_at;
  gsize n_in;
  gsize n_out;
  slwr_vector *data;
  evaluation_dataset_statistics *stats;
} evaluation_dataset;

evaluation_dataset_statistics *evaluation_dataset_statistics_alloc(gsize n_in,
                                                                   gsize n_out);
void evaluation_dataset_statistics_free(evaluation_dataset_statistics *stats);

evaluation_dataset *evaluation_dataset_alloc(const gchar *name, gsize n_in,
                                             gsize n_out, gsize size);
evaluation_dataset *generate_evaluation_dataset(
    const slwr_synthetic_data_generator *generating_function_metadata,
    gsize size);
void evaluation_dataset_free(evaluation_dataset *dataset);

json_t *evaluation_dataset_to_json(const evaluation_dataset *dataset);
evaluation_dataset *evaluation_dataset_from_json_string(const gchar *json);
