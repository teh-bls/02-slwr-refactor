#include "cl-options.h"

#include <slwr/utils/slwr-synthetic-data.h>

static gchar *generating_function = NULL;
static gchar *output_filename = NULL;
static int dataset_size = 50000;

static gchar *generating_function_name_option_description() {
  gchar *fns = slwr_available_generator_functions_to_str();
  gchar *description =
      g_strdup_printf("The name of the function to generate the dataset from; "
                      "available generator functions are: %s",
                      fns);
  g_free(fns);
  return description;
}

gboolean cl_options_parse(int argc, char *argv[]) {
  gchar *generating_fn_name_description =
      generating_function_name_option_description();
  const GOptionEntry option_entries[] = {
      {"generating-function", 'g', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
       &generating_function, generating_fn_name_description, NULL},
      {"output-filename", 'o', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
       &output_filename, "Filename to save the generated dataset to", NULL},
      {"dataset-size", 's', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &dataset_size,
       "Dataset size", NULL},
      {NULL}};

  GOptionContext *context = g_option_context_new(NULL);
  g_option_context_add_main_entries(context, option_entries, NULL);

  gboolean result = TRUE;
  GError *error = NULL;
  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    g_critical("%s: %s", __PRETTY_FUNCTION__, error->message);
    result = FALSE;
    g_error_free(error);
  }
  g_option_context_free(context);
  g_free(generating_fn_name_description);
  return result;
}

gboolean cl_options_validate() {
  if (!generating_function) {
    g_critical("generating function name must be provided");
    return FALSE;
  }
  if (!slwr_valid_generator_function_name(generating_function)) {
    gchar *fns = slwr_available_generator_functions_to_str();
    g_critical("generating function with name %s is not available; available "
               "functions are %s",
               generating_function, fns);
    g_free(fns);
    return FALSE;
  }
  return TRUE;
}

gchar *cl_options_generating_function() { return generating_function; }

gchar *cl_options_output_filename() { return output_filename; }

gsize cl_options_training_dataset_size() { return (gsize)dataset_size; }
