#pragma once

#include <glib.h>

gboolean cl_options_parse(int argc, char *argv[]);
gboolean cl_options_validate();

gchar *cl_options_generating_function();
gchar *cl_options_output_filename();
gsize cl_options_training_dataset_size();
