#include "cl-options.h"
#include "evaluation/common/evaluation-dataset.h"

#include "slwr/utils/slwr-json-utils.h"

#include <stdlib.h>

int main(int argc, char *argv[]) {
  if (!cl_options_parse(argc, argv)) {
    return EXIT_FAILURE;
  }
  if (!cl_options_validate()) {
    return EXIT_FAILURE;
  }
  slwr_synthetic_data_generator *generating_function_metadata = NULL;
  evaluation_dataset *dataset = NULL;
  json_t *json = NULL;
  gchar *output_filename = NULL;
  slwr_status status;

  generating_function_metadata =
      slwr_synthetic_data_generator_from_name(cl_options_generating_function());
  if (!generating_function_metadata) {
    g_critical("could not obtain metadata for generating function %s",
               cl_options_generating_function());
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  dataset = generate_evaluation_dataset(generating_function_metadata,
                                        cl_options_training_dataset_size());
  if (!dataset) {
    g_critical(
        "could not generate dataset of size %lu for generating function %s",
        cl_options_training_dataset_size(), cl_options_generating_function());
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  json = evaluation_dataset_to_json(dataset);
  if (!json) {
    g_critical("could not convert the generated dataset to JSON");
    status = SLWR_STATUS_ERROR;
    goto end;
  }

  if (cl_options_output_filename()) {
    if ((status = slwr_write_compact_json_to_file(
             json, cl_options_output_filename())) != SLWR_STATUS_OK) {
      g_critical("could not write the generated dataset to %s",
                 cl_options_output_filename());
      goto end;
    }
    g_message("generated dataset written to %s", cl_options_output_filename());
  } else {
    output_filename =
        g_strdup_printf("%s-dataset-%lu.json", cl_options_generating_function(),
                        cl_options_training_dataset_size());
    if ((status = slwr_write_compact_json_to_file(json, output_filename)) !=
        SLWR_STATUS_OK) {
      g_critical("could not write the generated dataset to %s",
                 output_filename);
      goto end;
    }
    g_message("generated dataset written to %s", output_filename);
  }

end:
  if (output_filename) {
    g_free(output_filename);
  }
  if (json) {
    json_decref(json);
  }
  if (dataset) {
    evaluation_dataset_free(dataset);
  }
  if (generating_function_metadata) {
    slwr_synthetic_data_free(generating_function_metadata);
  }
  return status == SLWR_STATUS_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
