#pragma once

#include <slwr/slwr.h>

#include <glib.h>

typedef struct {
  gsize size;
  slwr_matrix *xx1;
  slwr_matrix *xx2;
} mesh_grid_2d;

mesh_grid_2d *mesh_grid_2d_alloc(gsize grid_size);
void mesh_grid_2d_free(mesh_grid_2d *grid);
slwr_status
mesh_grid_2d_initialize(const slwr_synthetic_data_generator *generator,
                        mesh_grid_2d *grid);
