#include "cl-options.h"
#include "evaluation/common/evaluation-dataset.h"
#include "io-utils.h"
#include "prediction.h"

#include <slwr/slwr.h>

#include <stdlib.h>

static inline gboolean
learning_problem_is_plottable(const slwr_network_context *ctx) {
  return ctx->network_params->n_in == 2 && ctx->network_params->n_out == 1;
}

/**
 * Takes the snapshot of the training progress after n updates. The snapshot
 * includes the model populations and the MAE.
 *
 * @param ws The slwr workspace
 * @param n The index of the most recent update
 * @param network_ctx The network
 * @param prediction_ctx The prediction context
 * @param paths The current file paths
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status take_nth_snapshot(slwr_workspace *ws, gsize n,
                                     const slwr_network_context *network_ctx,
                                     const prediction_context *prediction_ctx,
                                     const results_filepaths *paths) {
  g_return_val_if_fail(ws != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(network_ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);

  const gsize n_out = network_ctx->network_params->n_out;
  gdouble mae_data[n_out];
  slwr_vector mae = {.size = n_out, .data = mae_data};
  slwr_vector *y_pred = predict(ws, network_ctx, prediction_ctx, &mae);
  if (!y_pred) {
    g_critical("%s: could not predict using the network after %lu updates",
               __PRETTY_FUNCTION__, n);
    return SLWR_STATUS_ERROR;
  }

  slwr_status status;
  if ((status = io_utils_write_nth_populations_snapshot(
           paths, network_ctx->populations, n)) != SLWR_STATUS_OK) {
    g_critical("%s: could not write the population snapshot after %lu updates",
               __PRETTY_FUNCTION__, n);
    goto end;
  }
  if ((status = io_utils_write_nth_mae_snapshot(paths, &mae, n)) !=
      SLWR_STATUS_OK) {
    g_critical("%s: could not write the MAE snapshot after %lu updates",
               __PRETTY_FUNCTION__, n);
    goto end;
  }
  if (learning_problem_is_plottable(network_ctx)) {
    if ((status = io_utils_write_nth_prediction_mesh_snapshot(
             paths, prediction_ctx, y_pred, n)) != SLWR_STATUS_OK) {
      g_critical(
          "%s: could not write the prediction mesh snapshot after %lu updates",
          __PRETTY_FUNCTION__, n);
      goto end;
    }
    if ((status = io_utils_write_nth_error_mesh_snapshot(
             paths, prediction_ctx, y_pred, n)) != SLWR_STATUS_OK) {
      g_critical(
          "%s: could not write the error mesh snapshot after %lu updates",
          __PRETTY_FUNCTION__, n);
      goto end;
    }
  }

end:
  slwr_vector_free(y_pred);
  return status;
}

/**
 * Determines, based on the corresponding command-line parameter, whether a
 * snapshot of the training progress should be taken.
 *
 * @param n The index of the most recent update
 * @return TRUE if the shapshot should be taken, FALSE otherwise
 */
static inline gboolean should_take_nth_snapshot(gsize n) {
  return cl_options_snapshot_interval() > 0 &&
         n % cl_options_snapshot_interval() == 0;
}

/**
 * Trains a network on the given training dataset while using the given
 * prediction context to take snapshots during training.
 *
 * @param ws The slwr workspace
 * @param training_dataset The dataset to train the network on
 * @param prediction_ctx The prediction context to take snapshots from
 * @param paths The current file paths
 * @param params_str The network parameters
 * @return (nullable) (transfer full) A context representing the trained network
 */
static slwr_network_context *train(slwr_workspace *ws,
                                   const evaluation_dataset *training_dataset,
                                   const prediction_context *prediction_ctx,
                                   const results_filepaths *paths,
                                   const gchar *params_str) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(training_dataset != NULL, NULL);
  g_return_val_if_fail(prediction_ctx != NULL, NULL);
  g_return_val_if_fail(paths != NULL, NULL);
  g_return_val_if_fail(params_str != NULL, NULL);

  slwr_network_context *network_ctx = NULL;

  network_ctx = slwr_network_context_alloc_from_json_string(params_str);
  if (!network_ctx) {
    g_critical("%s: could not allocate an slwr network context",
               __PRETTY_FUNCTION__);
    goto on_error;
  }

  const gsize n_in = training_dataset->n_in;
  const gsize n_out = training_dataset->n_out;
  slwr_vector x = {.size = n_in, .data = training_dataset->data->data};
  slwr_vector y = {.size = n_out, .data = training_dataset->data->data + n_in};
  for (gsize i = 1; i <= training_dataset->size; ++i) {
    if (slwr_network_update(ws, network_ctx, &x, &y) != SLWR_STATUS_OK) {
      goto on_error;
    }
    x.data += n_in + n_out;
    y.data = x.data + n_in;

    if (should_take_nth_snapshot(i)) {
      if (take_nth_snapshot(ws, i, network_ctx, prediction_ctx, paths) !=
          SLWR_STATUS_OK) {
        goto on_error;
      }
    }
  }
  return network_ctx;

on_error:
  if (network_ctx) {
    slwr_network_context_free(network_ctx);
  }
  return NULL;
}

/**
 * Takes the final training snapshot.
 *
 * @param ctx The context representing the network
 * @param training_dataset The training dataset
 * @param prediction_ctx The current prediction context
 * @param mae The MAE
 * @param paths The current paths
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status
take_final_snapshot(const slwr_network_context *ctx,
                    const evaluation_dataset *training_dataset,
                    const prediction_context *prediction_ctx,
                    const slwr_vector *mae, const results_filepaths *paths) {
  g_return_val_if_fail(ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(mae != NULL, SLWR_STATUS_ERROR);

  if (io_utils_write_training_dataset(paths, training_dataset) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (io_utils_write_prediction_dataset(paths, prediction_ctx->dataset) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (io_utils_write_final_populations(paths, ctx->populations) !=
      SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  if (io_utils_write_final_mae(paths, mae) != SLWR_STATUS_OK) {
    return SLWR_STATUS_ERROR;
  }
  return SLWR_STATUS_OK;
}

static inline void print_mae_for_run_number(const slwr_vector *mae,
                                            gsize run_number) {
  gchar *mae_str = slwr_vector_to_str(mae);
  if (!mae_str) {
    g_critical("%s: could not print the MAE to console for run %lu",
               __PRETTY_FUNCTION__, run_number);
    return;
  }
  g_message("MAE = %s for run %lu", mae_str, run_number);
  g_free(mae_str);
}

/**
 * Performs a number of independent training runs.
 *
 * @param generator The synthetic data generator for the chosen function
 * @param prediction_ctx The prediction context
 * @return SLWR_STATUS on success, an slwr_status indicating an error otherwise
 */
static slwr_status perform_independent_training_runs(
    const slwr_synthetic_data_generator *generator,
    const prediction_context *prediction_ctx) {
  g_return_val_if_fail(generator != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx != NULL, SLWR_STATUS_ERROR);

  const gsize n_in = generator->n_in;
  const gsize n_out = generator->n_out;

  slwr_workspace *ws = NULL;
  results_filepaths *paths = NULL;
  gchar *params_str = NULL;
  evaluation_dataset *training_dataset = NULL;
  slwr_network_context *ctx = NULL;
  gdouble mae_data[n_out];
  slwr_vector mae = {.size = n_out, .data = mae_data};
  slwr_vector *y_pred = NULL;

  ws = slwr_workspace_alloc(n_in, n_out);
  if (!ws) {
    g_critical("%s: could not allocate an slwr workspace", __PRETTY_FUNCTION__);
    goto on_error;
  }
  paths = io_utils_create_results_base_directory(cl_options_generate_from());
  if (!paths) {
    g_critical("%s: could not create the results directory",
               __PRETTY_FUNCTION__);
    goto on_error;
  }
  g_message("results will be written to %s",
            results_filepaths_base_directory(paths));
  if (prediction_ctx->grid && prediction_ctx->z) {
    if (io_utils_write_generating_function_mesh(
            paths, prediction_ctx->grid, prediction_ctx->z) != SLWR_STATUS_OK) {
      g_critical("%s: could not write the surface mesh for the generating "
                 "function to file",
                 __PRETTY_FUNCTION__);
      goto on_error;
    }
  }
  params_str = slwr_get_file_contents(cl_options_parameters_json());
  if (!params_str) {
    g_critical("%s: could not read network parameters from %s",
               __PRETTY_FUNCTION__, cl_options_parameters_json());
    goto on_error;
  }
  if (io_utils_write_network_parameter_str(paths, params_str) !=
      SLWR_STATUS_OK) {
    goto on_error;
  }

  for (gsize i = 1; i <= cl_options_independent_training_runs(); ++i) {
    if (io_utils_change_run_number_directory(paths, i) != SLWR_STATUS_OK) {
      g_critical("%s: could not switch to the directory for run number %lu",
                 __PRETTY_FUNCTION__, i);
      goto on_error;
    }
    training_dataset = generate_evaluation_dataset(
        generator, cl_options_training_dataset_size());
    if (!training_dataset) {
      g_critical(
          "%s: could not generate a training dataset from %s for run %lu",
          __PRETTY_FUNCTION__, cl_options_generate_from(), i);
      goto on_error;
    }
    ctx = train(ws, training_dataset, prediction_ctx, paths, params_str);
    if (!ctx) {
      g_critical("%s: could not train the network for run %lu",
                 __PRETTY_FUNCTION__, i);
      goto on_error;
    }
    y_pred = predict(ws, ctx, prediction_ctx, &mae);
    if (!y_pred) {
      g_critical("%s: could not predict from the network trained at run %lu",
                 __PRETTY_FUNCTION__, i);
      goto on_error;
    }
    take_final_snapshot(ctx, training_dataset, prediction_ctx, &mae, paths);
    print_mae_for_run_number(&mae, i);
    slwr_vector_free(y_pred);
    slwr_network_context_free(ctx);
    evaluation_dataset_free(training_dataset);
  }

  g_free(params_str);
  results_filepaths_free(paths);
  slwr_workspace_free(ws);
  return SLWR_STATUS_OK;

on_error:
  if (y_pred) {
    slwr_vector_free(y_pred);
  }
  if (ctx) {
    slwr_network_context_free(ctx);
  }
  if (training_dataset) {
    evaluation_dataset_free(training_dataset);
  }
  if (params_str) {
    g_free(params_str);
  }
  if (paths) {
    results_filepaths_free(paths);
  }
  if (ws) {
    slwr_workspace_free(ws);
  }
  return SLWR_STATUS_ERROR;
}

int main(int argc, char *argv[]) {
  if (!cl_options_parse(argc, argv)) {
    return EXIT_FAILURE;
  }
  if (!cl_options_validate()) {
    return EXIT_FAILURE;
  }

  slwr_synthetic_data_generator *generator = NULL;
  prediction_context *prediction_ctx = NULL;
  slwr_status status;

  generator =
      slwr_synthetic_data_generator_from_name(cl_options_generate_from());
  if (!generator) {
    g_critical("could not obtain a synthetic data generator for %s",
               cl_options_generate_from());
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  /* Prediction data, which is encoded either as a grid for plottable functions
   * or as a dataset, is generated only once. The same prediction context is
   * reused in all runs. */
  prediction_ctx = prediction_context_alloc(generator);
  if (!prediction_ctx) {
    g_critical("could not allocate a prediction context for %s",
               cl_options_generate_from());
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  status = perform_independent_training_runs(generator, prediction_ctx);

end:
  if (prediction_ctx) {
    prediction_context_free(prediction_ctx);
  }
  if (generator) {
    slwr_synthetic_data_free(generator);
  }
  return status == SLWR_STATUS_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
