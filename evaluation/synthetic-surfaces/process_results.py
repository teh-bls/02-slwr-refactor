#!/usr/bin/env python3

import argparse
import json
import math
import numpy as np
import os
import sys

from typing import Dict, List, Union

import matplotlib.pyplot as plt

from process_results_utils import print_error
import process_results_utils as utils
import slwr.utils


def _parse_options() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--results-directory', type=str, required=True, help='Results directory')
    return parser.parse_args()


def _write_mae_graph(plt_ctx: Dict, directory: str, snapshot_metadata: List[Dict]):
    def mae_from_file(mfn: str) -> float:
        try:
            with open(mfn) as f:
                contents = json.load(f)
                return contents['mae']
        except (FileNotFoundError, json.JSONDecodeError, KeyError):
            return math.nan

    figure, axes = plt_ctx['figure'], plt_ctx['axes']
    figure.clear()
    figure.add_axes(plt_ctx['axes'])

    axes.clear()
    axes.set_aspect('auto')
    y = [mae_from_file(os.path.join(directory, i)) for i in list(map(lambda i: i['mae'], snapshot_metadata))]
    axes.plot(list(map(lambda i: i['int-prefix'], snapshot_metadata)), y)
    plt.savefig(os.path.join(directory, 'maes.png'))


def _write_population_size_graph(plt_ctx: Dict, directory: str,
                                 snapshot_metadata: List[Dict]):
    def population_size_from_file(pfn: str) -> int:
        try:
            with open(pfn) as f:
                contents = json.load(f)
                return len(contents[0]['receptive-fields'])
        except (FileNotFoundError, json.JSONDecodeError, KeyError):
            return 0

    figure, axes = plt_ctx['figure'], plt_ctx['axes']

    figure.clear()
    figure.add_axes(plt_ctx['axes'])

    axes.clear()
    axes.set_aspect('auto')
    y = [population_size_from_file(os.path.join(directory, i)) for i
         in list(map(lambda i: i['model-populations'], snapshot_metadata))]
    axes.plot(list(map(lambda i: i['int-prefix'], snapshot_metadata)), y)
    plt.savefig(os.path.join(directory, 'population-sizes.png'))


def _write_mesh_to_file(mesh: Dict, plt_ctx: Dict, filename: str):
    figure = plt_ctx['figure']
    figure.clear()
    try:
        slwr.utils.plot_mesh(figure, np.array(mesh['xx1']), np.array(mesh['xx2']), np.array(mesh['z']))
        plt.savefig(filename)
    except KeyError as e:
        print_error(f'{e}')


def _setup_figure_for_population_plot(plt_ctx: Dict, ):
    figure, axes, xlims = plt_ctx['figure'], plt_ctx['axes'], plt_ctx['xlims']

    figure.clear()
    figure.add_axes(axes)

    axes.clear()
    axes.set_aspect('equal', adjustable='box')
    axes.set_xlim([xlims[0] - 1.0, xlims[1] + 1.0])
    axes.set_ylim([xlims[2] - 1.0, xlims[3] + 1.0])
    axes.set_xticks([xlims[0], 0.0, xlims[1]], fontsize=12)
    axes.set_xlabel('$x_1$', fontsize=14)
    axes.set_yticks([xlims[2], 0.0, xlims[3]], fontsize=12)
    axes.set_ylabel('$x_2$', fontsize=14)


def _write_population_superimposed_on_error_to_file(population: Dict, mesh: Dict, plt_ctx: Dict, filename: str):
    _setup_figure_for_population_plot(plt_ctx)
    axes, xlims = plt_ctx['axes'], plt_ctx['xlims']
    slwr.utils.plot_receptive_field_population(axes, population)
    im = axes.imshow(np.array(mesh['z']), alpha=0.8, cmap=plt.cm.Greys, extent=xlims)
    cb = plt.colorbar(im)
    plt.savefig(filename)
    cb.remove()


def _write_population_plot(pop: Dict, plt_ctx: Dict, filename: str):
    _setup_figure_for_population_plot(plt_ctx)
    axes, xlims = plt_ctx['axes'], plt_ctx['xlims']
    slwr.utils.plot_receptive_field_population(axes, pop)
    plt.savefig(filename)


def _load_dict_from_json(filename: str) -> Union[Dict, None]:
    try:
        with open(filename) as f:
            return json.load(f)
    except FileNotFoundError as e:
        print_error(f'{e.strerror}')
    except json.JSONDecodeError as e:
        print_error(f'cannot load JSON from {filename}: {e}')
    return None


def _process_snapshot_directory(directory: str, plt_ctx: Dict):
    snapshot_metadata = utils.extract_sorted_snapshot_filenames(directory)
    if not snapshot_metadata:
        print_error(f'could not extract snapshots in {directory}')
        return
    _write_mae_graph(plt_ctx, directory, snapshot_metadata)
    _write_population_size_graph(plt_ctx, directory, snapshot_metadata)
    for i in snapshot_metadata:
        populations = _load_dict_from_json(os.path.join(directory, i['model-populations']))
        if not populations:
            continue
        _write_population_plot(populations[0], plt_ctx,
                               os.path.join(directory, f'{i["str-prefix"]}-model-populations.png'))
        prediction_mesh = _load_dict_from_json(os.path.join(directory, i['prediction-mesh']))
        if not prediction_mesh:
            continue
        _write_mesh_to_file(prediction_mesh, plt_ctx, os.path.join(directory, f'{i["str-prefix"]}-prediction-mesh.png'))
        error_mesh = _load_dict_from_json(os.path.join(directory, i['error-mesh']))
        if not error_mesh:
            continue
        _write_mesh_to_file(error_mesh, plt_ctx, os.path.join(directory, f'{i["str-prefix"]}-error-mesh.png'))
        _write_population_superimposed_on_error_to_file(populations[0], error_mesh, plt_ctx, os.path.join(directory,
                                                                                                          f'{i["str-prefix"]}-population-superimposed-on-error.png'))


def _process_run_directory(directory: str, plt_ctx: Dict):
    plt_ctx['xlims'] = utils.determine_xlims_from_available_training_dataset(directory)
    if not plt_ctx['xlims']:
        print_error('could not determine the input space limits')
        return
    populations = _load_dict_from_json(os.path.join(directory, 'final-model-populations.json'))
    _write_population_plot(populations[0], plt_ctx, os.path.join(directory, 'final-model-population.png'))
    _process_snapshot_directory(os.path.join(directory, 'snapshots'), plt_ctx)


def _initialize_plotting() -> Dict:
    plt.rc('text', usetex=True)
    fig, (ax) = plt.subplots()
    return {'figure': fig, 'axes': ax, 'xlims': None}


def _main(argv: Dict):
    results_directory = argv['results_directory']
    if not os.path.exists(results_directory):
        print_error(f'the results directory {results_directory} does not exist')
        sys.exit(-1)
    params = utils.load_network_parameters(results_directory)
    if not params:
        print_error(f'could not load the network parameters from results directory {results_directory}')
        sys.exit(-1)
    try:
        if params['n-in'] != 2 or params['n-out'] != 1:
            print_error(
                f'processing results for networks with input dimensions different than 2 or output dimensions different than 1 is not supported')
            sys.exit(-1)
    except KeyError as e:
        print_error(f'could not find the required key in the network parameters JSON: {e}')
        sys.exit(-1)

    plt_ctx = _initialize_plotting()
    # Assume that all subdirectories in the results directory are run directories
    for f in os.listdir(results_directory):
        run_directory = os.path.join(results_directory, f)
        if os.path.isdir(run_directory):  # Skip regular files
            print(f'processing run directory "{run_directory}" ... ', end='')
            sys.stdout.flush()
            _process_run_directory(run_directory, plt_ctx)
            print('done')


if __name__ == '__main__':
    _main(vars(_parse_options()))
