#pragma once

#include <glib.h>

gboolean cl_options_parse(int argc, char *argv[]);
gboolean cl_options_validate();

const gchar *cl_options_parameters_json();
const gchar *cl_options_generate_from();
gsize cl_options_training_dataset_size();
gsize cl_options_independent_training_runs();
gsize cl_options_snapshot_interval();
