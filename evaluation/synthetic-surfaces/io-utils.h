#pragma once

#include "evaluation/common/evaluation-dataset.h"
#include "mesh-grid-2d.h"
#include "prediction.h"

#include <slwr/slwr.h>

#include <glib.h>

typedef struct results_filepaths results_filepaths;

results_filepaths *
io_utils_create_results_base_directory(const gchar *generating_function_name);
slwr_status io_utils_change_run_number_directory(results_filepaths *paths,
                                                 gsize run_number);

void results_filepaths_free(results_filepaths *paths);

const gchar *results_filepaths_base_directory(const results_filepaths *paths);

slwr_status
io_utils_write_training_dataset(const results_filepaths *paths,
                                const evaluation_dataset *training_dataset);
slwr_status
io_utils_write_prediction_dataset(const results_filepaths *paths,
                                  const evaluation_dataset *prediction_dataset);
slwr_status io_utils_write_network_parameter_str(const results_filepaths *paths,
                                                 const gchar *params);

slwr_status
io_utils_write_generating_function_mesh(const results_filepaths *paths,
                                        const mesh_grid_2d *grid,
                                        const slwr_matrix *z);

slwr_status
io_utils_write_nth_populations_snapshot(const results_filepaths *paths,
                                        const GPtrArray *populations, gsize n);
slwr_status io_utils_write_final_populations(const results_filepaths *paths,
                                             const GPtrArray *populations);

slwr_status io_utils_write_nth_mae_snapshot(const results_filepaths *paths,
                                            const slwr_vector *mae, gsize n);
slwr_status io_utils_write_final_mae(const results_filepaths *paths,
                                     const slwr_vector *mae);

slwr_status io_utils_write_nth_prediction_mesh_snapshot(
    const results_filepaths *paths, const prediction_context *prediction_ctx,
    const slwr_vector *y_pred, gsize n);

slwr_status
io_utils_write_nth_error_mesh_snapshot(const results_filepaths *paths,
                                       const prediction_context *prediction_ctx,
                                       const slwr_vector *y_pred, gsize n);
