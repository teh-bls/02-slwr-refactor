#include "cl-options.h"

#include <slwr/utils/slwr-synthetic-data.h>

static gchar *network_parameters_filename = NULL;
static gchar *generate_from = NULL;
static gint training_dataset_size = 50000;
static gint independent_training_runs = 1;
static gint snapshot_interval = 0;

static gchar *generate_from_option_description() {
  gchar *fns = slwr_available_generator_functions_to_str();
  gchar *description =
      g_strdup_printf("The name of the function to generate the dataset from; "
                      "available generator functions are: %s",
                      fns);
  g_free(fns);
  return description;
}

gboolean cl_options_parse(int argc, char *argv[]) {
  gchar *generate_from_description = generate_from_option_description();
  const GOptionEntry option_entries[] = {
      {"parameters-json", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
       &network_parameters_filename, "Network parameters JSON file", NULL},
      {"generate-from", 'g', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
       &generate_from, generate_from_description, NULL},
      {"training-dataset-size", 't', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
       &training_dataset_size, "Training dataset size", NULL},
      {"independent-training-runs", 'i', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
       &independent_training_runs, "Number of independent training runs", NULL},
      {"snapshot-interval", 's', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
       &snapshot_interval, "Interval at which training snapshots are taken",
       NULL},
      {NULL}};

  GOptionContext *context = g_option_context_new(NULL);
  g_option_context_add_main_entries(context, option_entries, NULL);

  gboolean result = TRUE;
  GError *error = NULL;
  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    g_critical("%s: %s", __PRETTY_FUNCTION__, error->message);
    result = FALSE;
    g_error_free(error);
  }
  g_option_context_free(context);
  g_free(generate_from_description);
  return result;
}

gboolean cl_options_validate() {
  if (!network_parameters_filename) {
    g_critical("%s: a network parameter JSON file must be provided",
               __PRETTY_FUNCTION__);
    return FALSE;
  }
  if (!generate_from) {
    g_critical("%s: the name of a function to generate the training dataset "
               "from must be provided",
               __PRETTY_FUNCTION__);
    return FALSE;
  }
  if (!slwr_valid_generator_function_name(generate_from)) {
    gchar *fns = slwr_available_generator_functions_to_str();
    g_critical("%s: generating function with name %s is not available; "
               "available generator functions are: %s",
               __PRETTY_FUNCTION__, generate_from, fns);
    g_free(fns);
    return FALSE;
  }
  return TRUE;
}

const gchar *cl_options_parameters_json() {
  return network_parameters_filename;
}

const gchar *cl_options_generate_from() { return generate_from; }

gsize cl_options_training_dataset_size() { return training_dataset_size; }

gsize cl_options_independent_training_runs() {
  return independent_training_runs;
}

gsize cl_options_snapshot_interval() { return snapshot_interval; }
