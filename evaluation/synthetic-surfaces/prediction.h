#pragma once

#include "mesh-grid-2d.h"

#include "evaluation/common/evaluation-dataset.h"

#include <slwr/slwr.h>

typedef struct {
  evaluation_dataset *dataset;
  mesh_grid_2d *grid;
  slwr_matrix *z;
} prediction_context;

prediction_context *
prediction_context_alloc(const slwr_synthetic_data_generator *generator);
void prediction_context_free(prediction_context *ctx);

slwr_vector *predict(slwr_workspace *ws,
                     const slwr_network_context *network_ctx,
                     const prediction_context *prediction_ctx,
                     slwr_vector *mae);
