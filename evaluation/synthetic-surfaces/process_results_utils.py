from typing import Dict, List, Union
import json
import math
import os
import sys


def print_error(message: str):
    print(f'error: {message}', file=sys.stderr)


def extract_sorted_snapshot_filenames(directory: str) -> List[Dict]:
    """
    Extracts a list of dictionaries representing the names of snapshots of model population and MAE files and their
    prefixes found in the given directory. The entries in each dictionary in the list are:
        "int-prefix" : the numerical snapshot filename prefix,
        "str-prefix" : the actual string snapshot filename prefix,
        "model-populations" : the model populations snapshot filename,
        "mae" : the MAE snapshot filename,
        "prediction-mesh": the prediction mesh filename, and
        "error-mesh": the error mesh filename.

    The function expects that each snapshot of model populations has a corresponding MAE snapshots.

    On error, an empty list is returned.

    :param directory: The directory from which the list is extracted
    :return: The list sorted by the numerical prefix or an empty list on error
    """
    try:
        ls = os.listdir(directory)
        suffix = {'pops': '-model-populations.json', 'mae': '-mae.json', 'prediction': '-prediction-mesh.json',
                  'error': '-error-mesh.json'}
        pops = list(filter(lambda f: f.endswith(suffix['pops']), ls))
        if len(pops) == 0:
            print_error(f'no snapshots of model populations found in {directory}')
            return []
        maes = list(filter(lambda f: f.endswith(suffix['mae']), ls))
        if len(pops) == 0:
            print_error(f'no snapshots of MAEs found in {directory}')
            return []
        prediction_meshes = list(filter(lambda f: f.endswith(suffix['prediction']), ls))
        if len(prediction_meshes) == 0:
            print_error(f'no snapshots of prediction meshes found in {directory}')
            return []
        error_meshes = list(filter(lambda f: f.endswith(suffix['error']), ls))
        if len(error_meshes) == 0:
            print_error(f'no snapshots of error meshes found in {directory}')
            return []
        if len(pops) != len(maes) != len(prediction_meshes) != len(error_meshes):
            print_error(f'the numbers of snapshots of in {directory} do not match')
            return []
        result = []
        for pop in pops:
            prefix = pop.removesuffix(suffix['pops'])
            if not prefix.isnumeric():
                print_error(f'expected a numeric prefix for filename {pop} in {directory}')
                return []
            mae = list(filter(lambda m: m.startswith(prefix), maes))
            if len(mae) != 1:
                print_error(f'cannot find a matching MAE snapshot for prefix {prefix} in {directory}')
                return []
            prediction_mesh = list(filter(lambda m: m.startswith(prefix), prediction_meshes))
            if len(prediction_mesh) != 1:
                print_error(f'cannot find a matching prediction mesh snapshot for prefix {prefix} in {directory}')
                return []
            error_mesh = list(filter(lambda m: m.startswith(prefix), error_meshes))
            if len(error_mesh) != 1:
                print_error(f'cannot find a matching error mesh snapshot for prefix {prefix} in {directory}')
                return []
            result.append({'int-prefix': int(prefix), 'str-prefix': prefix, 'model-populations': pop, 'mae': mae[0],
                           'prediction-mesh': prediction_mesh[0], 'error-mesh': error_mesh[0]})
        result.sort(key=lambda i: i['int-prefix'])
        return result
    except FileNotFoundError as e:
        print_error(f'{e.strerror}')
        return []


def load_network_parameters(directory: str) -> Dict:
    """
    Loads and returns the network parameters from a file, assumed to be named "network-parameters.json", in the given
    directory.

    On error, an empty dictionary is returned.

    :param directory: The directory containing the network parameters file
    :return: The loaded network parameters or an empty dictionary on error
    """
    try:
        with open(os.path.join(directory, 'network-parameters.json')) as f:
            return json.load(f)
    except FileNotFoundError as e:
        print_error(f'could not load network parameters from {directory}: {e.strerror}')
        return {}
    except json.JSONDecodeError as e:
        print_error(f'could not decode the network parameters JSON: {e}')
        return {}


def determine_xlims_from_available_training_dataset(directory: str) -> List[float]:
    """
    Determines the input space (x-dimension) limits from the training dataset statistics found in the given run
    directory. The limits are returned a four-element [x1-min, x2-max, x2-min, x2-max] list.

    On error, an empty list is returned.

    :param directory: The directory containing the training dataset
    :return: The input space limits as a list or an empty list on error
    """
    try:
        with open(os.path.join(directory, 'training-dataset.json')) as f:
            dataset = json.load(f)
            return [
                math.floor(dataset['statistics']['data-in-min'][0][0]),
                math.ceil(dataset['statistics']['data-in-max'][0][0]),
                math.floor(dataset['statistics']['data-in-min'][1][0]),
                math.ceil(dataset['statistics']['data-in-max'][1][0])
            ]
    except FileNotFoundError as e:
        print_error(f'could not find the training dataset in {directory}: {e.strerror}')
        return []
    except json.JSONDecodeError as e:
        print_error(f': could not decode the training dataset JSON for run in {directory}: {e}')
        return []
    except KeyError as e:
        print_error(f'training dataset JSON in {directory} malformed; KeyError: {e}')
        return []
