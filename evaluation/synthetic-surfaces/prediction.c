#include "prediction.h"

#include <math.h>

static prediction_context *prediction_context_alloc_with_dataset(
    const slwr_synthetic_data_generator *generator, gsize dataset_size) {
  g_return_val_if_fail(generator != NULL, NULL);

  prediction_context *ctx = g_malloc0(sizeof(prediction_context));

  ctx->dataset = generate_evaluation_dataset(generator, dataset_size);
  if (!ctx->dataset) {
    goto on_error;
  }
  return ctx;

on_error:
  prediction_context_free(ctx);
  return NULL;
}

static prediction_context *prediction_context_alloc_with_grid(
    const slwr_synthetic_data_generator *generator, gsize grid_size) {
  g_return_val_if_fail(generator != NULL, NULL);

  const gsize n_in = generator->n_in;
  const gsize n_out = generator->n_out;

  prediction_context *ctx = g_malloc0(sizeof(prediction_context));
  if (!ctx) {
    goto on_error;
  }
  ctx->dataset = evaluation_dataset_alloc(generator->name, n_in, n_out,
                                          grid_size * grid_size);
  if (!ctx->dataset) {
    goto on_error;
  }
  ctx->grid = mesh_grid_2d_alloc(grid_size);
  if (!ctx->grid) {
    goto on_error;
  }
  if (mesh_grid_2d_initialize(generator, ctx->grid) != SLWR_STATUS_OK) {
    goto on_error;
  }
  ctx->z = slwr_matrix_alloc(grid_size, grid_size);
  if (!ctx->z) {
    goto on_error;
  }
  gdouble x_data[2];
  slwr_vector x = {.size = 2, .data = x_data};
  slwr_vector y = {.size = 1, .data = NULL};
  for (gsize i = 0; i < grid_size * grid_size; ++i) {
    x.data[0] = ctx->grid->xx1->data[i];
    x.data[1] = ctx->grid->xx2->data[i];
    y.data = ctx->z->data + i;
    if (generator->generate(&x, &y) != SLWR_STATUS_OK) {
      goto on_error;
    }
    ctx->dataset->data->data[i * (n_in + n_out)] = x.data[0];
    ctx->dataset->data->data[i * (n_in + n_out) + 1] = x.data[1];
    ctx->dataset->data->data[i * (n_in + n_out) + 2] = y.data[0];
  }
  return ctx;

on_error:
  prediction_context_free(ctx);
  return NULL;
}

static inline gboolean generating_function_is_plottable(
    const slwr_synthetic_data_generator *generator) {
  return generator->n_in == 2 && generator->n_out == 1;
}

prediction_context *
prediction_context_alloc(const slwr_synthetic_data_generator *generator) {
  g_return_val_if_fail(generator != NULL, NULL);
  return generating_function_is_plottable(generator)
             ? prediction_context_alloc_with_grid(generator, 40)
             : prediction_context_alloc_with_dataset(generator, 20000);
}

void prediction_context_free(prediction_context *ctx) {
  if (!ctx) {
    return;
  }
  if (ctx->z) {
    slwr_matrix_free(ctx->z);
  }
  if (ctx->grid) {
    mesh_grid_2d_free(ctx->grid);
  }
  if (ctx->dataset) {
    evaluation_dataset_free(ctx->dataset);
  }
  g_free(ctx);
}

static inline void clear_double_array(gdouble a[], gsize n) {
  for (gsize i = 0; i < n; ++i) {
    a[i] = 0.0;
  }
}

/**
 * Uses the trained network context on the existing prediction context and
 * returns the MAE.
 *
 * @param ws The slwr workspace
 * @param network_ctx The network context
 * @param prediction_ctx The prediction context
 * @param mae (inout) the MAE
 * @return (nullable) (transfer full) An slwr_vector of predicted values, or
 * NULL on error.
 */
slwr_vector *predict(slwr_workspace *ws,
                     const slwr_network_context *network_ctx,
                     const prediction_context *prediction_ctx,
                     slwr_vector *mae) {
  g_return_val_if_fail(ws != NULL, NULL);
  g_return_val_if_fail(network_ctx != NULL, NULL);
  g_return_val_if_fail(prediction_ctx != NULL, NULL);
  g_return_val_if_fail(mae != NULL, NULL);

  const evaluation_dataset *dataset = prediction_ctx->dataset;
  g_return_val_if_fail(dataset != NULL, NULL);

  const gsize n_in = dataset->n_in;
  const gsize n_out = dataset->n_out;
  g_return_val_if_fail(mae->size == n_out, NULL);

  slwr_vector x = {.size = n_in, .data = dataset->data->data};
  slwr_vector y = {.size = n_out, .data = dataset->data->data + n_in};
  slwr_vector *y_pred =
      slwr_vector_alloc(n_out * prediction_ctx->dataset->size);
  slwr_vector y_pred_v = {.size = n_out, .data = y_pred->data};
  /* TODO: can absolute_error_sum be a vector? There is an slwr_vector
   * difference function (or there should be) */
  gdouble absolute_error_sum[n_out];
  clear_double_array(absolute_error_sum, n_out);
  for (gsize i = 0; i < dataset->size; ++i) {
    if (slwr_network_predict(ws, network_ctx, &x, &y_pred_v) !=
        SLWR_STATUS_OK) {
      goto on_error;
    }
    for (gsize j = 0; j < n_out; ++j) {
      absolute_error_sum[j] += fabs(y.data[j] - y_pred_v.data[j]);
    }
    x.data += n_in + n_out;
    y.data = x.data + n_in;
    y_pred_v.data += n_out;
  }
  for (gsize i = 0; i < n_out; ++i) {
    mae->data[i] = absolute_error_sum[i] / (double)dataset->size;
  }
  return y_pred;

on_error:
  slwr_vector_free(y_pred);
  return NULL;
}
