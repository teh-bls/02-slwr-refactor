#include "io-utils.h"

#include <gio/gio.h>

#include <math.h>

struct results_filepaths {
  gchar *base_dir;
  gchar *run_number_dir;
  gchar *snapshots_dir;
};

/**
 * Builds a filename in the current run number directory.
 *
 * @param paths The current paths
 * @param filename The filename to put in the run number directory
 * @return (transfer full) The path to filename
 */
static gchar *
build_filename_in_run_number_directory(const results_filepaths *paths,
                                       const gchar *filename) {
  g_return_val_if_fail(paths != NULL, NULL);
  g_return_val_if_fail(filename != NULL, NULL);
  g_return_val_if_fail(paths->base_dir != NULL, NULL);
  g_return_val_if_fail(paths->run_number_dir != NULL, NULL);

  return g_build_filename(paths->base_dir, paths->run_number_dir, filename,
                          NULL);
}

/**
 * Builds a filename in the base directory.
 *
 * @param paths The current paths
 * @param filename The filename to put in the base directory
 * @return (transfer full) The path to filename
 */
static gchar *build_filename_in_base_directory(const results_filepaths *paths,
                                               const gchar *filename) {
  g_return_val_if_fail(paths != NULL, NULL);
  g_return_val_if_fail(filename != NULL, NULL);
  g_return_val_if_fail(paths->base_dir != NULL, NULL);

  return g_build_filename(paths->base_dir, filename, NULL);
}

/**
 * Builds a filename in the current model population snapshots directory.
 *
 * @param paths The current paths
 * @param filename The filename to put in the snapshots directory
 * @return (transfer full) The path to filename
 */
static gchar *build_filename_in_model_population_snapshots_directory(
    const results_filepaths *paths, const gchar *filename) {
  g_return_val_if_fail(paths != NULL, NULL);
  g_return_val_if_fail(filename != NULL, NULL);
  g_return_val_if_fail(paths->base_dir != NULL, NULL);
  g_return_val_if_fail(paths->run_number_dir != NULL, NULL);
  g_return_val_if_fail(paths->snapshots_dir != NULL, NULL);

  return g_build_filename(paths->base_dir, paths->run_number_dir,
                          paths->snapshots_dir, filename, NULL);
}

/**
 * Writes a dataset to the file with the given filename.
 *
 * @param filename The filename of the file to write to
 * @param dataset The dataset to write
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise
 */
static slwr_status write_dataset_to_file(const gchar *filename,
                                         const evaluation_dataset *dataset) {
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(dataset != NULL, SLWR_STATUS_ERROR);

  json_t *dataset_obj = evaluation_dataset_to_json(dataset);
  if (!dataset_obj) {
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = slwr_write_compact_json_to_file(dataset_obj, filename);
  json_decref(dataset_obj);
  return status;
}

/**
 * Creates a directory in the given path. If the directory exists, it omits its
 * creation and returns successfully.
 *
 * @param path The path to the directory to create.
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status create_directory(const gchar *path) {
  g_return_val_if_fail(path != NULL, SLWR_STATUS_ERROR);

  GFile *dir = NULL;
  GError *error = NULL;
  slwr_status status = SLWR_STATUS_OK;

  dir = g_file_new_for_path(path);
  if (!g_file_make_directory(dir, NULL, &error) &&
      error->code != G_IO_ERROR_EXISTS) {
    g_critical("%s: could not create directory for path %s: %s (code %d)",
               __PRETTY_FUNCTION__, path, error->message, error->code);
    status = SLWR_STATUS_ERROR;
  }

  if (error) {
    g_error_free(error);
  }
  if (dir) {
    g_object_unref(dir);
  }
  return status;
}

/**
 * Writes a MAE to the file with the given filename after converting it to a
 * JSON object representation.
 *
 * @param mae The MAE to write
 * @param filename The filename of the file to write to
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise
 */
static slwr_status write_mae_to_file(double mae, const gchar *filename) {
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);

  json_t *mae_obj = NULL;
  json_t *mae_real = NULL;
  slwr_status status;

  mae_obj = json_object();
  if (!mae_obj) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  mae_real = json_real(mae);
  if (!mae_real) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if (json_object_set(mae_obj, "mae", mae_real) != 0) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  status = slwr_write_compact_json_to_file(mae_obj, filename);

end:
  if (mae_real) {
    json_decref(mae_real);
  }
  if (mae_obj) {
    json_decref(mae_obj);
  }
  return status;
}

/**
 * Writes model populations to the file with the given filename.
 *
 * @param populations The model populations to write
 * @param filename The filename of the file to write to
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status write_model_populations_to_file(const GPtrArray *populations,
                                                   const gchar *filename) {
  g_return_val_if_fail(populations != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);

  json_t *populations_array = NULL;
  json_t *population_obj = NULL;
  slwr_model_population *pop = NULL;
  slwr_status status;

  populations_array = json_array();
  if (!populations_array) {
    g_critical("%s: could not create an array for populations",
               __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  for (gsize i = 0; i < populations->len; ++i) {
    pop = (slwr_model_population *)g_ptr_array_index(populations, i);
    if (!pop) {
      g_critical(
          "%s: could not obtain the model population for output dimension %lu",
          __PRETTY_FUNCTION__, i);
      status = SLWR_STATUS_ERROR;
      goto end;
    }
    population_obj = slwr_model_population_to_json(pop);
    if (!population_obj) {
      g_critical(
          "%s: could not get the model population at output dimension %lu",
          __PRETTY_FUNCTION__, i);
      status = SLWR_STATUS_ERROR;
      goto end;
    }
    if (json_array_append(populations_array, population_obj) != 0) {
      g_critical(
          "%s: could not append the model population at output dimension "
          "%lu to the populations array",
          __PRETTY_FUNCTION__, i);
      status = SLWR_STATUS_ERROR;
      goto end;
    }
    json_decref(population_obj);
    population_obj = NULL;
  }
  if ((status = slwr_write_compact_json_to_file(populations_array, filename)) !=
      SLWR_STATUS_OK) {
    g_critical("could not write the model populations array to %s", filename);
    goto end;
  }

end:
  if (populations_array) {
    json_decref(populations_array);
  }
  if (population_obj) {
    json_decref(population_obj);
  }
  return status;
}

/**
 * Writes a 2d mesh grid to the file with the given filename.
 *
 * @param grid The mesh grid
 * @param z The output dimension grid
 * @param filename The filename of the file to write to
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
static slwr_status write_mesh_grid_2d_to_file(const mesh_grid_2d *grid,
                                              const slwr_matrix *z,
                                              const gchar *filename) {
  g_return_val_if_fail(grid != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(z != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);

  json_t *mesh_grid = NULL;
  json_t *xx1_obj = NULL;
  json_t *xx2_obj = NULL;
  json_t *z_obj = NULL;
  slwr_status status;

  xx1_obj = slwr_matrix_to_json(grid->xx1);
  if (!xx1_obj) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  xx2_obj = slwr_matrix_to_json(grid->xx2);
  if (!xx2_obj) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  z_obj = slwr_matrix_to_json(z);
  if (!z_obj) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  mesh_grid = json_object();
  if (!mesh_grid) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if (json_object_set(mesh_grid, "xx1", xx1_obj) != 0) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if (json_object_set(mesh_grid, "xx2", xx2_obj) != 0) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if (json_object_set(mesh_grid, "z", z_obj) != 0) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if ((status = slwr_write_compact_json_to_file(mesh_grid, filename)) !=
      SLWR_STATUS_OK) {
    goto end;
  }

end:
  if (z_obj) {
    json_decref(z_obj);
  }
  if (xx2_obj) {
    json_decref(xx2_obj);
  }
  if (xx1_obj) {
    json_decref(xx1_obj);
  }
  if (mesh_grid) {
    json_decref(mesh_grid);
  }
  return status;
}

/**
 * Creates the results directory from the given generating function name and
 * returns the resulting paths.
 *
 * @param generating_function_name The name of the data generating function
 * @return (nullable) (transfer full) The paths to results on success, NULL
 * on error.
 */
results_filepaths *
io_utils_create_results_base_directory(const gchar *generating_function_name) {
  gchar *timestamp = slwr_time_now_as_str();
  if (!timestamp) {
    return NULL;
  }
  results_filepaths *paths =
      (results_filepaths *)g_malloc0(sizeof(results_filepaths));
  if (!paths) {
    return NULL;
  }
  paths->snapshots_dir = g_strdup_printf("snapshots");
  if (!paths->snapshots_dir) {
    goto on_error;
  }

  paths->base_dir =
      g_strdup_printf("%s-results-%s", generating_function_name, timestamp);
  if (!paths->base_dir) {
    goto on_error;
  }
  if (create_directory(paths->base_dir) != SLWR_STATUS_OK) {
    goto on_error;
  }
  g_free(timestamp);
  return paths;

on_error:
  results_filepaths_free(paths);
  return NULL;
}

/**
 * Changes the current run number directory to the given run.
 *
 * @param paths The paths to change
 * @param run_number The desired run number
 * @return SLWR_STATUS_OK on success, an slwr_status indicating and error
 * otherwise.
 */
slwr_status io_utils_change_run_number_directory(results_filepaths *paths,
                                                 gsize run_number) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);

  gchar *run_number_dir = NULL;
  gchar *path_from_base_dir = NULL;

  run_number_dir = g_strdup_printf("%04lu", run_number);
  if (!run_number_dir) {
    goto on_error;
  }

  path_from_base_dir = g_build_filename(paths->base_dir, run_number_dir, NULL);
  if (create_directory(path_from_base_dir) != SLWR_STATUS_OK) {
    goto on_error;
  }
  g_free(path_from_base_dir);

  path_from_base_dir = g_build_filename(paths->base_dir, run_number_dir,
                                        paths->snapshots_dir, NULL);
  if (create_directory(path_from_base_dir) != SLWR_STATUS_OK) {
    goto on_error;
  }
  g_free(path_from_base_dir);

  /* Set paths->run_number_dir to run_number_dir as the final step, when no more
   * errors can occur
   */
  if (paths->run_number_dir) {
    g_free(paths->run_number_dir);
  }
  paths->run_number_dir = run_number_dir;

  return SLWR_STATUS_OK;

on_error:
  if (path_from_base_dir) {
    g_free(path_from_base_dir);
  }
  if (run_number_dir) {
    g_free(run_number_dir);
  }
  return SLWR_STATUS_ERROR;
}

/**
 * Frees the results_filepaths object.
 *
 * @param paths (nullable) The paths to free
 */
void results_filepaths_free(results_filepaths *paths) {
  if (!paths) {
    return;
  }
  if (paths->snapshots_dir) {
    g_free(paths->snapshots_dir);
  }
  if (paths->run_number_dir) {
    g_free(paths->run_number_dir);
  }
  if (paths->base_dir) {
    g_free(paths->base_dir);
  }
  g_free(paths);
}

/**
 * Returns the current base directory.
 *
 * @param paths The current paths
 * @return (nullable) (transfer none) The base directory
 */
const gchar *results_filepaths_base_directory(const results_filepaths *paths) {
  g_return_val_if_fail(paths != NULL, NULL);

  return paths->base_dir;
}

/**
 * Writes the training dataset to the location determined by the given paths.
 *
 * @param paths The current paths
 * @param training_dataset The training dataset to write
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status
io_utils_write_training_dataset(const results_filepaths *paths,
                                const evaluation_dataset *training_dataset) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(training_dataset != NULL, SLWR_STATUS_ERROR);

  gchar *filename =
      build_filename_in_run_number_directory(paths, "training-dataset.json");
  if (!filename) {
    g_critical("%s: could not build training dataset filename",
               __PRETTY_FUNCTION__);
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = write_dataset_to_file(filename, training_dataset);
  g_free(filename);
  return status;
}

/**
 * Writes the prediction dataset to the location determined by the given paths.
 *
 * @param paths The current paths
 * @param prediction_dataset The prediction dataset to write
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status io_utils_write_prediction_dataset(
    const results_filepaths *paths,
    const evaluation_dataset *prediction_dataset) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_dataset != NULL, SLWR_STATUS_ERROR);

  gchar *filename =
      build_filename_in_run_number_directory(paths, "prediction-dataset.json");
  if (!filename) {
    g_critical("%s: could not build prediction dataset filename",
               __PRETTY_FUNCTION__);
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = write_dataset_to_file(filename, prediction_dataset);
  g_free(filename);
  return status;
}

/**
 * Writes the network parameter string to the location determined by the given
 * paths.
 *
 * @param paths The current paths
 * @param params The parameter string to write
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status io_utils_write_network_parameter_str(const results_filepaths *paths,
                                                 const gchar *params) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(params != NULL, SLWR_STATUS_ERROR);

  gchar *filename =
      build_filename_in_base_directory(paths, "network-parameters.json");
  if (!filename) {
    g_critical("%s: could not build network parameters filename",
               __PRETTY_FUNCTION__);
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = slwr_write_str_to_file(params, filename);
  g_free(filename);
  return status;
}

/**
 * Writes a mesh of a 2d generating function described by a grid and z to the
 * location determined by the given paths.
 *
 * @param paths The current paths
 * @param grid The input mesh grid
 * @param z Output dimension grid values
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status
io_utils_write_generating_function_mesh(const results_filepaths *paths,
                                        const mesh_grid_2d *grid,
                                        const slwr_matrix *z) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->base_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(grid != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(z != NULL, SLWR_STATUS_ERROR);

  gchar *filename =
      build_filename_in_base_directory(paths, "generating-function-mesh.json");
  if (!filename) {
    g_critical("%s: could not build the generating function mesh filename",
               __PRETTY_FUNCTION__);
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = write_mesh_grid_2d_to_file(grid, z, filename);
  g_free(filename);
  return status;
}

/**
 * Writes to file the snapshot of the populations after an n number of updates.
 *
 * @param paths The current paths
 * @param populations The populations after n updates
 * @param n The number of updates thus far
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status
io_utils_write_nth_populations_snapshot(const results_filepaths *paths,
                                        const GPtrArray *populations, gsize n) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->base_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->run_number_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->snapshots_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(populations != NULL, SLWR_STATUS_ERROR);

  gchar *filename = NULL;
  gchar *filename_in_snapshots_dir = NULL;
  slwr_status status;

  filename = g_strdup_printf("%07lu-model-populations.json", n);
  if (!filename) {
    g_critical(
        "%s: could not build the model populations filename for snapshot %lu",
        __PRETTY_FUNCTION__, n);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  filename_in_snapshots_dir =
      build_filename_in_model_population_snapshots_directory(paths, filename);
  if (!filename_in_snapshots_dir) {
    g_critical(
        "%s: could not build the model populations file path for snapshot %lu",
        __PRETTY_FUNCTION__, n);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if ((status = write_model_populations_to_file(
           populations, filename_in_snapshots_dir)) != SLWR_STATUS_OK) {
    g_critical("%s: could not write the model populations for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    goto end;
  }

end:
  if (filename_in_snapshots_dir) {
    g_free(filename_in_snapshots_dir);
  }
  if (filename) {
    g_free(filename);
  }
  return status;
}

/**
 * Writes to file the final populations.
 *
 * @param paths The current paths
 * @param populations The final populations
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status io_utils_write_final_populations(const results_filepaths *paths,
                                             const GPtrArray *populations) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->base_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->run_number_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(populations != NULL, SLWR_STATUS_ERROR);

  gchar *filename = build_filename_in_run_number_directory(
      paths, "final-model-populations.json");
  if (!filename) {
    g_critical("%s: could not build the model populations filename",
               __PRETTY_FUNCTION__);
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = write_model_populations_to_file(populations, filename);
  if (status != SLWR_STATUS_OK) {
    g_critical("%s: could not write the final model populations",
               __PRETTY_FUNCTION__);
  }
  g_free(filename);
  return status;
}

/**
 * Writes to file the snapshot of the MAE after an n number of updates.
 *
 * @param paths The current paths
 * @param mae The MAE after n updates
 * @param n The number of updates so far
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status io_utils_write_nth_mae_snapshot(const results_filepaths *paths,
                                            const slwr_vector *mae, gsize n) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->base_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->run_number_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->snapshots_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(mae != NULL, SLWR_STATUS_ERROR);

  gchar *filename = NULL;
  gchar *filename_in_snapshots_dir = NULL;
  slwr_status status;

  filename = g_strdup_printf("%07lu-mae.json", n);
  if (!filename) {
    g_critical("%s: could not build the mae filename for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  filename_in_snapshots_dir =
      build_filename_in_model_population_snapshots_directory(paths, filename);
  if (!filename_in_snapshots_dir) {
    g_critical("%s: could not build the mae file path for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if ((status = write_mae_to_file(mae->data[0], filename_in_snapshots_dir)) !=
      SLWR_STATUS_OK) {
    g_critical("%s: could not write the mae for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    goto end;
  }

end:
  if (filename_in_snapshots_dir) {
    g_free(filename_in_snapshots_dir);
  }
  if (filename) {
    g_free(filename);
  }
  return status;
}

/**
 * Write to file the final MAE.
 *
 * @param paths The current paths
 * @param mae The final mae
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status io_utils_write_final_mae(const results_filepaths *paths,
                                     const slwr_vector *mae) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->base_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->run_number_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(mae != NULL, SLWR_STATUS_ERROR);

  gchar *filename =
      build_filename_in_run_number_directory(paths, "final-mae.json");
  if (!filename) {
    g_critical("%s: could not build the final mae filename",
               __PRETTY_FUNCTION__);
    return SLWR_STATUS_ERROR;
  }
  slwr_status status = write_mae_to_file(mae->data[0], filename);
  if (status != SLWR_STATUS_OK) {
    g_critical("%s: could not write the final mae", __PRETTY_FUNCTION__);
  }
  g_free(filename);
  return status;
}

static slwr_status write_mesh_to_file(const results_filepaths *paths,
                                      const prediction_context *prediction_ctx,
                                      gchar *filename, const slwr_matrix *z) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->base_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->run_number_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(paths->snapshots_dir != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx->grid != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(filename != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(z != NULL, SLWR_STATUS_ERROR);

  slwr_status status;
  gchar *filename_in_snapshots_dir = NULL;
  filename_in_snapshots_dir =
      build_filename_in_model_population_snapshots_directory(paths, filename);
  if (!filename_in_snapshots_dir) {
    g_critical("%s: could not build the prediction mesh file path",
               __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if ((status = write_mesh_grid_2d_to_file(prediction_ctx->grid, z,
                                           filename_in_snapshots_dir)) !=
      SLWR_STATUS_OK) {
    g_critical("%s: could not write the prediction mesh", __PRETTY_FUNCTION__);
    goto end;
  }

end:
  if (filename_in_snapshots_dir) {
    g_free(filename_in_snapshots_dir);
  }
  return status;
}

/**
 * Writes to file the snapshot of the prediction mesh after an n number of
 * updates.
 *
 * @param paths The current paths
 * @param prediction_ctx The current prediction context
 * @param y_pred The current prediction
 * @param n The number of updates so far
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status io_utils_write_nth_prediction_mesh_snapshot(
    const results_filepaths *paths, const prediction_context *prediction_ctx,
    const slwr_vector *y_pred, gsize n) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx->grid != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  gsize grid_size = prediction_ctx->grid->size;
  g_return_val_if_fail(y_pred->size == grid_size * grid_size,
                       SLWR_STATUS_ERROR);

  gchar *filename = NULL;
  slwr_matrix z = {
      .size1 = grid_size, .size2 = grid_size, .data = y_pred->data};
  slwr_status status;

  filename = g_strdup_printf("%07lu-prediction-mesh.json", n);
  if (!filename) {
    g_critical(
        "%s: could not build the prediction mesh filename for snapshot %lu",
        __PRETTY_FUNCTION__, n);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if ((status = write_mesh_to_file(paths, prediction_ctx, filename, &z)) !=
      SLWR_STATUS_OK) {
    g_critical("%s: could not write the prediction mesh for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    goto end;
  }

end:
  if (filename) {
    g_free(filename);
  }
  return status;
}

/**
 * Writes to file the snapshot of the error mesh after an n number of updates.
 *
 * @param paths The current paths
 * @param prediction_ctx The current prediction context
 * @param y_pred The current prediction
 * @param n The number of updates so far
 * @return SLWR_STATUS_OK on success, an slwr_status indicating an error
 * otherwise.
 */
slwr_status
io_utils_write_nth_error_mesh_snapshot(const results_filepaths *paths,
                                       const prediction_context *prediction_ctx,
                                       const slwr_vector *y_pred, gsize n) {
  g_return_val_if_fail(paths != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(prediction_ctx->grid != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(y_pred != NULL, SLWR_STATUS_ERROR);

  gsize grid_size = prediction_ctx->grid->size;
  g_return_val_if_fail(y_pred->size == grid_size * grid_size,
                       SLWR_STATUS_ERROR);

  slwr_status status;
  gchar *filename = NULL;
  slwr_matrix *z = NULL;

  filename = g_strdup_printf("%07lu-error-mesh.json", n);
  if (!filename) {
    g_critical("%s: could not build the error mesh filename for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  z = slwr_matrix_alloc(grid_size, grid_size);
  if (!z) {
    g_critical("%s: could not allocate data for the output dimension",
               __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  memcpy(z->data, y_pred->data, y_pred->size * sizeof(gdouble));
  if ((status = slwr_matrix_sub(z, prediction_ctx->z)) != SLWR_STATUS_OK) {
    g_critical("%s: could not obtain the error mesh", __PRETTY_FUNCTION__);
    goto end;
  }
  for (gsize i = 0; i < z->size1 * z->size2; ++i) {
    z->data[i] = fabs(z->data[i]);
  }
  if ((status = write_mesh_to_file(paths, prediction_ctx, filename, z)) !=
      SLWR_STATUS_OK) {
    g_critical("%s: could not write the error mesh for snapshot %lu",
               __PRETTY_FUNCTION__, n);
    goto end;
  }

end:
  if (z) {
    slwr_matrix_free(z);
  }
  if (filename) {
    g_free(filename);
  }
  return status;
}
