#include "mesh-grid-2d.h"

mesh_grid_2d *mesh_grid_2d_alloc(gsize grid_size) {
  mesh_grid_2d *grid = (mesh_grid_2d *)g_malloc0(sizeof(mesh_grid_2d));
  if (!grid) {
    goto on_error;
  }
  grid->size = grid_size;
  grid->xx1 = slwr_matrix_alloc(grid_size, grid_size);
  if (!grid->xx1) {
    goto on_error;
  }
  grid->xx2 = slwr_matrix_alloc(grid_size, grid_size);
  if (!grid->xx2) {
    goto on_error;
  }
  return grid;
on_error:
  if (grid) {
    mesh_grid_2d_free(grid);
  }
  return NULL;
}

void mesh_grid_2d_free(mesh_grid_2d *grid) {
  if (!grid) {
    return;
  }
  if (grid->xx2) {
    slwr_matrix_free(grid->xx2);
  }
  if (grid->xx1) {
    slwr_matrix_free(grid->xx1);
  }
  g_free(grid);
}

slwr_status
mesh_grid_2d_initialize(const slwr_synthetic_data_generator *generator,
                        mesh_grid_2d *grid) {
  g_return_val_if_fail(generator != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(grid != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(grid->xx1 != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(grid->xx2 != NULL, SLWR_STATUS_ERROR);
  g_return_val_if_fail(grid->xx1->size1 == grid->size &&
                           grid->xx1->size2 == grid->size,
                       SLWR_STATUS_DIMENSION_MISMATCH);
  g_return_val_if_fail(grid->xx2->size1 == grid->size &&
                           grid->xx2->size2 == grid->size,
                       SLWR_STATUS_DIMENSION_MISMATCH);

  slwr_vector *x1 = NULL;
  slwr_vector *x2 = NULL;
  slwr_status status;

  x1 = slwr_linspace(generator->domain_min->data[0],
                     generator->domain_max->data[0], grid->size, TRUE);
  if (!x1) {
    goto on_error;
  }
  x2 = slwr_linspace(generator->domain_min->data[1],
                     generator->domain_max->data[1], grid->size, TRUE);
  if (!x2) {
    goto on_error;
  }
  status = slwr_meshgrid_2d(x1, x2, grid->xx1, grid->xx2);
  slwr_vector_free(x2);
  slwr_vector_free(x1);
  return status;

on_error:
  if (x1) {
    slwr_vector_free(x1);
  }
  return SLWR_STATUS_ERROR;
}
