#include "datasets.h"

#include <glib.h>
#include <gsl/gsl_math.h>

#include <math.h>

static gdouble random_angle() { return g_random_double_range(0, 2 * M_PI); }

void sample_star_dataset(slwr_vector *x) {
  g_return_if_fail(x != NULL);
  g_return_if_fail(x->data != NULL);
  g_return_if_fail(x->size == 2);

  gdouble angle = random_angle();
  gdouble radius = g_random_double_range(0, 9);

  x->data[0] = cos(angle) * radius * gsl_pow_4(sin(angle * 5));
  x->data[1] = sin(angle) * radius * gsl_pow_4(sin(angle * 5));
}

void sample_donut_dataset(slwr_vector *x) {
  g_return_if_fail(x != NULL);
  g_return_if_fail(x->data != NULL);
  g_return_if_fail(x->size == 2);

  gdouble angle = random_angle();
  gdouble radius = g_random_double_range(3, 7);

  x->data[0] = cos(angle) * radius;
  x->data[1] = sin(angle) * radius;
}

void sample_spiral_dataset(slwr_vector *x) {
  g_return_if_fail(x != NULL);
  g_return_if_fail(x->data != NULL);
  g_return_if_fail(x->size == 2);

  gdouble angle = random_angle() * 5;
  gdouble delta_radius = g_random_double_range(-0.5, 0.5);
  gdouble radius = angle * 0.28 + delta_radius;

  x->data[0] = cos(angle) * radius;
  x->data[1] = sin(angle) * radius;
}
