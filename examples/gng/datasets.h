#pragma once

#include <slwr/slwr.h>

void sample_star_dataset(slwr_vector *x);
void sample_donut_dataset(slwr_vector *x);
void sample_spiral_dataset(slwr_vector *x);
