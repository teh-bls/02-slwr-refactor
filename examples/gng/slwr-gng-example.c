#include "cl-options.h"
#include "datasets.h"

#include <slwr/slwr.h>

#include <glib.h>
#include <jansson.h>

#include <stdlib.h>

static const gsize n_in = 2;

static void generate_sample(slwr_vector *x) {
  if (cl_options_dataset_name_is_donut()) {
    sample_donut_dataset(x);
  } else if (cl_options_dataset_name_is_star()) {
    sample_star_dataset(x);
  } else if (cl_options_dataset_name_is_spiral()) {
    sample_spiral_dataset(x);
  } else {
    g_critical("could not determine dataset name, zeroing the sample");
    slwr_vector_set_zero(x);
  }
}

static void randomize_vector_entries(slwr_vector *sample) {
  for (gsize i = 0; i < sample->size; ++i) {
    sample->data[i] = g_random_double_range(-1.0, 1.0);
  }
}

static void initialize_gng_context(slwr_gng_context *ctx) {
  g_assert_nonnull(ctx);
  g_assert_nonnull(ctx->nodes);
  g_assert_nonnull(ctx->edges);
  g_assert(ctx->nodes->len == 0);
  g_assert(ctx->edges->len == 0);

  for (gsize i = 0; i < 2; ++i) {
    slwr_gng_node *n = (slwr_gng_node *)g_malloc0(sizeof(slwr_gng_node));
    n->rf = slwr_receptive_field_alloc(n_in);
    randomize_vector_entries(n->rf->c);
    g_ptr_array_add(ctx->nodes, n);
  }

  slwr_gng_edge *e = (slwr_gng_edge *)g_malloc0(sizeof(slwr_gng_edge));
  e->a = g_ptr_array_index(ctx->nodes, 0);
  e->b = g_ptr_array_index(ctx->nodes, 1);
  g_ptr_array_add(ctx->edges, e);
}

static slwr_status save_gng_context_to_file(const slwr_gng_context *ctx,
                                            const gchar *filename) {
  slwr_status status = SLWR_STATUS_OK;
  json_t *ctx_json = slwr_gng_context_to_json(ctx);
  if (!ctx_json) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  if (slwr_write_json_to_file(ctx_json, filename) != SLWR_STATUS_OK) {
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  g_message("Gng context saved to %s", filename);

end:
  if (ctx_json) {
    json_decref(ctx_json);
  }
  return status;
}

int main(int argc, char *argv[]) {
  cl_options_parse(argc, argv);
  if (!cl_options_validate()) {
    return EXIT_FAILURE;
  }

  slwr_status status;

  slwr_workspace *ws = NULL;
  slwr_gng_context *ctx = NULL;
  slwr_gng_parameters params;
  slwr_gng_parameters_default_values(&params);
  gdouble sample_data[n_in];
  slwr_vector sample = {.size = n_in, .data = sample_data};

  ws = slwr_workspace_alloc(n_in, 0);
  if (!ws) {
    g_critical("could not create an slwr workspace");
    status = SLWR_STATUS_ERROR;
    goto end;
  }

  ctx = slwr_gng_context_alloc();
  if (!ctx) {
    g_critical("could not create a GNG context");
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  initialize_gng_context(ctx);

  for (gsize i = 1; i <= cl_options_num_samples(); ++i) {
    generate_sample(&sample);
    if ((status = slwr_gng_update(ws, ctx, &params, &sample, i)) !=
        SLWR_STATUS_OK) {
      g_critical("could not update the GNG network");
      goto end;
    }
  }

  if ((status = save_gng_context_to_file(ctx, "gng-example.json")) !=
      SLWR_STATUS_OK) {
    g_critical("could not save GNG context to file");
    goto end;
  }

end:
  if (ctx) {
    slwr_gng_context_free(ctx);
  }
  if (ws) {
    slwr_workspace_free(ws);
  }
  return status == SLWR_STATUS_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
