#include "cl-options.h"

#include <slwr/slwr.h>

static gchar *dataset_name = "donut";
static gsize num_samples = 100000;

gboolean cl_options_parse(int argc, char *argv[]) {
  const GOptionEntry option_entries[] = {
      {"dataset-name", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
       &dataset_name,
       "Training dataset name (one of ['donut', 'star', 'spiral'])", NULL},
      {"num-samples", 'n', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &num_samples,
       "Number of training samples", NULL},
      {NULL}};

  GError *error = NULL;
  GOptionContext *context = g_option_context_new(NULL);
  g_option_context_add_main_entries(context, option_entries, NULL);

  gboolean result = TRUE;

  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    g_critical("%s: %s", __PRETTY_FUNCTION__, error->message);
    result = FALSE;
    g_error_free(error);
  }

  g_option_context_free(context);

  return result;
}

gboolean cl_options_validate() {
  if (!slwr_str_equal(dataset_name, "donut") &&
      !slwr_str_equal(dataset_name, "star") &&
      !slwr_str_equal(dataset_name, "spiral")) {
    g_critical(
        "%s: dataset name must be one of ['donut', 'star', 'spiral'] (got %s)",
        __PRETTY_FUNCTION__, dataset_name);
    return FALSE;
  }
  if (num_samples < 1) {
    g_critical("%s: %s", __PRETTY_FUNCTION__,
               "the number of samples must be a strictly positive number");
    return FALSE;
  }
  return TRUE;
}

gsize cl_options_num_samples() { return num_samples; }

gboolean cl_options_dataset_name_is_donut() {
  return slwr_str_equal(dataset_name, "donut");
}

gboolean cl_options_dataset_name_is_star() {
  return slwr_str_equal(dataset_name, "star");
}

gboolean cl_options_dataset_name_is_spiral() {
  return slwr_str_equal(dataset_name, "spiral");
}