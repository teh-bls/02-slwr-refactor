#pragma once

#include <glib.h>

gboolean cl_options_parse(int argc, char *argv[]);
gboolean cl_options_validate();

gsize cl_options_num_samples();
gboolean cl_options_dataset_name_is_donut();
gboolean cl_options_dataset_name_is_star();
gboolean cl_options_dataset_name_is_spiral();
