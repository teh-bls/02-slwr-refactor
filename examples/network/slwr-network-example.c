#include "slwr/slwr.h"

#include <stdlib.h>

static void random_input_vector(const slwr_synthetic_data_generator *data,
                                slwr_vector *x);
static slwr_status train(slwr_workspace *ws, slwr_network_context *ctx);
static slwr_status predict(slwr_workspace *ws, slwr_network_context *ctx);
static void print_stats(const slwr_model_population *pop,
                        const slwr_network_statistics *stats);
static slwr_status write_population_to_file(const slwr_network_context *ctx,
                                            const gchar *population_filename);

int main() {
  slwr_workspace *ws = NULL;
  slwr_network_context *ctx = NULL;

  slwr_status status;

  /* Create a workspace */
  ws = slwr_workspace_alloc(2, 1);
  if (!ws) {
    g_critical("could not allocate an slwr workspace");
    status = SLWR_STATUS_ERROR;
    goto end;
  }

  /* Create a network context from network parameters */
  /* clang-format off */
  ctx = slwr_network_context_alloc_from_json_string(
  "{"
  "\"n-in\": 2, \"n-out\": 1,\n"
  "\"D-init\": [[100.0, 0.0], [0.0, 100.0]],\n"
  "\"w-gen\": 0.20,\n"
  "\"w-prune\": 0.70,\n"
  "\"error-max\": 0.020,\n"
  "\"decay-error-max-tau\" : 50,\n"
  "\"s\" : 1000.0,\n"
  "\"epsilon\" : 0.150,\n"
  "\"epsilon-A\" : 0.300,\n"
  "\"epsilon-w-out\" : 0.300,\n"
  "\"max-age\" : 1000,\n"
  "\"insertion-interval\" : 700,\n"
  "\"epsilon-n\" : 0.05,\n"
  "\"epsilon-b\" : 0.0001,\n"
  "\"alpha-error\" : 0.500,\n"
  "\"d\" : 0.995,\n"
  "\"receptive-field-update\" : \"pt-modify-rho\",\n"
  "\"decay-error-max\": \"off\",\n"
  "\"apply-strict-decay-error-max\": \"off\",\n"
  "\"model-pruning\": \"on\",\n"
  "\"model-allocation\": \"default-lwpr\",\n"
  "\"error-guided-insertion-error-source\": \"input-space\",\n"
  "\"max-models\": 1000"
  "}"
  );
  /* clang-format on */
  if (!ctx) {
    g_critical("could not allocate an slwr network context");
    status = SLWR_STATUS_ERROR;
    goto end;
  }

  /* Train the network */
  if ((status = train(ws, ctx)) != SLWR_STATUS_OK) {
    g_critical("could not train the network");
    goto end;
  }

  /* Write the receptive field population to a file */
  if ((status = write_population_to_file(
           ctx, "slwr-network-example-population.json")) != SLWR_STATUS_OK) {
    g_critical("could not save the population to a file");
    goto end;
  }

  /* Use the network for prediction */
  g_message("Network output:");
  if ((status = predict(ws, ctx)) != SLWR_STATUS_OK) {
    g_critical("could not use the network to predict");
    goto end;
  }

end:
  if (ctx) {
    slwr_network_context_free(ctx);
  }
  if (ws) {
    slwr_workspace_free(ws);
  }
  return status == SLWR_STATUS_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}

static void random_input_vector(const slwr_synthetic_data_generator *data,
                                slwr_vector *x) {
  g_assert(data->n_in == x->size);
  for (gsize i = 0; i < data->n_in; ++i) {
    x->data[i] = g_random_double_range(data->domain_min->data[i],
                                       data->domain_max->data[i]);
  }
}

static slwr_status train(slwr_workspace *ws, slwr_network_context *ctx) {
  slwr_synthetic_data_generator *data = slwr_crossed_ridge_data();
  if (!data) {
    return SLWR_STATUS_ERROR;
  }
  gdouble x_data[2];
  gdouble y_data[1];
  slwr_vector x = {.size = G_N_ELEMENTS(x_data), .data = x_data};
  slwr_vector y = {.size = G_N_ELEMENTS(y_data), .data = y_data};
  slwr_status status;
  for (gsize i = 0; i < 20000; ++i) {
    random_input_vector(data, &x);
    if ((status = data->generate(&x, &y)) != SLWR_STATUS_OK) {
      goto end;
    }
    if ((status = slwr_network_update(ws, ctx, &x, &y)) != SLWR_STATUS_OK) {
      goto end;
    }
  }

end:
  slwr_synthetic_data_free(data);
  return status;
}

static slwr_status predict(slwr_workspace *ws, slwr_network_context *ctx) {
  slwr_synthetic_data_generator *data = slwr_crossed_ridge_data();
  if (!data) {
    return SLWR_STATUS_ERROR;
  }
  gdouble x_data[2];
  gdouble y_data[1];
  gdouble y_pred_data[1];
  slwr_vector x = {.size = G_N_ELEMENTS(x_data), .data = x_data};
  slwr_vector y = {.size = G_N_ELEMENTS(y_data), .data = y_data};
  slwr_vector y_pred = {.size = G_N_ELEMENTS(y_pred_data), .data = y_pred_data};
  slwr_status status;
  for (gsize i = 0; i < 10; ++i) {
    random_input_vector(data, &x);
    if ((status = data->generate(&x, &y)) != SLWR_STATUS_OK) {
      goto end;
    }
    if ((status = slwr_network_predict(ws, ctx, &x, &y_pred)) !=
        SLWR_STATUS_OK) {
      goto end;
    }
    g_message("(%f, %f): predicted = %f, actual = %f", x.data[0], x.data[1],
              y_pred.data[0], y.data[0]);
  }

end:
  slwr_synthetic_data_free(data);
  return status;
}

static void print_stats(const slwr_model_population *pop,
                        const slwr_network_statistics *stats) {
  g_message("saw %lu updates", stats->n_updates);
  g_message("there are %d models in the population",
            pop->allocated_models->len);
  if (pop->gng_ctx) {
    g_message("there are %d nodes in the population", pop->gng_ctx->nodes->len);
  }
  gchar *min = slwr_vector_to_str(stats->input_space_min);
  gchar *max = slwr_vector_to_str(stats->input_space_max);
  g_message("input space min = %s", min);
  g_message("input space max = %s", max);
  g_free(max);
  g_free(min);
}

static slwr_status write_population_to_file(const slwr_network_context *ctx,
                                            const gchar *population_filename) {
  json_t *population_object = NULL;
  slwr_model_population *population = NULL;

  slwr_status status;

  population = g_ptr_array_index(ctx->populations, 0);
  population_object = slwr_model_population_to_json(population);
  if (!population_object) {
    g_critical("%s: could not convert the receptive field population to JSON",
               __PRETTY_FUNCTION__);
    status = SLWR_STATUS_ERROR;
    goto end;
  }
  print_stats(population, ctx->stats);
  if ((status = slwr_write_compact_json_to_file(
           population_object, population_filename)) != SLWR_STATUS_OK) {
    g_critical("%s: could not write the receptive field to a file",
               __PRETTY_FUNCTION__);
    goto end;
  }
  g_message("population written to %s", population_filename);

end:
  if (population_object) {
    json_decref(population_object);
  }
  return status;
}
