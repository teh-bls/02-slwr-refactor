#include "slwr/slwr.h"

#include <glib/gprintf.h>

#include <stdlib.h>

static const gsize n_in = 2;
static const gsize n_out = 1;

// clang-format off
static const gsize training_data_size = 10;
static gdouble training_data[] = {
    -0.803770, 0.408116,  -1.584830,
    0.137476,  0.468881,  0.302608,
    0.490270,  -0.435234, 1.006152,
    -0.894980, -0.109019, -1.787827,
    0.620625,  0.391628,  1.203272,
    -0.713102, 0.691930,  -1.381571,
    0.037465,  0.704040, 0.072348,
    0.356460,  -0.230910, 0.670361,
    0.779249,  0.879625,  1.522139,
    -0.931517, 0.720755,  -1.859779
};
static const gsize testing_data_size = 10;
static gdouble testing_data[] = {
    -0.497898,-0.765326,-0.957549,
    -0.627390,-0.195031,-1.282703,
    -0.559298,-0.230321,-1.070925,
    -0.464725,0.144059,-0.946023,
    -0.120145,-0.270800,-0.259758,
    -0.165555,0.792935,-0.306295,
    0.231376,0.571658,0.440202,
    -0.199744,-0.346298,-0.426847,
    0.128880,0.060343,0.233623,
    0.932880,0.737642,1.825793,
};
// clang-format on

static void generate_sample(slwr_vector *x, slwr_vector *y) {
  for (gsize i = 0; i < x->size; ++i) {
    x->data[i] = g_random_double_range(-1.0, 1.0);
  }
  y->data[0] = 2 * x->data[0] + g_random_double_range(-0.05, 0.05);
  for (gsize i = 1; i < y->size; ++i) {
    y->data[i] = 0.0;
  }
}

int main() {
  slwr_workspace *ws = slwr_workspace_alloc(n_in, n_out);

  slwr_ols_parameters params = {
      .n_in = n_in, .n_out = n_out, .epsilon_A = 0.05, .epsilon_w_out = 0.05};
  slwr_ols_context *ctx = slwr_ols_context_alloc(&params);
  if (!ctx) {
    return EXIT_FAILURE;
  }

  slwr_receptive_field *rf = slwr_receptive_field_alloc(n_in);
  if (!rf) {
    return EXIT_FAILURE;
  }

  /* Train */
  slwr_vector x = {.data = training_data, .size = n_in};
  slwr_vector y = {.data = training_data + n_in, .size = n_out};
  for (gsize i = 0; i < training_data_size; ++i) {
    slwr_status status = slwr_ols_update(ws, ctx, &params, rf, &x, &y);
    g_assert(status == SLWR_STATUS_OK);
    x.data = x.data + n_in + n_out;
    y.data = y.data + n_in + n_out;
  }

  /* Test */
  x.data = testing_data;
  y.data = testing_data + n_in;
  gdouble y_pred_data[n_out];
  slwr_vector y_pred = {.size = n_out, .data = y_pred_data};
  for (gsize i = 0; i < testing_data_size; ++i) {
    slwr_status status = slwr_ols_predict(ws, ctx, rf->c, &x, &y_pred);
    g_assert(status == SLWR_STATUS_OK);
    gchar *actual = slwr_vector_to_str(&y);
    gchar *predicted = slwr_vector_to_str(&y_pred);
    g_printf("actual = %s, predicted = %s\n", actual, predicted);
    g_free(predicted);
    g_free(actual);
    x.data = x.data + n_in + n_out;
    y.data = y.data + n_in + n_out;
  }

  slwr_receptive_field_free(rf);
  slwr_ols_context_free(ctx);
  slwr_workspace_free(ws);
  return EXIT_SUCCESS;
}
