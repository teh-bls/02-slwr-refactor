#include "slwr/slwr.h"

#include <glib.h>

#include <stdio.h>
#include <stdlib.h>

static gdouble data[] = {
    -0.168256, -0.072238, -0.600047, -0.164566, 0.316028,  0.212951,  0.904032,
    0.416614,  -0.573503, 0.207670,  0.177620,  0.904745,  0.351947,  0.911511,
    0.985429,  -0.336683, 0.289540,  -0.150912, -0.280095, 0.494226,  0.825380,
    0.135118,  0.335316,  -0.025290, 0.215976,  0.032099,  -0.639832, 0.549459,
    0.319978,  0.724508,  0.017742,  -0.954892, -0.160532, 0.880857,  -0.891028,
    -0.900775, 0.703196,  -0.135492, -0.032714, -0.474488};

int main() {
  const gsize n_in = 2;
  const gsize n_out = 1;
  slwr_workspace *ws = slwr_workspace_alloc(n_in, n_out);
  slwr_pt_modify_parameters params = {.n_in = n_in,
                                      .dist_max = 1.0,
                                      .error_max = 0.1,
                                      .epsilon = 0.01,
                                      .s = 1000.0};
  slwr_receptive_field *rf = slwr_receptive_field_alloc(n_in);
  gchar *D = slwr_matrix_to_str(rf->D);
  printf("initial receptive field = %s\n", D);
  g_free(D);

  slwr_vector x = {.size = n_in, .data = data};
  for (gsize i = 0; i < G_N_ELEMENTS(data); ++i) {
    slwr_pt_modify_update(ws, &params, SLWR_PT_MODIFY_RHO, rf, &x, 0.05);
    x.data = x.data + n_in;
  }
  D = slwr_matrix_to_str(rf->D);
  printf("final receptive field = %s\n", D);
  g_free(D);

  slwr_receptive_field_free(rf);
  slwr_workspace_free(ws);
  return EXIT_SUCCESS;
}
