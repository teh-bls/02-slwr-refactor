function(add_slwr_executable executable_name source_list)
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(GSL REQUIRED gsl)
    pkg_check_modules(GIO REQUIRED gio-2.0)
    pkg_check_modules(GLIB REQUIRED glib-2.0)
    pkg_check_modules(JANSSON REQUIRED jansson)
    add_executable(${executable_name})
    add_dependencies(${executable_name} 02-slwr-refactor_static)
    target_include_directories(${executable_name} PUBLIC
            ${GIO_INCLUDE_DIRS}
            ${GLIB_INCLUDE_DIRS}
            ${GSL_INCLUDE_DIRS}
            ${JANSSON_INCLUDE_DIRS}
            ${CMAKE_SOURCE_DIR})
    target_link_libraries(${executable_name} PUBLIC
            ${GIO_LIBRARIES}
            ${GLIB_LIBRARIES}
            ${GSL_LIBRARIES}
            ${JANSSON_LIBRARIES}
            ${CMAKE_BINARY_DIR}/slwr/lib02-slwr-refactor_static.a)
    target_sources(${executable_name} PRIVATE ${source_list})
endfunction()
